<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */

namespace Pasoka\Component\Config\Interfaces;

/**
 * Interface para classes de configuracao
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Interfaces
 * @namespace  Pasoka\Interfaces
 * @version    1.0.0
 */
interface ConfigInterface
{



}