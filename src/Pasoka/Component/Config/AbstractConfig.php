<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Config;

use Pasoka\Component\Config\Interfaces\ConfigInterface;

/**
 * Class AbstractConfig
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Config
 */
abstract class AbstractConfig implements ConfigInterface
{

    /**
     * @param mixed $key
     * @return null|mixed
     */
    public static function getProperty($key = null)
    {
        $properties = static::getProperties();

        if (!is_array($properties)) {
            $properties = [];
        }

        if (array_key_exists($key, $properties) !== false) {
            return $properties[$key];
        }

        return null;
    }

    /**
     * @return array
     */
    abstract public function getProperties();

}