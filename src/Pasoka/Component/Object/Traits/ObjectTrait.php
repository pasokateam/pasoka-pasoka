<?php

namespace Pasoka\Component\Object\Traits;

use Doctrine\Common\Collections\ArrayCollection;
use Pasoka\Component\Date\DateFormatter;
use Pasoka\Component\Annotation\Annotation;
use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\PasokaObject;

/**
 * Class StdTrait
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @author  Adilson Ferreira - adilson@climatempo.com.br
 * @version 1.1.0
 * @package Pasoka\Component\Object\Traits
 */
trait ObjectTrait
{

    /**
     * @param mixed  $value
     * @param string $from
     *
     * @return null|\stdClass|\stdClass[]|string
     */
    private static function getValueProperty($value, $from = null, $config = [])
    {
        if ($value instanceof ObjectInterface) {
            return PasokaObject::valueOf($value, $from, $config);
        } else if ($value instanceof ArrayCollection ||
            is_array($value) ||
            is_object($value) &&
            !$value instanceof \DateTime
        ) {
            return PasokaObject::valuesOf($value, $from, $config);
        } else if ($value instanceof \DateTime) {
            $format = null;
            if (isset($config['formatDate'])) {
                $format = $config['formatDate'];
            }
            return DateFormatter::getString($value, $format);
        } else {
            return $value;
        }
    }


    /**
     * Converte uma entidade em stdClass
     *
     * @param string $from
     * @return \stdClass
     */
    public function valueOf($from = null, $config = [])
    {
        $obj = new \stdClass();
        $class = __CLASS__;
        $reflectionClass = new \ReflectionClass($class);

        foreach ($this as $property => $value) {

            if ($reflectionClass->hasProperty($property)) {

                $reflectionProperty = new \ReflectionProperty($class, $property);
                $comment = $reflectionProperty->getDocComment();

                if (Annotation::hide($comment, $from)) {
                    continue;
                }

                $obj->$property = self::getValueProperty($value, $class, $config);
            }
        }

        return $obj;
    }

    /**
     * Metodo para obter nome da classe, para uso
     * em repositorio.
     *
     * @return string
     */
    public static function getClassName()
    {
        return __CLASS__;
    }

}
