<?php

namespace Pasoka\Component\Object;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Pasoka\Component\Object\Interfaces\ObjectInterface;

/**
 * Class PasokaObject
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @author  Adilson Ferreira - adilson@climatempo.com.br
 * @version 1.1.0
 * @package Pasoka\Component\Object
 */
final class PasokaObject
{

    private function __construct()
    {
    }

    /**
     * @param ObjectInterface $vo
     *
     * @param string          $from
     *
     * @return null|\stdClass
     */
    public static function valueOf($vo = null, $from = null, $config = [])
    {
        if ($vo instanceof ObjectInterface) {
            return $vo->valueOf($from, $config);
        }

        return null;
    }

    /**
     * Retorna um Array de Objetos
     *
     * @param array|ArrayCollection $list
     *
     * @param string                $from
     *
     * @return \stdClass[]
     */
    public static function valuesOf($list = [], $from = null, $config = [])
    {
        $newList = [];
        if ($list instanceof Collection) {
            $list = $list->toArray();
        }

        if (is_array($list) || $list instanceof \stdClass) {

            foreach ($list as $item) {

                if ($item instanceof ObjectInterface) {
                    $item = $item->valueOf($from, $config);
                }

                $newList[] = $item;
            }
        }

        return $newList;
    }

}
