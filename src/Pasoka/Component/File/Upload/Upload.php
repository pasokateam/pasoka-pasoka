<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\File\Upload;

/**
 * Classe para upload de arquivos (Classe antiga).
 *
 * @author Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka
 * @subpackage Pasoka\Component\File
 * @namespace Pasoka\Component\File
 * @version 1.0.0
 */
class Upload
{
    /**
     * Caminho do destino
     *
     * @access private
     * @var string
     */
    private $path;
    
    /**
     * Recebe a variavel global $_FILE
     *
     * @access private
     * @var string[]
     */
    private $file;

    /**
     * Tipo da imagem
     *
     * @access private
     * @var string[]
     */
    private $types;

    /**
     * Extensão da imagem
     *
     * @access private
     * @var string
     */
    private $extension;

    /**
     * Nome da imagem atual
     *
     * @access private
     * @var string
     */
    private $name;

    /**
     * Atribui um novo nome para a imagem
     *
     * @access private
     * @var string
     */
    private $newName;

    /**
     * Caminho temporário
     *
     * @access private
     * @var string
     */
    private $pathTmp;

    /**
     * Tamanho da imagem
     *
     * @access private
     * @var int
     */
    private $size;

    /**
     * tamanho limit de upload em kb
     *
     * @access private
     * @var int
     */
    private $limitSize;

    /**
     * Construtor
     *
     * @access public
     * @param string[] $file
     * @param string $new_name
     * @param string[] $types
     * @param string $path
     * @param int $limit_size
     */
    public function __construct(
        array $file  = [], 
    	$new_name    = null, 
    	array $types = [],
        $path        = null, 
    	$limit_size  = 100000
    ) {
        $this->file      = $file;
        $this->newName   = $new_name;
        $this->types     = $types;
        $this->path      = $path;
        $this->limitSize = $limit_size;
    }

    /**
     * @access public
     * @param string $new_name
     */
    public function setName($new_name)
    {
        $this->newName = $new_name;
    }

    /**
     * @access public
     * @param int $limit_size
     */
    public function setLimitSize($limit_size)
    {
        $this->limitSize = $limit_size;
    }

    /**
     * atribui array file vindo do post
     *
     * @access public
     * @param array $file
     */
    public function setFile(array $file)
    {
        $this->file = $file;
    }

    /**
     * atribui tipos de arquivos que seram aceitos para upload
     *
     * @access public
     * @param string[] $types
     */
    public function setTypes(array $types)
    {
        $this->types = $types;
    }

    /**
     * atribui caminho de onde o arquivo deve ficar
     *
     * @access public
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * faz upload do arquivo e retorna objeto com dados se tudo ocorrer bem
     *
     * @access public
     * @return bool stdClass
     */
    public function move()
    {
        try {
            // verifica se o caminho foi atribuido ou se existe
            if (isset($this->path) || is_dir($this->path)) {
	
                // verifica se o array file foi atribuido
                if (isset($this->file['tmp_name']) 
                    && !empty($this->file['tmp_name']) 
                    && $this->file['error'] == 0
                ) {
					
                    // atribui dados aos atributos privados
                    $this->name     = $this->file['name'];
                    $this->path_tmp = $this->file['tmp_name'];
                    $this->size     = (round($this->file['size'] / 1024));

                    $this->checksSize();
                    
                    $ext = explode(".", $this->name);
                    $this->extension = strtolower(end($ext));

                    /**
                     * verifica se a extensao do arquivo 
                     * esta na lista permitida
                     */
                    if ($this->checkExtension()) {
                    	    	
                        // verifica se o nome eh valido
                    	if (!isset($this->newName) 
                    	    || strlen($this->newName) < 1
                        ) {
                            $this->newName = explode(".", $this->name)[0];
                        }
                        
                        // verifica se exista a barra no final do
                        // caminho
                        if (substr($this->path, -1, 1) != "/") {
                            $this->path = "{$this->path}/";
                        }

                        // arquivo com caminho
                        $destination = "{$this->path}{$this->newName}"
                            . ".{$this->extension}";
                        
                        // faz upload do arquivo e verifica se foi
                        // feito com sucesso
                        if (move_uploaded_file($this->pathTmp, $destination)) {
                            $object = new \stdClass();
                            $object->name = $this->newName;
                            $object->file = "{$this->newName}.{$this->extension}";
                            $object->path = $destination;
                            
                            $object->date = date(
                                "Y-m-d H:i:s",
                                filemtime($destination)
                            );
                                
                            $object->extension = $this->extension;
                            $object->size = [
                                'kb' => (float)$this->size,
                                'mb' => (float)(number_format(
                                		($this->size / 1024), 2))
                            ];
                            return $object;
                        }
                    }
                } else {
                    throw new \Exception("Arquivo nao encontrado");
                }
            } else {
                throw new \Exception("Diretorio nao existe ou nao foi definido");
            }
        } catch (\Exception $e) {
            echo("IO Error: " . $e->getMessage());
        }
        return null;
    }
    
    /**
     * Checa o tamanho da imagem em MB
     * 
     * @throws \Exception
     * @return boolean
     */
    private function checksSize()
    {
    	if ($this->size <= $this->limitSize) {
    		return true;
    	} else {
    		throw new \Exception("IO Error: Arquivo muito pesado");
    	}
    }

    /**
     * Checa a extensão da imagem
     * @return boolean
     */
    private function checkExtension()
    {
        if (array_search($this->extension, $this->types) !== false
            || count($this->types) == 0
    	) {
        	return true;	
    	}
    	
    	return false;
    }
}