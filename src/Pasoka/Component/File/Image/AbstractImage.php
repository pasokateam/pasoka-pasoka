<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\File\Image;

use Pasoka\Component\File\Image\Interfaces\ImageInterface;

/**
 * Contem os métodos e atributos base para a manipulação das classes de imagem
 *
 * @author     Michel Araujo - <araujo_michel@yahoo.com.br>
 * @package    Pasoka
 * @subpackage Component\File
 * @namespace  Pasoka\Component\File\Image
 * @version    1.0.0
 */
abstract class AbstractImage implements ImageInterface
{
    /**
     * resource para controle da imagem
     *
     * @access private
     * @var Resource
     */
    public $resource;

    /**
     * Codigo do tipo da imagem
     *
     * @access private
     * @var int
     */
    public $typeCode;

    /**
     * nome da imagem
     *
     * @access private
     * @var string
     */
    public $name;

    /**
     * Caminho da imagem
     *
     * @access private
     * @var string
     */
    public $path;

    /**
     * Diretorio da imagem
     *
     * @access private
     * @var string
     */
    public $dir;

    /**
     * largura da imagem
     *
     * @access private
     * @var int
     */
    public $width;

    /**
     * altura da imagem
     *
     * @access private
     * @var int
     */
    public $height;

    /**
     * Tamanho da imagem em kb
     *
     * @access private
     * @var double
     */
    public $size;

    /**
     * Extensao da imagem
     *
     * @access private
     * @var string
     */
    public $extension;

    /**
     * Extensao antiga
     *
     * @access private
     * @var string
     */
    public $oldExtension;

    /**
     * Nome antigo
     *
     * @access private
     * @var string
     */
    public $oldName;

    /**
     * Caminho inicial da imagem
     *
     * @access private
     * @var string
     */
    public $oldPath;

    /**
     * Qualidade da imagem de 0 a 9 (png) / 0 a 100 (jpeg)
     *
     * @access private
     * @var int
     */
    public $quality;

    /**
     * lista de extensoes validas
     *
     * @access private
     * @var string[]
     */
    public $extensions = [
        'gif',
        'jpeg',
        'jpg',
        'png',
        'apng',
        'tiff',
        'iff',
        'jp2',
        'bmp'
    ];

    /**
     * Construtor
     *
     * @access public
     * @param string $path
     * @throws \Exception
     */
    public function __construct($path)
    {
        // verifica se a imagem existe
        if (file_exists($path)) {

            // obtem extensao e nome da imagem
            $pathinfo = pathinfo($path);
            $this->extension = strtolower($pathinfo['extension']);
            $this->oldExtension = strtolower($pathinfo['extension']);
            $this->name = $pathinfo['filename'];
            $this->oldName = $pathinfo['filename'];
            $this->dir = str_replace($pathinfo['basename'], '', $path);

            $this->checkExtension($this->extension);
            $this->checkImageEstate($path);

            // carrega resource da imagem
            $this->loadResource();
        } else {
            throw new \Exception('Imagem nao encontrada');
        }
    }

    /**
     * Verifica se a extensão da imagem e valida pela a lista de extensão
     * definida no atributo extension
     *
     * @param string $extension
     * @throws \Exception
     * @return boolean
     */
    protected function checkExtension($extension)
    {
        if (array_search($extension, $this->extensions) !== false) {
            return true;
        } else {
            throw new \Exception('Extensao invalida');
        }
    }

    /**
     * Checa o estado da imagem como largura, comprimento, qualidade e etc
     *
     * @param string $path
     * @throws \Exception
     */
    protected function checkImageEstate($path)
    {
        // obtem dados da imagem
        $image_data = getimagesize($path);

        // verifica se a imagem eh valida
        if ($image_data != false) {
            $this->path = $path;
            $this->oldPath = str_replace(
                $this->name,
                $this->oldName,
                str_replace(
                    $this->extension,
                    $this->oldExtension,
                    $this->path
                )
            );
            $this->width = $image_data[0];
            $this->height = $image_data[1];
            $this->typeCode = $image_data[2];
            $this->size = (
            number_format((filesize($this->path) / 1024), 2)
            );
            $this->quality = 100;
        } else {
            throw new \Exception('Imagem invalida');
        }
    }

    /**
     * Carrega resource
     *
     * @access private
     */
    protected function loadResource()
    {
        $this->resource = imagecreatefromstring(
            file_get_contents($this->path)
        );

        switch ($this->extension) {
            case "png":
                imagesavealpha($this->resource, true);
                imagealphablending($this->resource, true);
                break;
            case "gif":
                $tc = imagecolorallocate($this->resource, 0, 0, 0);
                imagecolortransparent($this->resource, $tc);
                break;
        }
    }
}