<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\File\Image\Interfaces;

/**
 * Interface ImageInterface
 *
 * @package Pasoka\Component\File\Image\Interfaces
 */
interface ImageInterface
{

}