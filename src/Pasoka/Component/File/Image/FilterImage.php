<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Component\File\Image;

use Pasoka\Component\File\Image\AbstractImage;

/**
 * Manipular os filtros da imagem
 *
 * @author     Michel Araujo - <araujo_michel@yahoo.com.br>
 * @package    Pasoka
 * @subpackage Component\File
 * @namespace  Pasoka\Component\File\Image
 * @version    1.0.0
 */
class FilterImage extends AbstractImage
{
    /**
     * Filtro para contraste
     *
     * @access public
     * @param int $level
     * @return bool
     */
    public function setFilterContrast($level = 0)
    {
        if (is_int($level)) {
            if (imagefilter($this->resource, IMG_FILTER_CONTRAST, $level)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Filtro de pixel
     *
     * @access public
     * @param int $block
     * @param int $effect
     * @return bool
     */
    public function setFilterPixelate($block = 0, $effect = 0)
    {
        if (imagefilter($this->resource, IMG_FILTER_PIXELATE, $block, $effect)) {
            return true;
        }
        return false;
    }

    /**
     * Filtro para deixar imagem sem cor
     *
     * @access public
     * @return bool
     */
    public function setFilterGray()
    {
        if (imagefilter($this->resource, IMG_FILTER_GRAYSCALE)) {
            return true;
        }
        return false;
    }

    /**
     * Filtro negativo
     *
     * @access public
     * @return bool
     */
    public function setFilterNegate()
    {
        if (imagefilter($this->resource, IMG_FILTER_NEGATE)) {
            return true;
        }
        return false;
    }

    /**
     * Filtro de luz
     *
     * @access public
     * @param int $level
     * @return bool
     */
    public function setFilterBrightness($level = 0)
    {
        if (is_int($level)) {
            if (imagefilter($this->resource, IMG_FILTER_BRIGHTNESS, $level)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Filtro de cor
     *
     * @access public
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return bool
     */
    public function setFilterColorize($red = 0, $green = 0, $blue = 0)
    {
        // validando entradas
        $red = is_int($red) ? $red : 0;
        $red = $red <= 255 && $red >= 0 ? $red : 0;

        $green = is_int($green) ? $green : 0;
        $green = $green <= 255 && $green >= 0 ? $green : 0;

        $blue = is_int($blue) ? $blue : 0;
        $blue = $blue <= 255 && $blue >= 0 ? $blue : 0;

        // verifica se o filtro foi atribuido
        if (imagefilter($this->resource, IMG_FILTER_COLORIZE, $red, $green, $blue)) {
            return true;
        }
        return false;
    }
}