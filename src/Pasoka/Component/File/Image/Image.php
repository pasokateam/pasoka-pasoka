<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\File\Image;

/**
 * Classe responsável por manipular a imagem
 * contem função como setCrop e setSize
 *
 * @author     Michel Araujo - <araujo_michel@yahoo.com.br>
 * @package    Pasoka
 * @subpackage Component\File
 * @namespace  Pasoka\Component\File\Image
 * @version    1.0.0
 */
class Image extends AbstractImage
{
    /**
     * atribui nome da imagem
     *
     * @access public
     * @param string $name
     */
    public function setName($name)
    {
        $this->path = str_replace($this->name, $name, $this->path);
        $this->name = $name;
    }

    /**
     * retorna nome da imagem
     *
     * @access public
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * retorna caminho da imagem
     *
     * @access public
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * retorna nome do arquivo
     *
     * @access public
     * @return String
     */
    public function getFile()
    {
        return $this->name . "." . $this->extension;
    }

    /**
     * atribui larguta da imagem
     *
     * @access public
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->setSize($width, $this->height);
    }

    /**
     * retorna larguta da imagem
     *
     * @access public
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * atribui altura da imagem
     *
     * @access public
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->setSize($this->width, $height);
    }

    /**
     * retorna largura da imagem
     *
     * @access public
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * retorna tamanho da imagem
     *
     * @access public
     * @return double
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * atribui extensao da imagem
     *
     * @access public
     * @param string $extension
     */
    public function setExtension($extension)
    {
        if ($this->checkExtension($extension) !== false) {
            $this->path = str_replace(
                $this->extension,
                $extension,
                $this->path
            );
            $this->extension = strtolower($extension);
        }
    }

    /**
     * retorna extensao da imagem
     *
     * @access public
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * atribui Qualidade da imagem de 0 a 9 (png) / 0 a 100 (jpeg)
     *
     * @access public
     * @param int $quality
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }

    /**
     * retorna Qualidade da imagem de 0 a 9 (png) / 0 a 100 (jpeg)
     *
     * @access public
     * @return int
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * altera nome da imagem em instancia
     *
     * @access public
     * @param string $newname
     * @return bool
     */
    public function rename($newname)
    {
        // novo nome
        $newpath = str_replace($this->name, $newname, $this->path);

        // verifica se foi renomeado e atualiza varaveis locais
        if (rename($this->path, $newpath)) {
            $this->path = $newpath;
            $this->name = $newname;
            return true;
        }
        return false;
    }

    /**
     * Move arquivo de diretorio em instancia
     *
     * @access public
     * @param string $path
     * @return bool
     */
    public function move($path)
    {
        $this->save();

        // verifica se existe a barra no final do caminho
        if (substr($path, -1, 1) != "/") {
            $path = "{$path}/";
        }

        // verifica se o diretorio existe
        if (is_dir($path)) {
            $path = "{$path}{$this->name}.{$this->extension}";

            imagedestroy($this->resource);

            // copia arquivo para novo diretorio
            if (copy($this->oldPath, $path)) {

                // deleta arquivo antigo
                if (unlink($this->oldPath)) {
                    $this->oldPath = $path;
                    $this->path = $path;
                    $this->loadResource();
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Faz uma copia do arquivo em instancia
     *
     * @access public
     * @param string $name
     * @param string $path
     * @param bool   $instance
     * @return bool
     */
    public function copy($name, $path = null, $instance = false)
    {
        $newpath = str_replace($this->name, $name, $this->path);

        // verifica se o path foi atribuido
        if (isset($path)) {
            // verifica se exista a barra no final do caminho
            if (substr($path, -1, 1) != "/") {
                $path = "{$path}/";
            }

            // verifica se o diretorio existe
            if (is_dir($path)) {
                $newpath = str_replace($this->dir, $path, $newpath);
            }
        }

        // copia arquivo para novo diretorio
        if (copy($this->oldPath, $newpath)) {

            // verifica se deve alterar a instancia atual para o novo arquivo
            if ($instance === true) {
                $this->path = $newpath;
                $this->oldPath = $newpath;

                // carrega resource da imagem
                $this->loadResource();
            }
            return true;
        }
        return false;
    }

    /**
     * Deleta imagem em instancia
     *
     * @access public
     * @return bool
     *
     */
    public function delete()
    {
        return unlink($this->oldPath);
    }

    /**
     * Adiciona marca dagua na imagem em instancia
     *
     * @access public
     * @param string $path_mark
     * @param int    $x
     * @param int    $y
     * @param int    $width
     * @param int    $height
     * @return bool
     */
    public function setWatermark(
        $path_mark, $x = 0, $y = 0, $width = 0, $height = 0
    )
    {
        $mark = new Image($path_mark);

        // valida e atribui tamanhos da marca
        if (($width > 0
                && $height > 0)
            && (is_int($width)
                && is_int($height))
        ) {
            $mark->setSize($width, $height);
        }

        $mark_source = $mark->getResource();

        imagecopy(
            $this->resource,
            $mark_source,
            $x,
            $y,
            0,
            0,
            $mark->getWidth(),
            $mark->getHeight()
        );

        // faz uniao das imagens
        // imagecopymerge($this->resource, $mark->getResource(), $x, $y, 0, 0,
        // $mark->getWidth(), $mark->getHeight(), $opacity);
    }

    /**
     * Retorna codigo em base64 da imagem para img em html
     *
     * @access public
     * @return String
     */
    public function getBase64html()
    {
        return "data:image/{$this->extension};base64,"
        . base64_encode(file_get_contents($this->path));
    }

    /**
     * Retorna resource da imagem
     *
     * @access public
     * @return resource
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Altera tamanho da imagem em instancia
     *
     * @access public
     * @param int $width
     * @param int $height
     */
    public function setSize($width, $height = null)
    {
        // verifica se a altura deve ser ajustada
        if (!isset($height) || $height == 0) {
            $percent = $width / $this->width * 100;
            $height = $this->height * $percent / 100;
        }

        $image = $this->resource;
        $this->resource = imagecreatetruecolor($width, $height);

        // ajusta transparencia
        // $bgc = imagecolorallocate($this->resource, 255, 255, 255);
        $tc = imagecolorallocate($this->resource, 0, 0, 0);
        imagecolortransparent($this->resource, $tc);
        imagealphablending($this->resource, false);
        imagesavealpha($this->resource, true);

        // ajusta tamanho
        imagecopyresized(
            $this->resource,
            $image,
            0,
            0,
            0,
            0,
            $width,
            $height,
            $this->width,
            $this->height
        );

        // atualiza dados
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Ajusta tamanho da imagem por porcentagem
     *
     * @access public
     * @param double|int $percent
     */
    public function setSizeByPercent($percent)
    {
        $width = $this->width * $percent;
        $height = $this->height * $percent;

        $this->setSize($width, $height);
    }

    /**
     * Corta imagem em instancia
     *
     * @access public
     * @param     $width
     * @param     $height
     * @param int $o_width
     * @param int $o_height
     * @return bool
     */
    public function setCrop($width, $height, $o_width = null, $o_height = null)
    {
        // verifica se foi atribuido algum tamanho proprio
        if (!isset($o_width) && !is_int($o_width)) {
            $o_width = $this->width;
        }

        if (!isset($o_height) && !is_int($o_height)) {
            $o_height = $this->height;
        }

        // obtem posicoes do crop
        $start_x = ($o_width / 2) - ($width / 2);
        $start_y = ($o_height / 2) - ($height / 2);

        // cropa imagem
        $image = $this->resource;
        $this->resource = imagecreatetruecolor($width, $height);
        imagecopyresampled(
            $this->resource,
            $image,
            0,
            0,
            $start_x,
            $start_y,
            $width,
            $height,
            $width,
            $height
        );
    }

    /**
     * Exibe imagem em instancia (head)
     *
     * @access public
     */
    public function show()
    {
        switch ($this->extension) {
            case "png":
                $scaleQuality = round(($this->quality / 100) * 9);
                $invertScaleQuality = 9 - $scaleQuality;
                header('Content-Type: image/png');
                imagepng($this->resource, null, $invertScaleQuality);
                break;
            case "gif":
                header('Content-Type: image/gif');
                imagegif($this->resource, null);
                break;
            default:
                header('Content-Type: image/jpeg');
                imagejpeg($this->resource, null, $this->quality);
        }

        imagedestroy($this->resource);
        exit();
    }

    /**
     * Salva dados de imagem em instancia
     *
     * @access public
     */
    public function save($delete = true)
    {
        // deleta arquivo antigo
        if ($delete === true) {
            if (unlink($this->oldPath)) {
                $this->oldPath = $this->path;
            }
        }

        // salva novo arquivo
        switch ($this->extension) {
            case "png":
                imagepng($this->resource, $this->path, (9 - round(($this->quality / 100) * 9)));
                break;
            case "gif":
                imagegif($this->resource, $this->path);
                break;
            default:
                imagejpeg($this->resource, $this->path, $this->quality);
        }
        // imagedestroy($this->resource);
    }

    /**
     * destroi imagem
     *
     * @access public
     */
    public function __destruct()
    {
        if ($this->resource != null) {
            imagedestroy($this->resource);
        }
    }
}