<?php

namespace Pasoka\Component\Manifest;

use Pasoka\Component\Json\Json;
use Pasoka\Component\Manifest\Config\ModuleConfig;

/**
 * Class Manifest
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Manifest
 */
final class Manifest
{
    /**
     * @var string
     */
    const DEFAULT_NAME = "manifest.json";

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $file;

    /**
     * @var ModuleConfig
     */
    private $module;

    /**
     * Constructor
     *
     * @param string $path
     * @throws \Exception
     */
    public function __construct($path)
    {
        $this->path = $path;
        $this->file = $path . DIRECTORY_SEPARATOR . self::DEFAULT_NAME;

        if (!file_exists($this->file)) {
            throw new \Exception("Manifest file not found: {$this->file}");
        }

        $json = Json::createFromFile($this->file);
        $this->module = ModuleConfig::createFromObject($json->decode());
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }


    /**
     * @return ModuleConfig
     */
    public function getModule()
    {
        return $this->module;
    }


} 