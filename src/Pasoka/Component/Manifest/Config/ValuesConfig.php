<?php

namespace Pasoka\Component\Manifest\Config;


use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class ValuesConfig
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Manifest
 */
final class ValuesConfig implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @var string
     */
    private $path;

    public function __construct($path = null)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }


} 