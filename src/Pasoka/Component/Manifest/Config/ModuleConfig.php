<?php

namespace Pasoka\Component\Manifest\Config;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class ModuleConfig
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Manifest\Config
 */
final class ModuleConfig implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @var string
     */
    private $name;

    /**
     * @var DatabaseConfig
     */
    private $database;

    /**
     * @var LogConfig
     */
    private $log;

    /**
     * @var ValuesConfig
     */
    private $values;

    /**
     * @var ViewConfig
     */
    private $view;

    /**
     * @var \stdClass
     */
    private $metadata;


    /**
     * @param \stdClass $obj
     * @return $this
     */
    public static function createFromObject(\stdClass $obj)
    {

        $reflection = new \ReflectionObject($obj);
        $module = new self();

        if ($reflection->hasProperty("name")) {
            $module->setName($obj->name);
        }

        // DATABASE CONFIG
        if ($reflection->hasProperty("database") &&
            $obj->database instanceof \stdClass
        ) {

            $database = new DatabaseConfig();
            $reflectionDatabase = new \ReflectionObject($obj->database);

            if ($reflectionDatabase->hasProperty("devMode")) {
                $database->setDevMode($obj->database->devMode);
            }
            if ($reflectionDatabase->hasProperty("driver")) {
                $database->setDriver($obj->database->driver);
            }
            if ($reflectionDatabase->hasProperty("name")) {
                $database->setName($obj->database->name);
            }
            if ($reflectionDatabase->hasProperty("host")) {
                $database->setHost($obj->database->host);
            }
            if ($reflectionDatabase->hasProperty("port")) {
                $database->setPort($obj->database->port);
            }
            if ($reflectionDatabase->hasProperty("user")) {
                $database->setUser($obj->database->user);
            }
            if ($reflectionDatabase->hasProperty("password")) {
                $database->setPassword($obj->database->password);
            }
            if ($reflectionDatabase->hasProperty("pathEntities")) {
                $database->setPathEntities($obj->database->pathEntities);
            }

            $module->setDatabase($database);
        }

        // LOG CONFIG
        if ($reflection->hasProperty("log") &&
            $obj->log instanceof \stdClass
        ) {

            $log = new LogConfig();
            $reflectionLog = new \ReflectionObject($obj->log);

            if ($reflectionLog->hasProperty("name")) {
                $log->setName($obj->log->name);
            }
            if ($reflectionLog->hasProperty("path")) {
                $log->setPath($obj->log->path);
            }
            $module->setLog($log);
        }

        // VALUES CONFIG
        if ($reflection->hasProperty("values") &&
            $obj->values instanceof \stdClass
        ) {

            $values = new ValuesConfig();
            $reflectionValues = new \ReflectionObject($obj->values);

            if ($reflectionValues->hasProperty("path")) {
                $values->setPath($obj->values->path);
            }
            $module->setValues($values);
        }


        // VIEW CONFIG
        if ($reflection->hasProperty("view") &&
            $obj->values instanceof \stdClass
        ) {

            $view = new ViewConfig();
            $reflectionView = new \ReflectionObject($obj->view);

            if ($reflectionView->hasProperty("cache")) {
                $view->setCache($obj->view->cache);
            }
            if ($reflectionView->hasProperty("compile")) {
                $view->setCompile($obj->view->compile);
            }
            if ($reflectionView->hasProperty("pathCache")) {
                $view->setPathCache($obj->view->pathCache);
            }
            if ($reflectionView->hasProperty("pathCompile")) {
                $view->setPathCompile($obj->view->pathCompile);
            }
            if ($reflectionView->hasProperty("pathTemplate")) {
                $view->setPathTemplate($obj->view->pathTemplate);
            }
            $module->setView($view);
        }


        // METADATA
        if ($reflection->hasProperty("metadata") &&
            $obj->metadata instanceof \stdClass
        ) {
            $module->setMetadata($obj->metadata);
        }

        return $module;
    }

    /**
     * @return DatabaseConfig
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param DatabaseConfig $database
     * @return $this
     */
    public function setDatabase($database)
    {
        $this->database = $database;
        return $this;
    }

    /**
     * @return LogConfig
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param LogConfig $log
     * @return $this
     */
    public function setLog($log)
    {
        $this->log = $log;
        return $this;
    }

    /**
     * @return \stdClass
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param \stdClass $metadata
     * @return $this
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ValuesConfig
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param ValuesConfig $values
     * @return $this
     */
    public function setValues($values)
    {
        $this->values = $values;
        return $this;
    }

    /**
     * @return ViewConfig
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param ViewConfig $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }


}