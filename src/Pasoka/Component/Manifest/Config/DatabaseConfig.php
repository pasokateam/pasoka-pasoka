<?php

namespace Pasoka\Component\Manifest\Config;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class DatabaseConfig
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Manifest
 */
final class DatabaseConfig implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @var bool
     */
    private $devMode;

    /**
     * @var string
     */
    private $driver;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $host;

    /**
     * @var number
     */
    private $port;

    /**
     * @var string
     */
    private $user;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $pathEntities;


    /**
     * @param bool   $devMode
     * @param string $driver
     * @param string $host
     * @param string $name
     * @param string $pathEntities
     * @param number $port
     * @param string $user
     * @param string $password
     */
    public function __construct(
        $devMode = null,
        $driver = null,
        $host = null,
        $name = null,
        $pathEntities = null,
        $port = null,
        $user = null,
        $password = null)
    {
        $this->driver = $driver;
        $this->host = $host;
        $this->name = $name;
        $this->pathEntities = $pathEntities;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->devMode = $devMode;
    }

    /**
     * @return boolean
     */
    public function isDevMode()
    {
        return $this->devMode;
    }

    /**
     * @param boolean $devMode
     * @return $this
     */
    public function setDevMode($devMode)
    {
        $this->devMode = $devMode;
        return $this;
    }


    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $driver
     * @return $this
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return $this
     */
    public function setHost($host)
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathEntities()
    {
        return $this->pathEntities;
    }

    /**
     * @param string $pathEntities
     * @return $this
     */
    public function setPathEntities($pathEntities)
    {
        $this->pathEntities = $pathEntities;
        return $this;
    }

    /**
     * @return number
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param number $port
     * @return $this
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

} 