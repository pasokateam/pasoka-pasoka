<?php

namespace Pasoka\Component\Manifest\Config;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class ViewConfig
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Manifest
 */
final class ViewConfig implements ObjectInterface
{
    use ObjectTrait;

    /**
     * @var bool
     */
    private $cache;

    /**
     * @var bool
     */
    private $compile;

    /**
     * @var string
     */
    private $pathCache;

    /**
     * @var string
     */
    private $pathCompile;

    /**
     * @var string
     */
    private $pathTemplate;

    /**
     * @param bool   $cache
     * @param bool   $compile
     * @param string $pathCache
     * @param string $pathCompile
     * @param string $pathTemplate
     */
    public function __construct(
        $cache = null,
        $compile = null,
        $pathCache = null,
        $pathCompile = null,
        $pathTemplate = null)
    {
        $this->cache = $cache;
        $this->compile = $compile;
        $this->pathCache = $pathCache;
        $this->pathCompile = $pathCompile;
        $this->pathTemplate = $pathTemplate;
    }

    /**
     * @return boolean
     */
    public function isCache()
    {
        return $this->cache;
    }

    /**
     * @param boolean $cache
     * @return $this
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isCompile()
    {
        return $this->compile;
    }

    /**
     * @param boolean $compile
     * @return $this
     */
    public function setCompile($compile)
    {
        $this->compile = $compile;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathCache()
    {
        return $this->pathCache;
    }

    /**
     * @param string $pathCache
     * @return $this
     */
    public function setPathCache($pathCache)
    {
        $this->pathCache = $pathCache;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathCompile()
    {
        return $this->pathCompile;
    }

    /**
     * @param string $pathCompile
     * @return $this
     */
    public function setPathCompile($pathCompile)
    {
        $this->pathCompile = $pathCompile;
        return $this;
    }

    /**
     * @return string
     */
    public function getPathTemplate()
    {
        return $this->pathTemplate;
    }

    /**
     * @param string $pathTemplate
     * @return $this
     */
    public function setPathTemplate($pathTemplate)
    {
        $this->pathTemplate = $pathTemplate;
        return $this;
    }


} 