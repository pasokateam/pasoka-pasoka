<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Connect;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use Pasoka\Component\Connect\Interfaces\DataBaseInterface;

/**
 *
 * @author     Michel Araujo - michelaraujopinto@gmail.com
 * @package    Pasoka
 * @subpackage Component\Connect
 * @namespace  Pasoka\Component\Connect
 * @version    1.0.0
 */
final class Connection
{

    /**
     *
     * @param array $pathEntity
     * @param array $dbParams
     * @param  bool $mode
     * @return EntityManager
     */
    public static function create(
        array $pathEntity,
        array $dbParams,
        $mode = false
    )
    {
        return EntityManager::create(
            $dbParams,
            Setup::createAnnotationMetadataConfiguration(
                $pathEntity,
                $mode
            )
        );
    }
}