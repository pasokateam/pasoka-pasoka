<?php

namespace Pasoka\Component\Connect;

use Pasoka\Component\Connect\Interfaces\DataBaseInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;

/**
 * Class DataBase
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package vendor\pasoka\src\Pasoka\Component\Connect
 */
final class DataBase implements DataBaseInterface
{

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var SchemaTool
     */
    private $schemaTool;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->schemaTool = new SchemaTool($this->entityManager);
    }

    /**
     * @return array
     */
    public function getClasses()
    {
        return $this
            ->entityManager
            ->getMetadataFactory()
            ->getAllMetadata();
    }

    /**
     * Cria tabelas no banco de dados
     *
     */
    public function create()
    {
        $this->drop();
        $this->update();
    }


    /**
     * Atualiza tabelas no banco de dados
     */
    public function update()
    {
        $this->schemaTool->updateSchema(
            $this->getClasses()
        );
    }

    /**
     * deleta banco de dados
     */
    public function drop()
    {
        $this->schemaTool->dropSchema(
            $this->getClasses()
        );
    }
} 