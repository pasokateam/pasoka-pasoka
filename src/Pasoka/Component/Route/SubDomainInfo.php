<?php

namespace Pasoka\Component\Route;

/**
 * Class SubDomainInfo
 *
 * @package Pasoka\Component\Route
 */
class SubDomainInfo
{

    const METHOD_NOT_FOUND = 0;
    const METHOD_REDIRECT = 1;
    const METHOD_JOIN = 2;

    /**
     * @var null|string
     */
    private $subDomain;

    /**
     * @var null|string
     */
    private $routePrefix;

    /**
     * @var string
     */
    private $module;

    /**
     * @var int
     */
    private $outRouteMethod;

    /**
     * SubDomainInfo constructor.
     *
     * @param string $subDomain
     * @param string $routePrefix
     * @param string $module
     */
    public function __construct($subDomain = null, $routePrefix = null, $module = null)
    {
        $this->subDomain = $subDomain;
        $this->routePrefix = $routePrefix;
        $this->module = $module;
        $this->outRouteMethod = self::METHOD_NOT_FOUND;
    }

    /**
     * @return mixed
     */
    public function getSubDomain()
    {
        return $this->subDomain;
    }

    /**
     * @param string $subDomain
     * @return $this
     */
    public function setSubDomain($subDomain)
    {
        $this->subDomain = $subDomain;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoutePrefix()
    {
        return $this->routePrefix;
    }

    /**
     * @param string $routePrefix
     * @return $this
     */
    public function setRoutePrefix($routePrefix)
    {
        $this->routePrefix = $routePrefix;
        return $this;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     * @return $this
     */
    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @return $this
     */
    public function useMethodJoin()
    {
        $this->outRouteMethod = self::METHOD_JOIN;
        return $this;
    }

    /**
     * @return $this
     */
    public function useMethodRedirect()
    {
        $this->outRouteMethod = self::METHOD_REDIRECT;
        return $this;
    }

    /**
     * @return $this
     */
    public function useMethodNotFound()
    {
        $this->outRouteMethod = self::METHOD_NOT_FOUND;
        return $this;
    }

    /**
     * @return int
     */
    public function getMethod()
    {
        return $this->outRouteMethod;
    }
}