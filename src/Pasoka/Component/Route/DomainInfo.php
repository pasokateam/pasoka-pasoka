<?php

namespace Pasoka\Component\Route;

/**
 * Class DomainInfo
 *
 * @package Pasoka\Component\Route
 */
class DomainInfo
{

    /**
     * @var string
     */
    protected $local;

    /**
     * @var string
     */
    protected $beta;

    /**
     * @var string
     */
    protected $production;

    /**
     * DomainInfo constructor.
     *
     * @param string $local
     * @param string $beta
     * @param string $production
     */
    public function __construct($local = null, $beta = null, $production = null)
    {
        $this->local = $local;
        $this->beta = $beta;
        $this->production = $production;
    }

    /**
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * @param string $local
     * @return $this
     */
    public function setLocal($local)
    {
        $this->local = $local;
        return $this;
    }

    /**
     * @return string
     */
    public function getBeta()
    {
        return $this->beta;
    }

    /**
     * @param string $beta
     * @return $this
     */
    public function setBeta($beta)
    {
        $this->beta = $beta;
        return $this;
    }

    /**
     * @return string
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * @param string $production
     * @return $this
     */
    public function setProduction($production)
    {
        $this->production = $production;
        return $this;
    }
}