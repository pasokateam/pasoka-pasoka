<?php

namespace Pasoka\Component\Route;

use Pasoka\Component\Route\Interfaces\ControllerInterface;

/**
 * Class AbstractController
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Route
 */
abstract class AbstractController implements ControllerInterface
{

    protected $request;

    public function __construct()
    {

    }

} 