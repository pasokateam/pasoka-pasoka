<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Route;

use Pasoka\Component\Http\Request\Request;

/**
 * Class Router
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Route
 */
final class Router
{

    /**
     * @var string
     */
    protected $domain;

    /**
     * @var bool
     */
    protected $useSubDomain = false;

    /**
     * @var SubDomainInfo[]
     */
    protected $subDomains;

    /**
     * Route Interface path
     *
     * @var string
     */
    const CONTROLLER_INTERFACE = 'Pasoka\Component\Route\Interfaces\ControllerInterface';

    /**
     * Router
     *
     * @access private
     * @var Router
     */
    private static $instance;

    /**
     * Construct
     *
     * @access public
     */
    private function __construct()
    {
    }

    /**
     * Singleton
     *
     * @access public
     * @return Router
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }


    /**
     * @param string $domain
     * @return $this
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     *
     * index.php config:
     *
     * $routes->setDomain("domain")
     *   ->useSubDomain()
     *   ->addSubDomain(
     *       (new \Pasoka\Component\Route\SubDomainInfo())
     *          ->useMethodNotFound()
     *          ->setSubDomain("test")
     *          ->setRoutePrefix("test")
     *          ->setModule(\Bootstrap::getPathSrc("Example"))
     *   );
     *
     *
     * routes.json config:
     *
     * ALIAS: test\/info\/([a-z]*)\/([0-9]*)\/?
     * URL DOMAIN: www.domain.com/test/info/(name)/(age)
     * URL SUB: test.domain.com/info/(name)/(age)
     *
     *
     * "subDomain": {
     *    "name": "test",
     *    "params": {
     *      "name": 2,
     *      "age": 3
     *    }
     * },
     *
     * @return $this
     */
    public function useSubDomain()
    {
        $this->useSubDomain = true;
        return $this;
    }


    /**
     * @param SubDomainInfo $subDomainInfo
     * @return $this
     */
    public function addSubDomain(SubDomainInfo $subDomainInfo)
    {
        $this->subDomains[] = $subDomainInfo;
        return $this;
    }

    /**
     * Validate route
     *
     * @param string $class
     * @param string $method
     * @return bool
     */
    public static function isRoutable($class = null, $method = null)
    {

        if (is_null($class) || is_null($method)) {
            return false;
        }

        if (!class_exists($class)) {
            return false;
        }

        $reflection = new \ReflectionClass($class);

        if ($reflection->hasMethod($method) &&
            $reflection->implementsInterface(self::CONTROLLER_INTERFACE)
        ) {
            return true;
        }

        return false;
    }


    /**
     * Get routes by module
     *
     * @param string $module
     * @return array
     * @throws \Exception
     */
    public function getModuleRoutes($module)
    {

        if (!is_dir($module)) {
            throw new \Exception("Module path {$module} not found");
        }

        $routes = @json_decode(
            @file_get_contents(
                "{$module}/routes.json"
            ), true
        );

        if ($routes === false) {
            throw new \Exception("routes.json not found in {$module}");
        }

        return $routes;
    }

    /**
     * Get subDomain name
     *
     * @return string
     */
    public function getSubDomainName()
    {
        $url = $_SERVER['HTTP_HOST'];
        $url = explode(".", $url);
        $url = array_shift($url);
        $url = strtolower($url);
        return $url;
    }


    /**
     * @param string $name
     * @param array $subDomain
     * @return bool
     */
    protected function checkSubDomainName($name = null, Array $subDomain = [])
    {

        if (is_null($name)) {
            return false;
        }

        if (isset($subDomain["name"]) && $subDomain["name"] == $name) {
            return true;
        }

        return false;
    }


    /**
     * Redirect domain
     */
    protected function redirectDomain()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {

            $url = $_SERVER["HTTP_HOST"];
            $uri = $_SERVER["REQUEST_URI"];
            $urlRedirect = str_replace($this->getSubDomainName(), "www", $url);
            header("Location: http://{$urlRedirect}{$uri}");
            exit;
        }
    }


    /**
     * Execute controller
     *
     * @param string $class
     * @param string $method
     *
     * @return mixed
     */
    public function run($class = null, $method = null)
    {

        $subDomainName = $this->getSubDomainName();
        $defaultDomain = ["www", ""];

        // use subDomain
        if ($this->useSubDomain && in_array($subDomainName, $defaultDomain) === false) {

            $path = $_SERVER['REQUEST_URI'];
            $found = false;

            foreach ($this->subDomains as $subDomain) {

                if ($subDomainName !== $subDomain->getSubDomain()) {
                    continue;
                }

                $routes = $this->getModuleRoutes($subDomain->getModule());

                foreach ($routes as $route) {

                    // check subDomain name
                    if (isset($route["subDomain"]) && $this->checkSubDomainName($subDomainName, $route["subDomain"])) {

                        $alias = $route["alias"];
                        $alias = str_replace([$subDomain->getRoutePrefix(), "?"], "", $alias);
                        $alias = trim($alias, '/');
                        $path = trim($path, '/');
                        $path = explode("?", $path);
                        $path = $path[0];

                        // check route
                        if (preg_match("~^{$alias}$~", $path)) {

                            $class = $route["class"];
                            $method = $route["method"];
                            $params = explode("/", $path);
                            $found = true;

                            // mapping route params
                            if (isset($route["subDomain"]["params"])) {
                                $sdParams = $route["subDomain"]["params"];
                                foreach ($sdParams as $key => $value) {
                                    if (isset($params[$value - 1])) {
                                        $_GET[$key] = $params[$value - 1];
                                    }
                                }
                            }

                            break 2;
                        }
                    }
                }

                switch ($subDomain->getMethod()) {
                    case SubDomainInfo::METHOD_NOT_FOUND:
                        if ($found === false) {
                            return false;
                        }
                        break;
                    case SubDomainInfo::METHOD_REDIRECT:
                        $this->redirectDomain();
                        break;
                }
            }
        }

        // run route
        $class = $class != null ? str_replace(".", "\\", $class) : null;
        if (self::isRoutable($class, $method)) {
            (new $class())
                ->$method(new Request());
            return true;
        }
        return false;
    }
}