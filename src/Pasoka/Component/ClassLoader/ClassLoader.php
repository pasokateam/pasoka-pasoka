<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\ClassLoader;

/**
 * Classe responsável por configurar e fazer o autoload das classes
 *
 * @author    Guilherme Santos - guilhermedossantos91@gmail.com
 * @author    Michel Araujo  - araujo_michel@yahoo.com.br
 * @package   PasokaComponent\ClassLoader
 * @namespace Pasoka\Component\ClassLoader
 * @version   1.0.0
 */
final class ClassLoader
{
    /**
     * Instancia da classe (singleton)
     *
     * @access private
     * @var ClassLoader
     */
    public static $instance = null;

    /**
     * Lista de pacotes do projeto
     *
     * @access public
     * @var string[]
     */
    private $projectPackages = [];

    /**
     * Lista de module importadas
     *
     * @access private
     * @var string[]
     */
    private $projectClasses = [];

    /**
     * Construtor
     *
     * @access private
     */
    private function __construct($packages = [])
    {
        $this->projectPackages = $packages;
    }

    /**
     * Obtem lista de module rodando
     *
     * @access public
     * @return string[]
     */
    public function getNameClass()
    {
        return $this->projectClasses;
    }

    /**
     * Faz autoload das classes
     *
     * @access public
     */
    public function load()
    {
        $this->setIncludePath();
        spl_autoload_register(function ($name) {
            self::getInstance()->append($name);
        });
    }

    /**
     * Metodo responsavel por incluir o diretorio setado no bootstrap
     * no path do sistema
     *
     * @access public
     * @return void
     */
    public function setIncludePath()
    {
        set_include_path(implode(PATH_SEPARATOR, array(
            PATH_ROOT . '/' . $this->projectPackages[0],
            get_include_path(),
        )));
    }

    /**
     * Faz require da classe.
     *
     * @access public
     * @param string $namespace
     * @return bool
     */
    public function append($namespace)
    {
        $filename = strtr($namespace, '\\', DIRECTORY_SEPARATOR) . '.php';
        foreach (explode(PATH_SEPARATOR, get_include_path()) as $path) {
            $this->projectClasses[] = $path .= DIRECTORY_SEPARATOR . $filename;

            if (is_file($path)) {
                require_once $path;
                return true;
            }
        }
        return false;
    }

    /**
     * Retorna a instancia da classe
     *
     * @access public
     * @param string[] $packages
     * @return ClassLoader
     */
    public static function getInstance(array $packages = [])
    {
        // verifica se o loader ja foi instanciado e inicia instancia
        if (self::$instance == null) {
            self::$instance = new self($packages);
        }

        return self::$instance;
    }

    /**
     * Retorna string com o nome das classes carregadas
     *
     * @access public
     * @return string
     */
    public function __toString()
    {
        return implode(", ", $this->projectClasses);
    }
}