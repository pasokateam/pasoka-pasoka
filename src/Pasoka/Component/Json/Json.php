<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Json;

use Pasoka\Component\Http\Response\Header;
use Pasoka\Component\Http\Response\Interfaces\ResponseInterface;

/**
 * Classe para trabalhar com JSON, converter dados em JSON.
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Component\JSON
 * @namespace  Pasoka\Component\JSON
 * @version    1.6.0
 */
class Json implements ResponseInterface
{

    const TYPE_STRING = 1;
    const TYPE_OBJECT = 2;
    const TYPE_ARRAY = 3;
    const INVALID = -1;

    /**
     * @var string|array|\stdClass
     */
    private $data;

    /**
     * @var bool
     */
    private $force;

    /**
     * Construtor
     *
     * @access public
     * @param array|\stdClass|string $data
     * @param bool                   $force
     */
    public function __construct($data = null, $force = false)
    {
        $this->setData($data, $force);
    }


    /**
     * @param string $file
     * @return \Pasoka\Component\Json\Json
     */
    public static function createFromFile($file)
    {
        $json = new self();
        $json->load($file);
        return $json;
    }

    /**
     * @param array|\stdClass|string $data
     * @param bool                   $force
     * @return $this
     */
    public function setData($data = null, $force = false)
    {
        $this->force = $force;
        switch ($this->validator($data)) {
            case self::TYPE_STRING:
            case self::TYPE_ARRAY:
            case self::TYPE_OBJECT:
                $this->data = $data;
                break;
            default:
                $this->data = new \stdClass();
                break;
        }
        return $this;
    }

    /**
     * @param $data
     * @return int
     */
    public function validator($data)
    {
        if (is_array($data)) {
            return self::TYPE_ARRAY;
        } else if ($data instanceof \stdClass) {
            return self::TYPE_OBJECT;
        } else if (is_string($data)) {
            return self::TYPE_STRING;
        }
        return self::INVALID;
    }


    /**
     * Transforma um array ou objeto stdClass em uma string JSON
     *
     * @access public
     * @param int $options
     * @throws \InvalidArgumentException
     * @return string|null
     */
    public function encode($options = null)
    {
        if (is_int($options) || $options != null) {
            throw new \InvalidArgumentException("Argument invalid, not a integer.");
        }
        if (is_string($this->data) && json_decode($this->data)) {
            return $this->data;
        } else if (is_string($this->data) && !json_decode($this->data) && $this->force === true) {
            $json = new \stdClass();
            $json->content = $this->data;
            $json->time = (new \DateTime('NOW'))->format("Y-m-d H:i:s");
            return json_encode($json);
        } else if (is_string($this->data) && !json_decode($this->data)) {
            return "[]";
        }
        return $this->data = json_encode($this->data, $options);
    }

    /**
     * Transforma uma string JSON em um Objeto/Array
     *
     * @access public
     * @return mixed|null
     */
    public function decode()
    {
        if (is_string($this->data) && json_decode($this->data)) {
            $this->data = json_decode($this->data);
        }
        return $this->data;
    }

    /**
     * Reponse JSON, finaliza script com reposta em application/json
     *
     * @access public
     * @param boolean $nocache
     */
    public function display($nocache = false)
    {
        $header = new Header();
        if ($nocache === true) {
            $header
                ->add('Cache-Control: no-cache, must-revalidate')
                ->add('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        }
        $header
            ->add('Content-type: application/json')
            ->run();
        die(self::encode());
    }


    /**
     * Carrega json de um arquivo
     *
     * @param string $file
     */
    public function load($file)
    {
        $data = [];
        if (is_file($file)) {
            $data = file_get_contents($file);
        }
        if (is_string($data) && json_decode($data)) {
            $data = json_decode($data);
        } else {
            $data = [];
        }
        $this->data = $data;
    }


    /**
     * Salva arquivo JSON
     *
     * @access public
     * @param string $file
     * @return boolean
     */
    public function save($file)
    {
        $json = self::encode();
        $path = explode(DIRECTORY_SEPARATOR, $file);
        array_pop($path);
        $path = implode(DIRECTORY_SEPARATOR, $path);
        if (!is_dir($path)) {
            return false;
        }

        if (is_writable($file) || !is_file($file)) {
            $rt = new \SplFileObject($file, "w+");
            $rt->fwrite($json);
            $rt = null;
            if (is_file($file) && chmod($file, 0664)) {
                return true;
            }
        }
        return false;
    }
}