<?php
namespace Pasoka\Component\Pagination;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;

/**
 * Class Pagination
 *
 * @autor Michel Araujo
 * @package Pasoka\Component\Pagination
 */
class Pagination
{
    /**
     * Pagina atual em que a listagem esta
     * @var int
     */
    private $currentPage;

    /**
     * A quantidade máxima de noticia que vai ser listada
     * @var int
     */
    private $maxNewsInPage;

    /**
     * A quantidade total de noticias para fazer a validação no template
     * @var int
     */
    private $totalPagesView;

    /**
     * A variavel maxPag define a quantidade de numeros que vai aparecer na paginação
     * @var int
     */
    private $maxPag;

    /**
     * @param int $currentPage
     * @param int $maxNewsInPage
     * @param int $maxPag
     */
    public function __construct(
        $currentPage,
        $maxNewsInPage = 10,
        $maxPag = 3
    ) {
        if (is_null($currentPage) || $currentPage <= 0) {
            $this->currentPage = 1;
        } else {
            $this->currentPage = $currentPage;
        }

        $this->maxNewsInPage = $maxNewsInPage;
        $this->maxPag = $maxPag;
    }

    /**
     * Retorna um array com os numeros da paginação
     *
     * @param int $totalResponse
     * @return array
     */
    public function getPages($totalResponse)
    {
        if (!is_int($totalResponse)) {
            throw new InvalidArgumentException('Tipo do parametro $totalResponse invalido!');
        }

        $totalPages = $this->totalPagesView = ceil($totalResponse / $this->maxNewsInPage);

        /**
         * Essa logica faz com que mostre sempre a quantidade de números da paginação
         * desejado deixando a pagina atual sempre no meio
         * (3"4"5) pagina atual = 4
         */
        if ($this->currentPage >= $this->maxPag) {
            $pagingNumber = $this->currentPage - 1;
        } else {
            $pagingNumber = 1;
        }

        $pages = [];
        while ($totalPages > 0) {
            $pages[] = $pagingNumber;

            $this->maxPag--;
            if ($this->maxPag == 0 || $pagingNumber >= $this->totalPagesView) {
                break;
            }

            $pagingNumber++;
            $totalPages--;
        }

        return $pages;
    }

    /**
     * Retorna a pagina atual
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * Retorna o resultado total
     * @return int
     */
    public function getTotalPagesView()
    {
        return $this->totalPagesView;
    }
} 