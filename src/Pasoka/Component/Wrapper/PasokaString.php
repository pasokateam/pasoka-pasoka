<?php
/***
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Wrapper;

/**
 * Classe Wrapper de dataWrapper
 *
 * @author     Michael Dias - michael@climatempo.com.br
 * @author     Adilson Ferreira - adilson@climatempo.com.br
 * @package    Pasoka
 * @subpackage Component\Wrapper
 * @namespace  Pasoka\Component\Wrapper
 * @version    1.1.0
 */
class PasokaString extends AbstractWrapper
{
    /**
     * Retorna o nome da classe.
     *
     * @return string
     */
    protected function getClassName()
    {
        return __CLASS__;
    }

    /**
     * Converte em minúsculas todos os caracteres da string.
     *
     * @return PasokaString
     */
    public function toLowerCase()
    {
        $this->dataWrapper = strtolower($this->dataWrapper);
        return $this;
    }

    /**
     * Converte em maiúsculas todos os caracteres da string.
     *
     * @return PasokaString
     */
    public function toUpperCase()
    {
        $this->dataWrapper = strtoupper($this->dataWrapper);
        return $this;
    }

    /**
     * Extrai parte da string.
     *
     * @param int $start
     * @param int $length
     * @return PasokaString
     */
    public function substr($start, $length = null)
    {
        if (is_int($length)) {
            $this->dataWrapper = substr($this->dataWrapper, $start, $length);
        } else {
            $this->dataWrapper = substr($this->dataWrapper, $start);
        }

        return $this;
    }

    /**
     * Insere na string barras invertidas antes dos caracteres que precisam
     * ser escapados (aspas simples, aspas duplas e barras invertidas).
     *
     * @return PasokaString
     */
    public function addslashes()
    {
        $this->dataWrapper = addslashes($this->dataWrapper);
        return $this;
    }

    /**
     * Converte o primeiro caractere da string em maiúsculo.
     *
     * @return PasokaString
     */
    public function ucfirst()
    {
        $this->dataWrapper = ucfirst($this->dataWrapper);
        return $this;
    }

    /**
     * Converte o primeiro caractere de cada palavra da string em maiúsculo.
     *
     * @return PasokaString
     */
    public function ucwords()
    {
        $this->dataWrapper = ucwords($this->dataWrapper);
        return $this;
    }

    /**
     * Repete a string $multiplier vezes.
     *
     * @param int $multiplier
     * @return PasokaString
     */
    public function repeat($multiplier)
    {
        $this->dataWrapper = str_repeat($this->dataWrapper, $multiplier);
        return $this;
    }

    /**
     * Retorna uma matriz de substrings, resultado da divisão da string
     * pelo delimitador $delimiter.
     *
     * @param string $delimiter
     * @return array
     */
    public function explode($delimiter)
    {
        return explode($delimiter, $this->dataWrapper);
    }

    /**
     * Substitui todas as ocorrências de $search para $replace na string.
     *
     * @param string $search
     * @param string $replace
     * @return PasokaString
     */
    public function replace($search, $replace)
    {
        $this->dataWrapper = str_replace($search, $replace, $this->dataWrapper);
        return $this;
    }

    /**
     * Retorna uma matriz com a primeira ocorrência de $pattern na string.
     *
     * @param string $pattern
     * @return array
     */
    public function match($pattern)
    {
        preg_match($pattern, $this->dataWrapper, $matches);
        return $matches;
    }

    /**
     * Retorna uma matriz com todas as ocorrências de $pattern na string.
     *
     * @param string $pattern
     * @return array
     */
    public function matchAll($pattern)
    {
        preg_match_all($pattern, $this->dataWrapper, $matches);
        return $matches;
    }

    /**
     * Encripta a string utilizando o algoritmo de hash "md5".
     *
     * @param bool $raw_output
     * @return PasokaString
     */
    public function md5($raw_output = false)
    {
        $this->dataWrapper = md5($this->dataWrapper, $raw_output);
        return $this;
    }

    /**
     * Encripta a string utilizado o algoritmo de hash "sha1".
     *
     * @param bool $raw_output
     * @return PasokaString
     */
    public function sha1($raw_output = false)
    {
        $this->dataWrapper = sha1($this->dataWrapper, $raw_output);
        return $this;
    }

    /**
     * Mistura os caracteres da string de forma aleatória.
     *
     * @return PasokaString
     */
    public function shuffle()
    {
        $this->dataWrapper = str_shuffle($this->dataWrapper);
        return $this;
    }

    /**
     * Remove espaços no início e final da string.
     *
     * @return PasokaString
     */
    public function trim()
    {
        $this->dataWrapper = trim($this->dataWrapper);
        return $this;
    }

    /**
     * Remove espaços no início da string.
     *
     * @return PasokaString
     */
    public function ltrim()
    {
        $this->dataWrapper = ltrim($this->dataWrapper);
        return $this;
    }

    /**
     * Remove espaços no final da string.
     *
     * @return PasokaString
     */
    public function rtrim()
    {
        $this->dataWrapper = rtrim($this->dataWrapper);
        return $this;
    }

    /**
     * Retorna a quantidade de caracteres da string.
     *
     * @return int
     */
    public function length()
    {
        return strlen($this->dataWrapper);
    }
}
