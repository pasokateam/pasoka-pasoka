<?php
/***
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Wrapper;

/**
 * Super Classe Wrapper
 *
 * @author Guilherme Santos - guilhermedossantos91@gmail.com
 * @author Michael Dias - michael@climatempo.com.br
 * @package Pasoka
 * @subpackage Component\Wrapper
 * @namespace Pasoka\Component\Wrapper
 * @version 1.0.0
 */
abstract class AbstractWrapper
{
    /**
     * Tipos primitivos aceitos no construtor de cada classe que
     * extende a Wrapper.
     */
    private $type = [
        'string',
        'integer',
        'double',
        'boolean'
    ];

    /**
     * Método que retorna o nome da classe que está implementando.
     */
    abstract protected function getClassName();

    /**
     * Tipo primitivo que será convertido em uma classe Wrapper.
     */
    protected $dataWrapper;

    /**
     * Método construtor.
     *
     * @param mixed|null $dataWrapper
     */
    public function __construct($dataWrapper)
    {
        try {
            if (!in_array(gettype($dataWrapper), $this->type)) {
                throw new \Exception('Invalid param.');
            }
            $this->dataWrapper = $this->casting($this->getType(), $dataWrapper);
        } catch (\Exception $e) {
            die('Message : ' . $e->getMessage() . '<br />' . 'File : ' . $e->getFile() . '<br />' . 'Line : ' . $e->getLine());
        }
    }

    /**
     * Retorna a string do objeto.
     *
     * @return mixed
     */
    public function __toString()
    {
        return (string)$this->dataWrapper;
    }

    /**
     * Retorna o tipo primitivo do objeto.
     *
     *
     * @return string
     */
    public function valueOf()
    {
        return $this->casting($this->getType(), $this->dataWrapper);
    }

    /**
     * Retorna no nome da classe referente a um tipo
     * Exemplo: string, int, float...
     *
     * @return mixed
     */
    public function getType()
    {
        $type = static::getClassName();
        $type = strtolower($type);
        $type = explode("\\", $type);
        $type = array_pop($type);
        return $type;
    }

    /**
     * Faz o casting da classe instanciada
     *
     * @param string $type
     * @param mixed $data
     * @return string|number|NULL
     */
    private function casting($type, $data)
    {
        switch ($type) {
            case 'string':
                return (string)$data;
            case 'int':
                return (int)$data;
            case 'real':
                return (real)$data;
            case 'float':
                return (float)$data;
            case 'double':
                return (double)$data;
            case 'boolean':
                return (boolean)$data;
            default:
                return null;
        }
    }
}