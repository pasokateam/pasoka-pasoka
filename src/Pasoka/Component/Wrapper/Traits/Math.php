<?php
namespace Pasoka\Component\Wrapper\Traits;

/**
 * @author michel       
 */
trait Math
{
	/**
	 * Faz a soma do valor atual do objeto int com um
	 * valor passado por parâmetro
	 *
	 * @param unknown $number
	 * @throws \InvalidArgumentException
	 * @return number
	 */
	public function sum($number)
	{
		$this->validTypeNumber($number);
		return $this->dataWrapper = $this->dataWrapper + $number;
	}
	
	/**
	 * Faz a subtração do valor atual do objeto int com um
	 * valor passado por parâmetro
	 *
	 * @param unknown $number
	 * @return number
	 */
	public function subtraction($number)
	{
		$this->validTypeNumber($number);
		return $this->dataWrapper = $this->dataWrapper - $number;
	}
	
	/**
	 * Faz a multiplicação do valor atual com o valor passado por parâmetro
	 *
	 * @param unknown $number
	 * @throws \InvalidArgumentException
	 * @return number
	 */
	public function multiply($number)
	{
		$this->validTypeNumber($number);
		return $this->dataWrapper = $this->dataWrapper * $number;
	}
	
	/**
	 * Faz a divisão do valor atual com o valor passado por parâmetro
	 *
	 * @param unknown $number
	 * @throws \ErrorException
	 * @return number
	 */
	public function division($number)
	{
		$this->validTypeNumber($number);
	
		if ($this->dataWrapper == 0 || $number == 0) {
			throw new \ErrorException("Impossible division by zero");
			exit();
		}
	
		return $this->dataWrapper = $this->dataWrapper / $number;
	}
	
	/**
	 * Calcula o modulo da divisão do valor atual com o valor
	 * passado por parâmetro
	 *
	 * @param unknown $number
	 * @throws \ErrorException
	 * @return number
	 */
	public function modDivision($number)
	{
		$this->validTypeNumber($number);
	
		if ($this->dataWrapper == 0 || $number == 0) {
			throw new \ErrorException("Impossible division by zero");
			exit();
		}
	
		return $this->dataWrapper % $number;
	}
	
	/**
	 * Calcula o Exponential do valor atual com o valor
	 * passado por parâmetro
	 *
	 * @param unknown $number
	 * @return number
	 */
	public function pow($number)
	{
		$this->validTypeNumber($number);
		return pow($this->dataWrapper, $number);
	}
	
	/**
	 * Verifica se o tipo do parâmetro e valido
	 *
	 * @param unknown $number
	 * @throws \InvalidArgumentException
	 */
	public function validTypeNumber($number)
	{
		if (!is_numeric($number)) {
			throw new \InvalidArgumentException("Argument must be a number");
			exit();
		}
	}
}