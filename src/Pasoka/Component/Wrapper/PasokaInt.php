<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Wrapper;

use Pasoka\Component\Wrapper\Interfaces\NumberInterface;
use Pasoka\Component\Wrapper\Traits\Math;

/**
 * Classe Wrapper de dataWrapper
 *
 * @author Michel Araujo - araujo_michel@yahoo.com.br
 * @author  Adilson Ferreira - adilson@climatempo.com.br
 * @package Pasoka
 * @subpackage Component\Wrapper
 * @namespace Pasoka\Component\Wrapper
 * @version 1.1.0
 */
class PasokaInt extends AbstractWrapper implements NumberInterface
{
	use Math;
	
	/**
	 * Retorna o nome da classe.
	 *
	 * @return string
	 */
	protected function getClassName()
	{
		return __CLASS__;
	}
}