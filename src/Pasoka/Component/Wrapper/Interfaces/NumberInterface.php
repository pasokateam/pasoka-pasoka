<?php
namespace Pasoka\Component\Wrapper\Interfaces;

/**
 * @author michel      
 */
interface NumberInterface
{
	public function sum($number);
	public function subtraction($number);
	public function multiply($number);
	public function division($number);
	public function modDivision($number);
	public function pow($number);
	public function validTypeNumber($number);
}