<?php
namespace Pasoka\Component\WeatherApi;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Cache\Hash;

/**
 * Class Cache
 *
 * @package Pasoka\Component\WeatherApi
 */
class Cache
{

    /**
     * @param \stdClass $data
     * @return bool
     */
    public static function validate($data)
    {
        if (!$data instanceof \stdClass) {
            return false;
        }

        if (!isset($data->response)) {
            return false;
        }

        if (!isset($data->response->success)) {
            return false;
        }

        if ($data->response->success == false) {
            return false;
        }

        return true;
    }


    /**
     * @param FileCache   $fileCache
     * @param Hash|string $id
     * @param \stdClass   $data
     * @throws \Exception
     * @return array
     */
    public static function save(FileCache $fileCache, $id, $data = null)
    {
        if (!self::validate($data)) {
            if ($fileCache->exists($id)) {
                return $fileCache->fetch($id);
            }
        }

        $fileCache->add($id, $data);
        return $data;

    }
}