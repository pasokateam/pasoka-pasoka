<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Observed;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class FogObserved
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Observed
 */
class FogObserved extends AbstractRetrieve
{

    /**
     * @param int idLocation Locale to search
     * @param string clearCache
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation, $clearCache = null)
    {
        return $this
            ->setRouter(['observed', 'fog'])
            ->addQueryString("?idLocale={$idLocation}")
            ->manageCache(
                WeatherApi::FETCH_FOG_OBSERVED  . '_' .
                $idLocation,
                $clearCache
            );
    }
}