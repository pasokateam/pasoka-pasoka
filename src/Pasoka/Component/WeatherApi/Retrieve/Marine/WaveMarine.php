<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Marine;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class WaveMarine
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Marine
 */
class WaveMarine extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @return array|\stdClass
     */
    public function getByPeriod($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['marine', 'wave'])
            ->addQueryString($queryString)
            ->manageCache(WeatherApi::FETCH_WAVE_MARINE . '_' . $idLocation . '_' . $dateBegin . '_' . $dateEnd);
    }
}