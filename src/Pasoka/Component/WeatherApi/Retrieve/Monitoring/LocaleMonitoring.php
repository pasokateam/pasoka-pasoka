<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Monitoring;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class LocaleMonitoring
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Monitoring
 */
class LocaleMonitoring extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function get()
    {
        return $this
            ->setRouter(['monitoring', 'locale'])
            ->manageCache(WeatherApi::FETCH_LOCALE_MONITORING);
    }
} 