<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Monitoring;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class LightningMonitoring
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Monitoring
 */
class LightningMonitoring extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function get()
    {
        return $this
            ->setRouter(['monitoring', 'lightning'])
            ->manageCache(WeatherApi::FETCH_LIGHTNING_MONITORING);
    }

    /**
     * @param int idLocation Id da localidade a ser buscada
     * @param string clearCache
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation, $clearCache = null)
    {
        return $this
            ->setRouter(['monitoring', 'lightning', 'by-locale'])
            ->addQueryString("?localeId={$idLocation}")
            ->manageCache(
                WeatherApi::FETCH_LIGHTNING_MONITORING . '-' .
                $idLocation,
                $clearCache
            );
    }
} 