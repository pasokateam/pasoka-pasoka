<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Monitoring;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class SeasonMonitoring
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Monitoring
 */
class SeasonMonitoring extends AbstractRetrieve
{

    /**
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function get($clearCache = null)
    {
        return $this
            ->setRouter(['monitoring', 'season'])
            ->manageCache(WeatherApi::FETCH_SEASON_MONITORING, $clearCache);
    }
}