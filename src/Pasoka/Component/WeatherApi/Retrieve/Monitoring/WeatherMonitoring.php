<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Monitoring;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class WeatherMonitoring
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Monitoring
 */
class WeatherMonitoring extends AbstractRetrieve
{

    /**
     * @param int $idlocale
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getByIdlocale($idlocale, $clearCache = null)
    {
        return $this
            ->setRouter(['monitoring', 'weather'])
            ->addQueryString("?idlocale={$idlocale}")
            ->manageCache(WeatherApi::FETCH_WEATHER_MONITORING . '_' . $idlocale, $clearCache);
    }

    /**
     * Retorna cidade mais fria do brasil.
     *
     * Rota: /monitoring/lowest-temperature
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @version 1.0.0
     * @param null|string $clearCache
     * @return array|\stdClass
     */
    public function getLowestTemperature($clearCache = null)
    {
        return $this
            ->setRouter(['monitoring', 'lowest-temperature'])
            ->manageCache(
                WeatherApi::FETCH_WEATHER_MONITORING_LOWEST_TEMPERATURE,
                $clearCache
            );
    }

    /**
     * Retorna cidade mais quente do brasil.
     *
     * Rota: /monitoring/highest-temperature
     *
     * @author Adilson Ferreira <adilson@climatempo.com.br>
     * @version 1.0.0
     * @param null|string $clearCache
     * @return array|\stdClass
     */
    public function getHighestTemperature($clearCache = null)
    {
        return $this
            ->setRouter(['monitoring', 'highest-temperature'])
            ->manageCache(
                WeatherApi::FETCH_WEATHER_MONITORING_HIGHEST_TEMPERATURE,
                $clearCache
            );
    }
}