<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Monitoring;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class LightningCepMonitoring
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Monitoring
 */
class LightningCepMonitoring extends AbstractRetrieve
{

    /**
     * @param array $numbers
     * @return array|\stdClass
     */
    public function getByCep(array $numbers = [])
    {
        $cepNumber = 0;
        $queryString = '?num=';

        foreach ($numbers as $num) {
            $queryString .= $num . ',';
            $cepNumber .= $num;
        }

        return $this
            ->setRouter(['monitoring', 'lightning', 'cep'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_LIGHTNING_CEP_MONITORING . '_' . $cepNumber
            );
    }
} 