<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Satellite;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class AgriculturalSatellite
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Satellite
 */
class AgriculturalSatellite extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function get()
    {
        return $this
            ->setRouter(['satellite', 'agricultural'])
            ->manageCache(WeatherApi::FETCH_AGRICULTURAL_SATELLITE);
    }
} 