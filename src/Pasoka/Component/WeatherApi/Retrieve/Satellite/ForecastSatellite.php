<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Satellite;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastSatellite
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Satellite
 */
class ForecastSatellite extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function get()
    {
        return $this
            ->setRouter(['satellite', 'forecast'])
            ->manageCache(WeatherApi::FETCH_FORECAST_SATELLITE);
    }
} 