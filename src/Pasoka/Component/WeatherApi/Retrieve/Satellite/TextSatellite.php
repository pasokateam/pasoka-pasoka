<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Satellite;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class TextSatellite
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Satellite
 */
class TextSatellite extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function getAll()
    {
        return $this
            ->setRouter(['satellite', 'text'])
            ->manageCache($this->configAuthentication['CLIENT'] . WeatherApi::FETCH_TEXT_SATELLITE);
    }
}