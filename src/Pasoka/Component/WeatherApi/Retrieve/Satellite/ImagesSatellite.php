<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Satellite;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ImagesSatellite
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Satellite
 */
class ImagesSatellite extends AbstractRetrieve
{

    /**
     * @param string $locale
     * @return array|\stdClass
     */
    public function getByLocale($locale)
    {
        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString("?locale={$locale}")
            ->manageCache(WeatherApi::FETCH_IMAGES_SATELLITE . '_' . $locale);
    }


    /**
     * @param string $style
     * @return array|\stdClass
     */
    public function getByStyle($style)
    {
        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString("?style={$style}")
            ->manageCache(WeatherApi::FETCH_IMAGES_SATELLITE . '_' . $style);
    }


    /**
     * @param string $locale
     * @param string $style
     * @return array|\stdClass
     */
    public function getByLocaleAndStyle($locale, $style)
    {
        return $this
            ->setRouter(['satellite', 'images'])
            ->addQueryString("?locale={$locale}&style={$style}")
            ->manageCache(WeatherApi::FETCH_IMAGES_SATELLITE . '_' . $locale . '_' . $style);
    }
}