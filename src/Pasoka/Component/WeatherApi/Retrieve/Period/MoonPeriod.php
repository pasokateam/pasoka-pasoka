<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Period;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class MoonPeriod
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Period
 */
class MoonPeriod extends AbstractRetrieve
{

    /**
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getByPeriod($dateBegin, $dateEnd = null, $clearCache = null)
    {

        $queryString = "?&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'moon'])
            ->addQueryString($queryString)
            ->manageCache(WeatherApi::FETCH_MOON_PERIOD . '_' . $dateBegin . '_' . $dateEnd, $clearCache);
    }
} 