<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Period;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class MosquitoPeriod
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Period
 */
class MosquitoPeriod extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @return array|\stdClass
     */
    public function getByPeriod($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        
        return $this
            ->setRouter(['forecast', 'period', 'mosquito'])
            ->addQueryString($queryString)
            ->manageCache(WeatherApi::FETCH_MOSQUITO_PERIOD . '_' . $idLocation . '_' . $dateBegin . '_' . $dateEnd);
    }
}