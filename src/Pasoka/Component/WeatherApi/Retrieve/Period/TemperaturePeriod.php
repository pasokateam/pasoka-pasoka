<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Period;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class TemperaturePeriod
 *
 * @author Michel Araujo
 * @package Pasoka\Component\WeatherApi\Retrieve\Period
 * @version 1.0.0
 */
class TemperaturePeriod extends AbstractRetrieve
{
    /**
     * Retorna os dados de temperatura por periodo
     *
     * @author Michel Araujo
     * @param int $idLocation
     * @param String $dateBegin
     * @param String $dateEnd
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?localeId={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'temperature'])
            ->addQueryString($queryString)
            ->manageCache(WeatherApi::FETCH_TEMPERATURE_PERIOD
                . '_' . $idLocation . '_' . $dateBegin . '_' . $dateEnd);
    }
} 