<?php

namespace Pasoka\Component\WeatherApi\Retrieve\Period;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class RankingPeriod
 *
 * Classe para obter ranking de dados.
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Period
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @version 1.0.0
 */
class RankingPeriod extends AbstractRetrieve
{

    /**
     * Retorna ranking das menores temperatura minima
     *
     * Rota: ranking/lower/temperature/min
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @version 1.0.0
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getLowerTemperatureMin($clearCache = null)
    {
        return $this
            ->setRouter(['ranking', 'lower', 'temperature', 'min'])
            ->manageCache(
                WeatherApi::FETCH_RANKING_TEMP_MIN_PERIOD,
                $clearCache
            );
    }


    /**
     * Retorna ranking das menores temperatura maxima
     *
     * Rota: ranking/lower/temperature/max
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @version 1.0.0
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getLowerTemperatureMax($clearCache = null)
    {
        return $this
            ->setRouter(['ranking', 'lower', 'temperature', 'max'])
            ->manageCache(
                WeatherApi::FETCH_RANKING_TEMP_MAX_PERIOD,
                $clearCache
            );
    }

    /**
     * Retorna ranking das maiores temperaturas minimas
     *
     * Rota: ranking/higher/temperature/min
     *
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @version 1.0.0
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getHigherTemperatureMin($clearCache = null)
    {
        return $this
            ->setRouter(['ranking', 'higher', 'temperature', 'min'])
            ->manageCache(
                WeatherApi::FETCH_RANKING_HIGHER_TEMP_MIN_PERIOD,
                $clearCache
            );
    }

    /**
     * Retorna ranking das maiores temperaturas maximas
     *
     * Rota: ranking/higher/temperature/max
     *
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @version 1.0.0
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getHigherTemperatureMax($clearCache = null)
    {
        return $this
            ->setRouter(['ranking', 'higher', 'temperature', 'max'])
            ->manageCache(
                WeatherApi::FETCH_RANKING_HIGHER_TEMP_MAX_PERIOD,
                $clearCache
            );
    }

}