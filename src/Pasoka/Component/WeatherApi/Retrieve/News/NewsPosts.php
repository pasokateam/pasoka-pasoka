<?php
namespace Pasoka\Component\WeatherApi\Retrieve\News;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class NewsPosts
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\News
 */
class NewsPosts extends AbstractRetrieve
{

    /**
     * @param string $slug
     * @return array|\stdClass
     */
    public function getBySlug($slug)
    {
        return $this
            ->setRouter(['news'])
            ->addQueryString("?slug={$slug}")
            ->manageCache(WeatherApi::FETCH_NEWS_POSTS . '_slug_' . $slug);
    }

    /**
     * @param int $category
     * @param int $limit
     * @param int $paged
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getByCategory($category, $limit, $paged, $clearCache = null, $orderBy = null)
    {
        return $this
            ->setRouter(['news'])
            ->addQueryString("?category={$category}&limit={$limit}&paged={$paged}&orderBy={$orderBy}")
            ->manageCache(
                WeatherApi::FETCH_NEWS_POSTS . '_category_'
                . $category . '_paged_' . $paged . '_orderBy_' . $orderBy, $clearCache
            );
    }

    /**
     * @param string $tag
     * @return array|\stdClass
     */
    public function getByTag($tag)
    {
        return $this
            ->setRouter(['news'])
            ->addQueryString("?tag={$tag}")
            ->manageCache(WeatherApi::FETCH_NEWS_POSTS . '_tag_' . $tag);
    }


    /**
     * @param int $idType
     * @param int $limit
     * @param int $current
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getByType($idType, $limit = 5, $current = 0, $clearCache = null)
    {
        return $this
            ->setRouter(['news'])
            ->addQueryString("?typeNews={$idType}&limit={$limit}&current={$current}")
            ->manageCache(WeatherApi::FETCH_NEWS_POSTS .
                '_idType_' . $idType .
                '_limit_' . $limit .
                '_current_' . $current,
                $clearCache
            );
    }

    /**
     * @param int $idContent
     * @return array|\stdClass
     */
    public function getByIdContent($idContent)
    {
        return $this
            ->setRouter(['news'])
            ->addQueryString("?idconteudo={$idContent}")
            ->manageCache(WeatherApi::FETCH_NEWS_POSTS . '_idContent_' . $idContent);
    }


    /**
     * Retorna varias noticias com base em uma array de categorias passado por parametro
     *
     * @param array $categories
     * @param null $limit
     * @param null $paged
     * @return array|\stdClass
     */
    public function getByCategories(Array $categories, $limit = null, $paged = null)
    {
        $queryString = '';
        foreach ($categories as $key => $value) {
            if ($key == 0) {
                $queryString .= '?category[]=' . $value;
            } else {
                $queryString .= '&category[]=' . $value;
            }
        }

        $hashCache = WeatherApi::FETCH_NEWS_POSTS
            . '_categories_' . serialize($categories) . '_paged_' . $paged . '_limit_' . $limit;

        return $this
            ->setRouter(['news'])
            ->addQueryString("{$queryString}&limit={$limit}&paged={$paged}")
            ->manageCache($hashCache);
    }

    /**
     * Retorna varias noticias com base em uma array de tags passado por parametro
     *
     * @param array $tags
     * @param null $limit
     * @param null $paged
     * @return array|\stdClass
     */
    public function getByTags(Array $tags, $limit = null, $paged = null)
    {
        $queryString = '';
        foreach ($tags as $key => $value) {
            if ($key == 0) {
                $queryString .= '?tag[]=' . $value;
            } else {
                $queryString .= '&tag[]=' . $value;
            }
        }

        $hashCache = WeatherApi::FETCH_NEWS_POSTS
            . '_tags_' . serialize($tags) . '_paged_' . $paged . '_limit_' . $limit;

        return $this
            ->setRouter(['news'])
            ->addQueryString("{$queryString}&limit={$limit}&paged={$paged}")
            ->manageCache($hashCache);
    }

    /**
     * Retorna um array de noticias com base em um array de categoria e um array
     * de tags passado por parametro
     *
     * @param array $categories
     * @param array $tags
     * @param null $limit
     * @param null $paged
     * @return array|\stdClass
     */
    public function getByCategoriesAndTags(
        Array $categories,
        Array $tags,
        $limit = null,
        $paged = null
    )
    {
        $queryString = '';
        foreach ($tags as $key => $value) {
            if ($key == 0) {
                $queryString .= '?tag[]=' . $value;
            } else {
                $queryString .= '&tag[]=' . $value;
            }
        }

        foreach ($categories as $key => $value) {
            if ($key == 0) {
                $queryString .= '&category[]=' . $value;
            } else {
                $queryString .= '&category[]=' . $value;
            }
        }

        $hashCache = WeatherApi::FETCH_NEWS_POSTS
            . '_tags_' . serialize($tags)
            . '_categories_' . serialize($categories)
            . '_paged_' . $paged . '_limit_' . $limit;

        return $this
            ->setRouter(['news'])
            ->addQueryString("{$queryString}&limit={$limit}&paged={$paged}")
            ->manageCache($hashCache);
    }

    /**
     * Retorna noticias por periodo
     *
     * @param String $dateBegin
     * @param String $dateEnd
     * @return array|\stdClass
     */
    public function getByPeriod($dateBegin, $dateEnd)
    {
        $queryString = "?dateBegin={$dateBegin}&dateEnd={$dateEnd}";
        $hashCache = WeatherApi::FETCH_NEWS_POSTS
            . '_period_dateBegin_' . $dateBegin . '_dateEnd_' . $dateEnd;

        return $this
            ->setRouter(['news', 'period'])
            ->addQueryString($queryString)
            ->manageCache($hashCache);
    }
}