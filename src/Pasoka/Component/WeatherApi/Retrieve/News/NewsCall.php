<?php
namespace Pasoka\Component\WeatherApi\Retrieve\News;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class NewsCall
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\News
 */
class NewsCall extends AbstractRetrieve
{

    /**
     * @param int $type
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function getByType($type, $clearCache = null)
    {
        return $this
            ->setRouter(['news', 'calls'])
            ->addQueryString("?typeCall={$type}")
            ->manageCache(WeatherApi::FETCH_NEWS_CALL . '_' . $type, $clearCache);
    }
}