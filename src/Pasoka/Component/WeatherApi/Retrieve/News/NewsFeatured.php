<?php
namespace Pasoka\Component\WeatherApi\Retrieve\News;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class NewsFeatured
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\News
 */
class NewsFeatured extends AbstractRetrieve
{

    /**
     * @param string $clearCache
     * @return array|\stdClass
     */
    public function get($clearCache = null)
    {
        return $this
            ->setRouter(['news', 'featured'])
            ->manageCache(WeatherApi::FETCH_NEWS_FEATURED, $clearCache);
    }
}