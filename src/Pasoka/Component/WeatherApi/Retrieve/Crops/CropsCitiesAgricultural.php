<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Crops;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class CropsCitiesAgricultural
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Crops
 * @version 1.1
 */
class CropsCitiesAgricultural extends AbstractRetrieve
{
    /**
     * @param int $idLocation
     * @return \stdClass|array
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['crops', 'locale'])
            ->addQueryString("?idLocale={$idLocation}")
            ->manageCache(
                WeatherApi::FETCH_CROPS_CITIES_AGRICULTURAL . '_' .
                $idLocation
            );
    }
}