<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Hourly;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastHourly
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Hourly
 */
class ForecastHourly extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @param string $clearCache
     * @param null|string $timezone
     * @return array|\stdClass
     */
    public function get(
        $idLocation,
        $dateBegin,
        $dateEnd = null,
        $timezone = null,
        $clearCache = null
    ) {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'hourly'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_HOURLY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd . '_' .
                $timezone,
                $clearCache
            );
    }

    /**
     * @param double $latitude
     * @param double $longitude
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @param string $clearCache
     * @param null|string $timezone
     * @return array|\stdClass
     */
    public function getByLatLon(
        $latitude,
        $longitude,
        $dateBegin,
        $dateEnd = null,
        $timezone = null,
        $clearCache = null
    ) {
        $queryString = "?latitude={$latitude}&longitude={$longitude}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        } else {
            $queryString .= "&dateEnd={$dateBegin}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['geo', 'forecast', 'hour'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_HOURLY . '_' .
                $queryString,
                $clearCache
            );
    }
}