<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Hourly;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class HumidityHourly
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Hourly
 */
class HumidityHourly extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '72hours', 'humidity'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_HUMIDITY_HOURLY . '_' . $idLocation);
    }


    /**
     * @param int    $idLocation
     * @param String $dateBegin
     * @param String $dateEnd
     * @param null   $timezone
     * @return \stdClass|String
     */
    public function getByIdLocaleAndPeriod($idLocation, $dateBegin, $dateEnd = null, $timezone = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'humidity'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_HUMIDITY_HOURLY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd . '_' .
                $timezone
            );
    }
}