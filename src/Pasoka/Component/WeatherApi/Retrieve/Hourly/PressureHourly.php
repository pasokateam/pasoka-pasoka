<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Hourly;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class PressureHourly
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Hourly
 */
class PressureHourly extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string|null $timezone
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation, $timezone = null)
    {
        $queryString = "?idlocale={$idLocation}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', '72hours', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_PRESSURE_HOURLY . '_' .
                $idLocation . '_' .
                $timezone
            );
    }


    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @param string|null $timezone
     * @return array|\stdClass
     */
    public function getByIdLocaleAndPeriod($idLocation, $dateBegin, $dateEnd = null, $timezone = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }
        return $this
            ->setRouter(['forecast', 'hourly', 'pressure'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_PRESSURE_HOURLY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd . '_' .
                $timezone
            );
    }
}