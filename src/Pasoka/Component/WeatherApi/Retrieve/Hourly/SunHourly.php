<?php

namespace Pasoka\Component\WeatherApi\Retrieve\Hourly;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class SunHourly
 *
 * Classe para obter dados de sol da API.
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Hourly
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @version 1.0.0
 */
class SunHourly extends AbstractRetrieve
{

    /**
     * Retorna Insolacao horaria por localidade da API.
     *
     * ROTA: /forecast/72hours/sunshine-duration?localeId=XXXX
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @version 1.0.0
     * @param int $idLocation
     * @param null|string $clearCache
     * @return array|\stdClass
     */
    public function getSunshineDuration($idLocation, $clearCache = null)
    {
        $queryString = "?localeId={$idLocation}";

        return $this
            ->setRouter(['forecast', '72hours', 'sunshine-duration'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_SUNSHINE_HOURLY . '_' .
                $idLocation . '_',
                $clearCache
            );
    }
}