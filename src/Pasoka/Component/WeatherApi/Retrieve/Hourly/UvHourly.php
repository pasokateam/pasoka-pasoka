<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Hourly;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class UvHourly
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Hourly
 */
class UvHourly extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $timezone
     * @return \stdClass|String
     */
    public function get($idLocation, $dateBegin, $timezone = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_UV_HOURLY .
                $idLocation .
                $dateBegin .
                $timezone
            );
    }

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @param string|null $timezone
     * @return array|\stdClass
     */
    public function getByIdLocaleAndPeriod($idLocation, $dateBegin, $dateEnd = null, $timezone = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        if (!is_null($timezone)) {
            $queryString .= "&timezone={$timezone}";
        }

        return $this
            ->setRouter(['forecast', 'hourly', 'uv'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_UV_HOURLY .
                $idLocation .
                $dateBegin .
                $dateEnd .
                $timezone
            );
    }
} 