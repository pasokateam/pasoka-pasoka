<?php
namespace Pasoka\Component\WeatherApi\Retrieve\NearbyLocations;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class NearbyBeaches
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\NearbyLocations
 */
class NearbyBeaches extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['nearby', 'beaches'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_NEARBY_BEACHES . '_' . $idLocation);
    }
}