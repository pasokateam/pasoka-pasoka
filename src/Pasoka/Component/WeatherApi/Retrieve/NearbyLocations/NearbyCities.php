<?php
namespace Pasoka\Component\WeatherApi\Retrieve\NearbyLocations;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class NearbyCities
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\NearbyLocations
 */
class NearbyCities extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['nearby', 'cities'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_NEARBY_CITIES . '_' . $idLocation);
    }
}