<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Geo;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Station
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Geo
 */
class Station extends AbstractRetrieve
{

    /**
     * @param float latitude Latitude from locale
     * @param float longitude Longitude from locale
     * @param string source Data's source
     * @param string clearCache
     * @return array|\stdClass
     */
    public function getAll($latitude, $longitude, $source, $clearCache = null)
    {
        return $this
            ->setRouter(['observed', 'latest', 'nearest-station'])
            ->addQueryString("?latitude={$latitude}&longitude={$longitude}&sources%5B%5D={$source}")
            ->manageCache(
                WeatherApi::FETCH_GEO_STATION . '_' .
                $latitude . '_' .
                $longitude  . '_' .
                $source,
                $clearCache
            );
    }
}