<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Geo;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Fire
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Geo
 */
class Fire extends AbstractRetrieve
{
    /**
     * @return array|\stdClass
     */
    public function getAll()
    {
        return $this
            ->setRouter(['geo', 'fire'])
            ->manageCache(
                WeatherApi::FETCH_GEO_FIRE .
                $this->configAuthentication['CLIENT']
            );
    }

    /**
     * @param int idLocation Id da localidade a ser buscada
     * @param string clearCache
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation, $clearCache = null)
    {
        return $this
            ->setRouter(['geo', 'fire'])
            ->addQueryString("?localeId={$idLocation}")
            ->manageCache(
                WeatherApi::FETCH_GEO_FIRE  . '_' .
                $idLocation,
                $clearCache
            );
    }
}