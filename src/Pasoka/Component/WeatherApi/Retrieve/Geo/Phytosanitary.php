<?php

namespace Pasoka\Component\WeatherApi\Retrieve\Geo;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;

/**
 * Class Phytosanitary
 *
 * @author Adilson Ferreira <adilson@climatempo.com.br>
 * @package Pasoka\Component\WeatherApi\Retrieve\Geo
 * @version 1.0
 */
class Phytosanitary extends AbstractRetrieve
{

    /**
     * @todo foi removido o cache dessa requisicao pois esta ocorrendo
     *       o erro conforme reportado na issue #1
     *
     * @param number    $latitude      Latitude do local
     * @param number    $longitude     Longitude do local
     * @param array     $phytosanitary Variavel de pragas do endpoint do mapserver
     * @param bool      $forceUpdate   Verifica se é pra forçar uma atualização ou não
     * @param \DateTime $dateBegin     Data de inicio
     * @param \DateTime $dateEnd       Data de fim
     *
     * @author Adilson Ferreira <adilson@climatempo.com.br>
     * @return object
     */
    public function getPhytosanitary15D(
        $latitude,
        $longitude,
        array $phytosanitary,
        \DateTime $dateBegin = null,
        \DateTime $dateEnd = null
    ) {
        $queryString = '?' . http_build_query([
            'latitude'      => $latitude,
            'longitude'     => $longitude,
            'phytosanitary' => implode(',', $phytosanitary)
        ]);

        if ($dateBegin && $dateEnd) {
            $queryString .= '&' . http_build_query([
                'dateBegin' => $dateBegin->format('Y-m-d'),
                'dateEnd'   => $dateEnd->format('Y-m-d')
            ]);
        }

        return $this
            ->setRouter(['geo', 'phytosanitary'])
            ->addQueryString($queryString)
            ->request();
    }
}