<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Geo;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Layer
 *
 * @author Michel Araujo
 * @author Gustavo Santos <gusanthiagodv@gmail.com>
 * @version 1.1.0
 * @package Pasoka\Component\WeatherApi\Retrieve\Geo
 */
class Layer extends AbstractRetrieve
{
    /**
     * Cacula a altura proporcional da imagem com base na largura,altura e nova largura
     *
     * @param int $width largura para calcular a altura
     * @author Gustavo Santos<gusanthiagodv@gmail.com
     * @return float|int
     */
    public function getResizeHeight($width)
    {
        $height = WeatherApi::IMG_GEO_BOX_SOUTH_AMERICA_HEIGHT * $width / WeatherApi::IMG_GEO_BOX_SOUTH_AMERICA_WIDTH;
        return intval($height);
    }

    /**
     * @return array|\stdClass
     */
    public function getAll()
    {
        return $this
            ->setRouter(['geo', 'map'])
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER .
                $this->configAuthentication['CLIENT']
            );
    }

    /**
     * Retorna os dados da camada de visibilidade
     *
     * @param int $width Width da imagem retornada
     * @param String $bbox String dos valores que forma o bbox
     * @param String $format Formato da imagem image/jpg ou image/png
     * @param float $quality Qualidade que a imagem vai ser gerada
     * @param String $styles Estilo da imagem
     * @param String $transparent Determina se vai ter fundo ou não
     * @author Michel Araujo
     * @author Gustavo Santos <gusanthiagodv@gmail.com>
     * @return array|\stdClass
     */
    public function getHourlyVis(
        $width,
        $bbox,
        $format = null,
        $quality = null,
        $styles = null,
        $transparent = null
    ) {
        $height = $this->getResizeHeight($width);
        $queryString = "?height={$height}&width={$width}&bbox={$bbox}"
            . "&format={$format}&quality={$quality}&styles={$styles}&transparent={$transparent}";

        return $this
            ->setRouter(['geo', 'layer', 'hourly', 'vis'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER . '_' .
                $height . '_' .
                $width . '_' .
                $bbox . '_' .
                $format . '_' .
                $quality . '_' .
                $styles . '_' .
                $transparent . '_' .
                'vis'
            );
    }

    /**
     * Retorna os dados da camada de infravermelho
     *
     * @param int $width Width da imagem retornada
     * @param String $bbox String dos valores que forma o bbox
     * @param String $format Formato da imagem image/jpg ou image/png
     * @param float $quality Qualidade que a imagem vai ser gerada
     * @param String $styles Estilo da imagem
     * @param String $transparent Determina se vai ter fundo ou não
     * @author Michel Araujo
     * @author Gustavo Santos <gusanthiagodv@gmail.com>
     * @return array|\stdClass
     */
    public function getHourlyIr(
        $width,
        $bbox,
        $format = null,
        $quality = null,
        $styles = null,
        $transparent = null
    ) {
        $height = $this->getResizeHeight($width);
        $queryString = "?height={$height}&width={$width}&bbox={$bbox}"
            . "&format={$format}&quality={$quality}&styles={$styles}&transparent={$transparent}";

        return $this
            ->setRouter(['geo', 'layer', 'hourly', 'ir'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER . '_' .
                $height . '_' .
                $width . '_' .
                $bbox . '_' .
                $format . '_' .
                $quality . '_' .
                $styles . '_' .
                $transparent . '_' .
                'ir'
            );
    }

    /**
     * Retorna os dados da camada de colorida
     *
     * @param int $width Width da imagem retornada
     * @param String $bbox String dos valores que forma o bbox
     * @param String $format Formato da imagem image/jpg ou image/png
     * @param float $quality Qualidade que a imagem vai ser gerada
     * @param String $styles Estilo da imagem
     * @param String $transparent Determina se vai ter fundo ou não
     * @author Michel Araujo
     * @author Gustavo Santos <gusanthiagodv@gmail.com>
     * @return array|\stdClass
     */
    public function getHourlyBm(
        $width,
        $bbox,
        $format = null,
        $quality = null,
        $styles = null,
        $transparent = null
    ) {
        $height = $this->getResizeHeight($width);
        $queryString = "?height={$height}&width={$width}&bbox={$bbox}"
            . "&format={$format}&quality={$quality}&styles={$styles}&transparent={$transparent}";

        return $this
            ->setRouter(['geo', 'layer', 'hourly', 'bm'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER . '_' .
                $height . '_' .
                $width . '_' .
                $bbox . '_' .
                $format . '_' .
                $quality . '_' .
                $styles . '_' .
                $transparent . '_' .
                'bm'
            );
    }

    /**
     * Retorna os dados da camada de Infra Colorida
     *
     * @param int $width Width da imagem retornada
     * @param String $bbox String dos valores que forma o bbox
     * @param String $format Formato da imagem image/jpg ou image/png
     * @param float $quality Qualidade que a imagem vai ser gerada
     * @param String $styles Estilo da imagem
     * @param String $transparent Determina se vai ter fundo ou não
     * @author Michel Araujo
     * @author Gustavo Santos <gusanthiagodv@gmail.com>
     * @return array|\stdClass
     */
    public function getHourlyIrr(
        $width,
        $bbox,
        $format = null,
        $quality = null,
        $styles = null,
        $transparent = null
    ) {
        $height = $this->getResizeHeight($width);
        $queryString = "?height={$height}&width={$width}&bbox={$bbox}"
            . "&format={$format}&quality={$quality}&styles={$styles}&transparent={$transparent}";

        return $this
            ->setRouter(['geo', 'layer', 'hourly', 'irr'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER . '_' .
                $height . '_' .
                $width . '_' .
                $bbox . '_' .
                $format . '_' .
                $quality . '_' .
                $styles . '_' .
                $transparent . '_' .
                'irr'
            );
    }

    /**
     * Retorna os dados da camada de Composta
     *
     * @param int $width Width da imagem retornada
     * @param String $bbox String dos valores que forma o bbox
     * @param String $format Formato da imagem image/jpg ou image/png
     * @param float $quality Qualidade que a imagem vai ser gerada
     * @param String $styles Estilo da imagem
     * @param String $transparent Determina se vai ter fundo ou não
     * @author Michel Araujo
     * @author Gustavo Santos <gusanthiagodv@gmail.com>
     * @return array|\stdClass
     */
    public function getHourlyRgb(
        $width,
        $bbox,
        $format = null,
        $quality = null,
        $styles = null,
        $transparent = null
    ) {
        $height = $this->getResizeHeight($width);
        $queryString = "?height={$height}&width={$width}&bbox={$bbox}"
            . "&format={$format}&quality={$quality}&styles={$styles}&transparent={$transparent}";

        return $this
            ->setRouter(['geo', 'layer', 'hourly', 'rgb'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_LAYER . '_' .
                $height . '_' .
                $width . '_' .
                $bbox . '_' .
                $format . '_' .
                $quality . '_' .
                $styles . '_' .
                $transparent . '_' .
                'rgb'
            );
    }

}