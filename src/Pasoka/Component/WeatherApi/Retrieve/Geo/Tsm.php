<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Geo;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Tsm
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Geo
 */
class Tsm extends AbstractRetrieve
{
    /**
     * @param float $latitude Latitude from locale
     * @param float $longitude Longitude from locale
     * @param String $dateBegin Beginning of search date
     * @param String $dateEnd Ending of search date
     * @param String $clearCache Ignore Cache
     *
     * @return array|\stdClass
     */
    public function getHourly($latitude, $longitude, $dateBegin = null, $dateEnd = null, $clearCache = null)
    {
        $queryString = "?".http_build_query([
            "latitude" => $latitude,
            "longitude" => $longitude,
            "dateBegin" => $dateBegin,
            "dateEnd" => $dateEnd
        ]);

        return $this
            ->setRouter(['geo', 'forecast', 'hourly', 'tsm'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_GEO_TSM. '_' .
                $latitude . '_' .
                $longitude  . '_' .
                $dateBegin . '_' .
                $dateEnd,
                $clearCache
            );
    }
}