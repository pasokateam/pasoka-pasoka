<?php
namespace Pasoka\Component\WeatherApi\Retrieve\History;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class AlertHistory
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\History
 */
class AlertHistory extends AbstractRetrieve
{
    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @return array|\stdClass
     */
    public function getByPeriod($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['history', 'alert'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_ALERT_HISTORY . '_' .
                $this->configAuthentication["CLIENT"] .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd
            );
    }
}