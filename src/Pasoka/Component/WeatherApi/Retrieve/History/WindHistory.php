<?php
namespace Pasoka\Component\WeatherApi\Retrieve\History;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class WindHistory
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\History
 */
class WindHistory extends AbstractRetrieve
{

    /**
     * @param int         $idLocation
     * @param string      $dateBegin
     * @param string|null $dateEnd
     * @return array|\stdClass
     */
    public function getByPeriod($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['history', 'wind'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_WIND_HISTORY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd
            );
    }
} 