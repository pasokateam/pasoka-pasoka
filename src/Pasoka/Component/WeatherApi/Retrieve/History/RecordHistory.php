<?php

namespace Pasoka\Component\WeatherApi\Retrieve\History;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class RecordHistory
 *
 * Classe para obter recordes de temperatura.
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\History
 * @author Guilherme Santos <guilhermedossantos91@gmail.com>
 * @version 1.0.0
 */
class RecordHistory extends AbstractRetrieve
{

    /**
     * Retorna recorde de temperatura minima por localidade
     *
     * Rota: /history/record/temperature/min?idLocale=XXXX
     *
     * @author Guilherme Santos <guilhermedossantos91@gmail.com>
     * @version 1.0.0
     * @param int $idLocation id da localidade
     * @param null|string $clearCache limpa cache
     * @return array|\stdClass retorno da API
     */
    public function getTemperatureMin($idLocation, $clearCache = null)
    {
        $queryString = "?idlocale={$idLocation}";

        return $this
            ->setRouter(['history', 'record', 'temperature', 'min'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_RECORD_HISTORY . '_' .
                $idLocation . '_',
                $clearCache
            );
    }
}