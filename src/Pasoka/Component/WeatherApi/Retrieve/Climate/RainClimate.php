<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Climate;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class RainClimate
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Climate
 * @version 1.1
 */
class RainClimate extends AbstractRetrieve
{
    /**
     * @param int    $idLocation
     * @param string $monthBegin
     * @param string $monthEnd
     * @return \stdClass|array
     * @throws \Exception
     */
    public function getByMonth($idLocation, $monthBegin, $monthEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&monthBegin={$monthBegin}";

        if (!is_null($monthEnd)) {
            $queryString .= "&monthEnd={$monthEnd}";
        }

        return $this
            ->setRouter(['climate', 'rain'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_RAIN_CLIMATE . '_' .
                $idLocation . '_' .
                $monthBegin . '_' .
                $monthEnd
            );
    }
}
