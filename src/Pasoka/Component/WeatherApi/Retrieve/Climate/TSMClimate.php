<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Climate;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class TSMClimate
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Climate
 * @version 1.1
 */
class TSMClimate extends AbstractRetrieve
{
    /**
     * @param String $monthBegin
     * @param String $monthEnd
     * @return \stdClass|String
     * @throws \Exception
     */
    public function getByMonth($monthBegin, $monthEnd = null)
    {
        $queryString = "?monthBegin={$monthBegin}";

        if (!is_null($monthBegin)) {
            $queryString .= "&monthBegin={$monthBegin}";
        }
        
        return $this
            ->setRouter(['climate', 'tsm'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_TSM_CLIMATE . '_' .
                $monthBegin . '_' .
                $monthEnd
            );
    }
}