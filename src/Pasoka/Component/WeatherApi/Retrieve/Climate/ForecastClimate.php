<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Climate;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastClimate
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Climate
 * @version 1.1
 */
class ForecastClimate extends AbstractRetrieve
{
    /**
     * @param int $idLocate
     * @param String $clearCache
     * @return \stdClass
     * @throws \Exception
     */
    public function getByIdLocation($idLocate, $clearCache = null)
    {
        return $this
            ->setRouter(['climate'])
            ->addQueryString("?idlocale={$idLocate}")
            ->manageCache(WeatherApi::FETCH_FORECAST_CLIMATE . '_' . $idLocate, $clearCache);
    }
}