<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Climate;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class RegionClimate
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Climate
 * @version 1.1
 */
class RegionClimate extends AbstractRetrieve
{
    /**
     * @param int    $idLocation
     * @param string $monthBegin
     * @param string $monthEnd
     * @return \stdClass|String
     * @throws \Exception
     */
    public function getByMonth($idLocation, $monthBegin, $monthEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&monthBegin={$monthBegin}";

        if (!is_null($monthBegin)) {
            $queryString .= "&monthBegin={$monthBegin}";
        }

        return $this
            ->setRouter(['climate', 'region'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_REGION_CLIMATE . '_' .
                $idLocation . '_' .
                $monthBegin . '_' .
                $monthEnd
            );
    }
}