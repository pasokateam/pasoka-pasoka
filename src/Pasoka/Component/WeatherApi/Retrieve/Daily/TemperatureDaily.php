<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class TemperatureDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class TemperatureDaily extends AbstractRetrieve
{
    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'temperature'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_TEMPERATURE_DAILY . '_' . $idLocation);
    }
}