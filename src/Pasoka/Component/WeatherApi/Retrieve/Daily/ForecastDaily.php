<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastDaily
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 * @version 1.1
 */
class ForecastDaily extends AbstractRetrieve
{
    /**
     * @param int    $idLocation
     * @param string $dateBegin
     * @param string $dateEnd
     * @param string $clearCache
     * @return \stdClass|String
     * @throws \Exception
     */
    public function getByIdLocale(
        $idLocation,
        $dateBegin,
        $dateEnd = null,
        $clearCache = null
    ) {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }
        return $this
            ->setRouter(['forecast', 'period', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_DAILY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd,
                $clearCache
            );
    }

    /**
     * @param double $latitude
     * @param double $longitude
     * @param string $dateBegin
     * @param string $dateEnd
     * @param string $clearCache
     * @return \stdClass|String
     * @throws \Exception
     */
    public function getByLatLon(
        $latitude,
        $longitude,
        $dateBegin,
        $dateEnd = null,
        $clearCache = null
    ) {
        $queryString = "?latitude={$latitude}&longitude={$longitude}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        } else {
            $queryString .= "&dateEnd={$dateBegin}";
        }
        return $this
            ->setRouter(['geo', 'forecast', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_DAILY . '_' .
                $queryString,
                $clearCache
            );
    }
}