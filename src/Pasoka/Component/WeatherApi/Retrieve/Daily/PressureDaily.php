<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class PressureDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class PressureDaily extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'pressure'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_PRESSURE_DAILY . '_' . $idLocation);
    }
}