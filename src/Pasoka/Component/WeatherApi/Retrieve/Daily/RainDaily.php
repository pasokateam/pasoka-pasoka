<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class RainDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class RainDaily extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'rain'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_RAIN_DAILY . '_' . $idLocation);
    }
}