<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class MoonDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class MoonDaily extends AbstractRetrieve
{
    /**
     * @return array|\stdClass
     */
    public function getPhases()
    {
        return $this
            ->setRouter(['forecast', '15days', 'moon'])
            ->manageCache(WeatherApi::FETCH_MOON_DAILY);
    }
}