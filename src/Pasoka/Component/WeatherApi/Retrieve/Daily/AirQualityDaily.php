<?php

namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class AirQualityDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class AirQualityDaily extends AbstractRetrieve
{
    /**
     * @param int    $idLocation
     * @param string $dateBegin
     * @param string $dateEnd
     * @param string $clearCache
     * @return \stdClass|String
     * @throws \Exception
     */
    public function getByIdLocale(
        $idLocation,
        $dateBegin,
        $dateEnd = null,
        $clearCache = null
    ) {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['forecast', 'period', 'air-quality'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_AIR_QUALITY_DAILY . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd,
                $clearCache
            );
    }
}
