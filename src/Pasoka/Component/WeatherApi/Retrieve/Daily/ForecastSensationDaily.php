<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastSensationDaily
 *
 * @author Michel Araujo
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 * @version 1.0.0
 */
class ForecastSensationDaily extends AbstractRetrieve
{
    /**
     * Retorna os dados de sensação termica por id localidade
     *
     * @param int $idLocation Id localidade
     * @author Michel Araujo
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'thermal-sensation'])
            ->addQueryString("?localeId={$idLocation}")
            ->manageCache(WeatherApi::FETCH_FORECAST_SENSATION);
    }
} 