<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class HumidityDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class HumidityDaily extends AbstractRetrieve
{
    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'humidity'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_HUMIDITY_DAILY . '_' . $idLocation);
    }
}