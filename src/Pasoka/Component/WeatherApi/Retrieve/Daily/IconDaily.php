<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class IconDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class IconDaily extends AbstractRetrieve
{

    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'icon'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_ICON_DAILY . '_' . $idLocation);
    }
}