<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class LightningRisk
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class LightningRisk extends AbstractRetrieve
{
    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'lightning', 'risk'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_LIGHTNING_RISK . '_' . $idLocation);
    }
}