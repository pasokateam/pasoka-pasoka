<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class WindDaily
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 */
class WindDaily extends AbstractRetrieve
{
    /**
     * @param int $idLocation
     * @return array|\stdClass
     */
    public function getByIdLocation($idLocation)
    {
        return $this
            ->setRouter(['forecast', '15days', 'wind'])
            ->addQueryString("?idlocale={$idLocation}")
            ->manageCache(WeatherApi::FETCH_WIND_DAILY . '_' . $idLocation);
    }
}