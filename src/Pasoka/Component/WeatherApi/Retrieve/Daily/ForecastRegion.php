<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastRegion
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 * @version 1.1
 */
class ForecastRegion extends AbstractRetrieve
{
    /**
     * @return \stdClass|array
     */
    public function get()
    {
        return $this
            ->setRouter(['forecast', 'region'])
            ->manageCache(WeatherApi::FETCH_FORECAST_REGION);
    }
}