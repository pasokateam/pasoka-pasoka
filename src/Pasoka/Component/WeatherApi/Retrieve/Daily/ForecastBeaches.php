<?php
namespace Pasoka\Component\WeatherApi\Retrieve\Daily;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class ForecastBeaches
 *
 * @author  Michel Araujo - michelaraujopinto@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Daily
 * @version 1.1
 */
class ForecastBeaches extends AbstractRetrieve
{
    /**
     * @param int    $idLocation
     * @param string $dateBegin
     * @param string $dateEnd
     * @return \stdClass|array
     * @throws \Exception
     */
    public function get($idLocation, $dateBegin, $dateEnd = null)
    {
        $queryString = "?idlocale={$idLocation}&dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['forecast', 'beach', 'period', 'daily'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_FORECAST_BEACHES . '_' .
                $idLocation . '_' .
                $dateBegin . '_' .
                $dateEnd
            );
    }
}