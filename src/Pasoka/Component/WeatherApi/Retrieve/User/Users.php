<?php
namespace Pasoka\Component\WeatherApi\Retrieve\User;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Users
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\User
 */
class Users extends AbstractRetrieve
{

    /**
     * @return array|\stdClass
     */
    public function get()
    {
        return $this
            ->setRouter(['user'])
            ->manageCache(
                WeatherApi::FETCH_USER . '-' .
                $this->configAuthentication['KEY'] . '-' .
                $this->configAuthentication['CLIENT']
            );
    }
}