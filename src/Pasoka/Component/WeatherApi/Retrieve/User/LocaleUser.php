<?php
namespace Pasoka\Component\WeatherApi\Retrieve\User;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class LocaleUser
 *
 * @package Pasoka\Component\WeatherApi\Retrieve\User
 */
class LocaleUser extends AbstractRetrieve
{
    /**
     * @param int $id
     * @return array|\stdClass
     */
    public function getById($id)
    {
        return $this
            ->setRouter(['module', 'locales'])
            ->addQueryString("?id={$id}")
            ->manageCache(WeatherApi::FETCH_LOCALE_USER . '_' . $id);
    }
}