<?php
namespace Pasoka\Component\WeatherApi\Retrieve;

use Pasoka\Component\Cache\FileCache;

/**
 * Class AbstractRetrieve
 *
 * @package Pasoka\Component\WeatherApi\Retrieve
 */
abstract class AbstractRetrieve
{

    const API_PROD = "http://api.climatempo.com.br/api/v1";
    const API_DEV = "http://api.climatempo.dev/api/v1";
    const API_BETA = "http://beta.api.climatempo.com.br/api/v1";

    const TIMEZONE_UTC = "UTC";
    const TIMEZONE_LOCAL = "LOCAL";

    /**
     * @var string
     */
    protected $urlAPI;

    /**
     * @var resource
     */
    protected $context = null;

    /**
     * @var int
     */
    protected $cacheTime;

    /**
     * @var string
     */
    protected $pathCache;

    /**
     * @var array
     */
    protected $configAuthentication;

    /**
     * Construct
     *
     * @param array  $configAuthentication
     * @param int    $cacheTime
     * @param string $pathCache
     */
    public function __construct(
        array $configAuthentication = [],
        $cacheTime = 0,
        $pathCache
    )
    {
        $this->configAuthentication = $configAuthentication;
        $this->cacheTime = $cacheTime;
        $this->pathCache = $pathCache;

        if (isset($configAuthentication["API"])) {
            switch ($configAuthentication["API"]) {
                case self::API_DEV:
                    $this->urlAPI = self::API_DEV;
                    break;
                case self::API_BETA:
                    $this->urlAPI = self::API_BETA;
                    break;
                case self::API_PROD:
                    $this->urlAPI = self:: API_PROD;
                    break;
            }
        } else {
            $this->urlAPI = self:: API_PROD;
        }
    }

    /**
     * API request
     *
     * @return mixed
     * @throws \Exception
     */
    protected function request()
    {
        $this->authentication();

        $token = base64_encode(http_build_query($this->authData));
        
        if (strpos($this->urlAPI, '?') !== false) {
            $token = '&token=' . $token;
        } else {
            $token = '?token=' . $token;
        }

        $content = @file_get_contents($this->urlAPI . $token, false, $this->context);
        if ($content === false) {
            throw new \Exception("API Error: Invalid server");
        }
        $api = @json_decode($content);

        if (!is_object($api)) {
            throw new \Exception("API Error: Invalid return");
        }

        if (!isset($api->response)) {
            throw new \Exception("API Error: Invalid response");
        }

        if (!isset($api->response->success)) {
            throw new \Exception("API Error: Invalid response.success");
        }

        if (!isset($api->data)) {
            throw new \Exception("API Error: Invalid data");
        }

        if (!isset($api->response->message)) {
            throw new \Exception("API Error: Invalid response.message");
        }

        if ($api->response->success == false) {
            throw new \Exception($api->response->message);
        }

        return $api;

    }


    /**
     * Request authentication config
     *
     * @throws \Exception
     */
    protected function authentication()
    {
        $this->validAuthentication();
        $this->authData = array(
            'key'         => $this->configAuthentication['KEY'],
            'client'      => $this->configAuthentication['CLIENT'],
            'nocache'     => false,
            'type'        => $this->configAuthentication['TYPE'],
            'application' => $this->configAuthentication['APPLICATION']
        );
        
        if (array_key_exists('PRODUCT', $this->configAuthentication)) {
            $this->authData['product'] = $this->configAuthentication['PRODUCT'];
        }

        $http = array(
            'method'  => 'GET',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($this->authData)
        );
        $this->context = stream_context_create(array('http' => $http));
    }

    /**
     * Authentication validate
     */
    private function validAuthentication()
    {
        if (!array_key_exists('KEY', $this->configAuthentication)
            || empty($this->configAuthentication['KEY'])
        ) {
            throw new \UnexpectedValueException('API Config Error: KEY not found');
        } else if (!array_key_exists('CLIENT', $this->configAuthentication)
            || empty($this->configAuthentication['CLIENT'])
            || !is_string($this->configAuthentication['CLIENT'])
        ) {
            throw new \UnexpectedValueException('API Config Error: CLIENT not found');
        } else if (!array_key_exists('TYPE', $this->configAuthentication)
            || empty($this->configAuthentication['TYPE'])
            || !is_string($this->configAuthentication['TYPE'])
        ) {
            throw new \UnexpectedValueException('API Config Error: TYPE not found');
        } else if (!array_key_exists('APPLICATION', $this->configAuthentication)
            || empty($this->configAuthentication['APPLICATION'])
            || !is_string($this->configAuthentication['APPLICATION'])
        ) {
            throw new \UnexpectedValueException('API Config Error: APPLICATION not found');
        }
    }

    /**
     * $routers = array(
     *     0 => 'alert',
     *     1 => 'lightning',
     *     2 => 'cep'
     * );
     * http://api.climatempo.com.br/api/v1/alert/lightning/cep
     *
     * @param array $router
     * @return $this
     */
    protected function setRouter(array $router)
    {
        foreach ($router as $value) {
            $this->urlAPI .= '/' . $value;
        }

        return $this;
    }

    /**
     * $queryString = array(
     *     'id' => 1,
     *     'country' => 'brazil'
     * );
     *
     * http://api.climatempo.com.br/api/v1?id=1&country=brazil
     *
     * @param array $queryStrings
     * @return $this
     */
    protected function setQueryString(array $queryStrings)
    {
        $this->urlAPI .= http_build_query($queryStrings);
        return $this;
    }

    /**
     * @param string $queryString
     * @return $this
     */
    protected function addQueryString($queryString)
    {
        $this->urlAPI .= $queryString;
        return $this;
    }

    /**
     * @param string $cacheName
     * @param string $clearCache
     * @return \stdClass|array
     */
    protected function manageCache($cacheName, $clearCache = null)
    {
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);
        $api = null;

        try {

            if (!is_null($clearCache) && $clearCache == 'sergio') {
                $api = $this->request();
                $fileCache->add($cacheName, $api);
            } else {
                if ($fileCache->isExpired($cacheName)) {
                    $api = $this->request();
                    $fileCache->add($cacheName, $api);
                }
            }

        } catch (\Exception $e) {
            $api = $fileCache->fetch($cacheName);

            if (!$api instanceof \stdClass) {
                $response = new \stdClass();
                $response->success = false;
                $response->message = "API Error: unknown";
                $response->time = 0;
                $api = new \stdClass();
                $api->response = $response;
                $api->data = [];
            } else {
                if (!isset($api->response)) {
                    $api->response = new \stdClass();
                }
                if (!isset($api->data)) {
                    $api->data = [];
                }
                $api->response->time = 0;
                $api->response->message = $e->getMessage();
            }

            $fileCache->add($cacheName, $api);
        }


        return $fileCache->fetch($cacheName);
    }
}
