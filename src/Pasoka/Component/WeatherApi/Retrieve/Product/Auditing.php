<?php

namespace Pasoka\Component\WeatherApi\Retrieve\Product;

use Pasoka\Component\WeatherApi\Retrieve\AbstractRetrieve;
use Pasoka\Component\WeatherApi\WeatherApi;

/**
 * Class Auditing
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\WeatherApi\Retrieve\Product
 */
class Auditing extends AbstractRetrieve
{
    /**
     * @param string $dateBegin
     * @param string $dateEnd
     * @return \stdClass|String
     * @throws \Exception
     */
    public function get($dateBegin, $dateEnd = null)
    {
        $queryString = "?dateBegin={$dateBegin}";

        if (!is_null($dateEnd)) {
            $queryString .= "&dateEnd={$dateEnd}";
        }

        return $this
            ->setRouter(['auditing'])
            ->addQueryString($queryString)
            ->manageCache(
                WeatherApi::FETCH_AUDITING . '_' .
                $dateBegin . '_' .
                $dateEnd
            );
    }
}