<?php
namespace Pasoka\Component\WeatherApi;

use Pasoka\Component\WeatherApi\Retrieve\Climate\ForecastClimate;
use Pasoka\Component\WeatherApi\Retrieve\Climate\RainClimate;
use Pasoka\Component\WeatherApi\Retrieve\Climate\RegionClimate;
use Pasoka\Component\WeatherApi\Retrieve\Climate\TemperatureClimate;
use Pasoka\Component\WeatherApi\Retrieve\Climate\TSMClimate;
use Pasoka\Component\WeatherApi\Retrieve\Crops\CropsCitiesAgricultural;
use Pasoka\Component\WeatherApi\Retrieve\Daily\AirQualityDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\ForecastBeaches;
use Pasoka\Component\WeatherApi\Retrieve\Daily\ForecastDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\ForecastRegion;
use Pasoka\Component\WeatherApi\Retrieve\Daily\ForecastSensationDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\HumidityDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\IconDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\LightningRisk;
use Pasoka\Component\WeatherApi\Retrieve\Daily\MoonDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\PressureDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\RainDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\SunDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\TemperatureDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\UvDaily;
use Pasoka\Component\WeatherApi\Retrieve\Daily\WindDaily;
use Pasoka\Component\WeatherApi\Retrieve\Geo\Fire;
use Pasoka\Component\WeatherApi\Retrieve\Geo\Layer;
use Pasoka\Component\WeatherApi\Retrieve\Geo\Station;
use Pasoka\Component\WeatherApi\Retrieve\Geo\Tsm;
use Pasoka\Component\WeatherApi\Retrieve\Geo\Phytosanitary;
use Pasoka\Component\WeatherApi\Retrieve\History\AlertHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\HumidityHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\LightningHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\RadiationHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\RainHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\RecordHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\TemperatureHistory;
use Pasoka\Component\WeatherApi\Retrieve\History\WindHistory;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\ForecastHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\HumidityHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\PressureHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\RainHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\SunHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\TemperatureHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\UvHourly;
use Pasoka\Component\WeatherApi\Retrieve\Hourly\WindHourly;
use Pasoka\Component\WeatherApi\Retrieve\Marine\BoardTideMarine;
use Pasoka\Component\WeatherApi\Retrieve\Marine\WaveMarine;
use Pasoka\Component\WeatherApi\Retrieve\Monitoring\LightningCepMonitoring;
use Pasoka\Component\WeatherApi\Retrieve\Monitoring\LightningMonitoring;
use Pasoka\Component\WeatherApi\Retrieve\Monitoring\LocaleMonitoring;
use Pasoka\Component\WeatherApi\Retrieve\Monitoring\SeasonMonitoring;
use Pasoka\Component\WeatherApi\Retrieve\Monitoring\WeatherMonitoring;
use Pasoka\Component\WeatherApi\Retrieve\NearbyLocations\NearbyBeaches;
use Pasoka\Component\WeatherApi\Retrieve\NearbyLocations\NearbyCities;
use Pasoka\Component\WeatherApi\Retrieve\News\NewsCall;
use Pasoka\Component\WeatherApi\Retrieve\News\NewsFeatured;
use Pasoka\Component\WeatherApi\Retrieve\News\NewsPosts;
use Pasoka\Component\WeatherApi\Retrieve\Period\MoonPeriod;
use Pasoka\Component\WeatherApi\Retrieve\Period\MosquitoPeriod;
use Pasoka\Component\WeatherApi\Retrieve\Period\RankingPeriod;
use Pasoka\Component\WeatherApi\Retrieve\Period\TemperaturePeriod;
use Pasoka\Component\WeatherApi\Retrieve\Portal\Pedagua;
use Pasoka\Component\WeatherApi\Retrieve\Product\Auditing;
use Pasoka\Component\WeatherApi\Retrieve\Satellite\AgriculturalSatellite;
use Pasoka\Component\WeatherApi\Retrieve\Satellite\ForecastSatellite;
use Pasoka\Component\WeatherApi\Retrieve\Satellite\ImagesSatellite;
use Pasoka\Component\WeatherApi\Retrieve\Satellite\TextSatellite;
use Pasoka\Component\WeatherApi\Retrieve\User\LocaleUser;
use Pasoka\Component\WeatherApi\Retrieve\User\Users;
use Pasoka\Component\WeatherApi\Retrieve\Observed\FogObserved;

/**
 * Class WeatherApi
 *
 * @package Pasoka\Component\WeatherApi
 */
class WeatherApi
{

    // timezone
    const TIMEZONE_UTC = "UTC";
    const TIMEZONE_LOCAL = "LOCAL";


    const FETCH_AUDITING = 'AUDITING';

    // Climate
    const FETCH_RAIN_CLIMATE = 'RAIN_CLIMATE';
    const FETCH_REGION_CLIMATE = 'REGION_CLIMATE';
    const FETCH_TEMPERATURE_CLIMATE = 'TEMPERATURE_CLIMATE';
    const FETCH_TSM_CLIMATE = 'TSM_CLIMATE';
    const FETCH_FORECAST_CLIMATE = 'FORECAST_CLIMATE';

    // Daily
    const FETCH_AIR_QUALITY_DAILY = 'AIR_QUALITY_DAILY';
    const FETCH_HUMIDITY_DAILY = 'HUMIDITY_DAILY';
    const FETCH_ICON_DAILY = 'ICON_DAILY';
    const FETCH_MOON_DAILY = 'MOON_DAILY';
    const FETCH_PRESSURE_DAILY = 'FETCH_PRESSURE_DAILY';
    const FETCH_RAIN_DAILY = 'RAIN_DAILY';
    const FETCH_SUN_DAILY = 'SUN_DAILY';
    const FETCH_TEMPERATURE_DAILY = 'TEMPERATURE_DAILY';
    const FETCH_UV_DAILY = 'UV_DAILY';
    const FETCH_WIND_DAILY = 'WIND_DAILY';
    const FETCH_LIGHTNING_RISK = 'LIGHTNING_RISK';
    const FETCH_FORECAST_DAILY = 'FORECAST_DAILY';
    const FETCH_FORECAST_BEACHES = 'FORECAST_BEACHES';
    const FETCH_FORECAST_REGION = 'FORECAST_REGION';
    const FETCH_FORECAST_SENSATION = 'FORECAST_SENSATION';

    // History
    const FETCH_ALERT_HISTORY = 'ALERT_HISTORY';
    const FETCH_HUMIDITY_HISTORY = 'HUMIDITY_HISTORY';
    const FETCH_LIGHTNING_HISTORY = 'FETCH_LIGHTNING_HISTORY';
    const FETCH_RADIATION_HISTORY = 'FETCH_RADIATION_HISTORY';
    const FETCH_RAIN_HISTORY = 'FETCH_RAIN_HISTORY';
    const FETCH_TEMPERATURE_HISTORY = 'TEMPERATURE_HISTORY';
    const FETCH_RECORD_HISTORY = 'RECORD_HISTORY';
    const FETCH_WIND_HISTORY = 'WIND_HISTORY';

    // Hourly
    const FETCH_HUMIDITY_HOURLY = 'HUMIDITY_HOURLY';
    const FETCH_PRESSURE_HOURLY = 'PRESSURE_HOURLY';
    const FETCH_RAIN_HOURLY = 'RAIN_HOURLY';
    const FETCH_TEMPERATURE_HOURLY = 'TEMPERATURE_HOURLY';
    const FETCH_WIND_HOURLY = 'WIND_HOURLY';
    const FETCH_FORECAST_HOURLY = 'FORECAST_HOURLY';
    const FETCH_FORECAST_UV_HOURLY = 'FORECAST_UV_HOURLY';
    const FETCH_FORECAST_SUNSHINE_HOURLY = 'FORECAST_SUNSHINE_HOURLY';

    // Marine
    const FETCH_BOARD_TIDE_MARINE = 'BOARD_TIDE_MARINE';
    const FETCH_WAVE_MARINE = 'FETCH_WAVE_MARINE';

    // Monitoring
    const FETCH_LIGHTNING_CEP_MONITORING = 'LIGHTNING_CEP_MONITORING';
    const FETCH_LIGHTNING_MONITORING = 'LIGHTNING_MONITORING';
    const FETCH_LOCALE_MONITORING = 'LOCALE_MONITORING';
    const FETCH_WEATHER_MONITORING = 'WEATHER_MONITORING';
    const FETCH_WEATHER_MONITORING_LOWEST_TEMPERATURE = 'WEATHER_MONITORING_LOWEST_TEMPERATURE';
    const FETCH_WEATHER_MONITORING_HIGHEST_TEMPERATURE = 'WEATHER_MONITORING_HIGHEST_TEMPERATURE';
    const FETCH_SEASON_MONITORING = 'SEASON_MONITORING';

    // Observed
    const FETCH_FOG_OBSERVED = 'FOG_OBSERVED';

    //Users
    const FETCH_USER = 'INFO_USER';
    const FETCH_LOCALE_USER = 'LOCALIDADE_USER';

    //Period
    const FETCH_MOON_PERIOD = 'MOON_PERIOD';
    const FETCH_MOSQUITO_PERIOD = 'MOSQUITO_PERIOD';
    const FETCH_RANKING_TEMP_MIN_PERIOD = 'RANKING_TEMP_MIN_PERIOD';
    const FETCH_RANKING_TEMP_MAX_PERIOD = 'RANKING_TEMP_MAX_PERIOD';
    const FETCH_RANKING_HIGHER_TEMP_MIN_PERIOD = 'RANKING_HIGHER_TEMP_MIN_PERIOD';
    const FETCH_RANKING_HIGHER_TEMP_MAX_PERIOD = 'RANKING_HIGHER_TEMP_MAX_PERIOD';
    const FETCH_RANKING_PERIOD = "RANKING_PERIOD";
    const FETCH_TEMPERATURE_PERIOD = "TEMPERATURE_PERIOD";

    //Satellite
    const FETCH_IMAGES_SATELLITE = 'IMAGES_SATELLITE';
    const FETCH_FORECAST_SATELLITE = 'FORECAST_SATELLITE';
    const FETCH_AGRICULTURAL_SATELLITE = 'AGRICULTURAL_SATELLITE';
    const FETCH_TEXT_SATELLITE = 'TEXT_SATELLITE';

    //Geo
    const FETCH_GEO_LAYER = 'GEO_LAYER';
    const FETCH_GEO_FIRE = 'GEO_FIRE';
    const FETCH_GEO_STATION = 'GEO_STATION';
    const FETCH_GEO_TSM = 'GEO_TSM';
    const FETCH_GEO_PHYTOSANITARY = 'GEO_PHYTOSANITARY';

    //Nearby Locations
    const FETCH_NEARBY_CITIES = 'NEARBY_CITIES';
    const FETCH_NEARBY_BEACHES = 'NEARBY_BEACHES';

    //Crops
    const FETCH_CROPS_CITIES_AGRICULTURAL = 'CROPS_CITIES_AGRICULTURAL';

    //News
    const FETCH_NEWS_CALL = 'NEWS_CALL';
    const FETCH_NEWS_POSTS = 'NEWS_POSTS';
    const FETCH_NEWS_FEATURED = 'NEWS_FEATURED';

    //Pedagua
    const FETCH_COUNT_POSTS = 'COUNT_POSTS';

    //cache time
    const NO_CACHE = 0;
    const CACHE_TIME_5MIN = 300;
    const CACHE_TIME_15MIN = 900;
    const CACHE_TIME_30MIN = 1800;
    const CACHE_TIME_1HOUR = 3600;
    const CACHE_TIME_2HOUR = 7200;
    const CACHE_TIME_6HOUR = 21600;
    const CACHE_TIME_1DAY = 86400;
    const CACHE_TIME_1MONTH = 2629800;
    const CACHE_TIME_INFINITE = -1;

    // Resize img
    const IMG_GEO_BOX_SOUTH_AMERICA_WIDTH = 3000;
    const IMG_GEO_BOX_SOUTH_AMERICA_HEIGHT = 2472;

    /**
     * @param array  $configAuthentication
     * @param string $fetch
     * @param int    $cacheTime
     * @param string $pathCache
     * @return null|ForecastClimate|ForecastRegion|UvHourly|SeasonMonitoring|NewsFeatured
     */
    public static function prepare(
        array $configAuthentication,
        $fetch,
        $cacheTime = self::CACHE_TIME_1HOUR,
        $pathCache
    )
    {
        switch ($fetch) {

            case self::FETCH_AUDITING:
                return new Auditing($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RAIN_CLIMATE:
                return new RainClimate($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_REGION_CLIMATE:
                return new RegionClimate($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEMPERATURE_CLIMATE:
                return new TemperatureClimate($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TSM_CLIMATE:
                return new TSMClimate($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_HUMIDITY_DAILY:
                return new HumidityDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_ICON_DAILY:
                return new IconDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_MOON_DAILY:
                return new MoonDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_PRESSURE_DAILY:
                return new PressureDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RAIN_DAILY:
                return new RainDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_SUN_DAILY:
                return new SunDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEMPERATURE_DAILY:
                return new TemperatureDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_UV_DAILY:
                return new UvDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_WIND_DAILY:
                return new WindDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_ALERT_HISTORY:
                return new AlertHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_HUMIDITY_HISTORY:
                return new HumidityHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LIGHTNING_HISTORY:
                return new LightningHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RADIATION_HISTORY:
                return new RadiationHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RAIN_HISTORY:
                return new RainHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEMPERATURE_HISTORY:
                return new TemperatureHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_WIND_HISTORY:
                return new WindHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_HUMIDITY_HOURLY:
                return new HumidityHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_PRESSURE_HOURLY:
                return new PressureHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RAIN_HOURLY:
                return new RainHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEMPERATURE_HOURLY:
                return new TemperatureHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_WIND_HOURLY:
                return new WindHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_BOARD_TIDE_MARINE:
                return new BoardTideMarine($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_WAVE_MARINE:
                return new WaveMarine($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LIGHTNING_CEP_MONITORING:
                return new LightningCepMonitoring($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LIGHTNING_MONITORING:
                return new LightningMonitoring($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FOG_OBSERVED:
                return new FogObserved($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_GEO_STATION:
                return new Station($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_GEO_TSM:
                return new Tsm($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_GEO_PHYTOSANITARY:
                return new Phytosanitary($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LOCALE_MONITORING:
                return new LocaleMonitoring($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_USER:
                return new Users($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LOCALE_USER:
                return new LocaleUser($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_MOON_PERIOD:
                return new MoonPeriod($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_MOSQUITO_PERIOD:
                return new MosquitoPeriod($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_WEATHER_MONITORING:
                return new WeatherMonitoring($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_IMAGES_SATELLITE:
                return new ImagesSatellite($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEXT_SATELLITE:
                return new TextSatellite($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_SATELLITE:
                return new ForecastSatellite($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_AGRICULTURAL_SATELLITE:
                return new AgriculturalSatellite($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_LIGHTNING_RISK:
                return new LightningRisk($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_GEO_LAYER:
                return new Layer($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_GEO_FIRE:
                return new Fire($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_DAILY:
                return new ForecastDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_HOURLY:
                return new ForecastHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_NEARBY_CITIES:
                return new NearbyCities($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_NEARBY_BEACHES:
                return new NearbyBeaches($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_CROPS_CITIES_AGRICULTURAL:
                return new CropsCitiesAgricultural($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_BEACHES:
                return new ForecastBeaches($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_NEWS_CALL:
                return new NewsCall($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_NEWS_POSTS:
                return new NewsPosts($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_NEWS_FEATURED:
                return new NewsFeatured($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_REGION:
                return new ForecastRegion($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_SEASON_MONITORING:
                return new SeasonMonitoring($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_CLIMATE:
                return new ForecastClimate($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_UV_HOURLY:
                return new UvHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RANKING_PERIOD:
                return new RankingPeriod($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_RECORD_HISTORY:
                return new RecordHistory($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_SUNSHINE_HOURLY:
                return new SunHourly($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_TEMPERATURE_PERIOD:
                return new TemperaturePeriod($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_FORECAST_SENSATION:
                return new ForecastSensationDaily($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_COUNT_POSTS:
                return new Pedagua($configAuthentication, $cacheTime, $pathCache);

            case self::FETCH_AIR_QUALITY_DAILY:
                return new AirQualityDaily($configAuthentication, $cacheTime, $pathCache);
        }
        return null;
    }
}
