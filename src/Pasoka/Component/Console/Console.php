<?php
/**
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Console;

/**
 * Class Console
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Console
 */
final class Console
{

    /**
     * Total de argumentos
     *
     * @access private
     * @var int
     */
    public $argc;

    /**
     * Lista de argumentos lidos
     *
     * @access private
     * @var string[]
     */
    public $argv;

    /**
     * @access private
     */
    public $stdout;

    /**
     * Lista de linhas lidas pelo console
     *
     * @access private
     * @var \ArrayObject
     */
    protected $params;

    /**
     * Construtor
     *
     * @access public
     * @param $argc
     * @param $argv
     */
    public function __construct($argc, $argv)
    {
        $this->argc = $argc;
        $this->argv = $argv;
        $this->stdout = fopen("php://stdout", "w");
    }


    /**
     * Exibe mensagem no console e quebra linha
     *
     * @access private
     * @param string $message
     * @param string $color
     */
    public function writeln($message, $color = "")
    {
        $this->write("\n{$message}", $color);
    }


    /**
     * Exibe mensagem no console
     *
     * @param string $message
     * @param string $color
     */
    public function write($message, $color = "")
    {
        $this->display("{$color}");
        $this->display("{$message}");
        $this->display(Color::REMOVE);
    }


    /**
     * @param string $message
     */
    public function display($message = "")
    {
        fwrite($this->stdout, $message);
    }


    /**
     * Le uma linha do console
     *
     * {|literal|}
     * if { }
     * {|/literal|}
     *
     * @access private
     * @param null|string $param
     * @return string
     */
    public function readLine($param = null)
    {
        $obj = new \stdClass();
        $obj->param = $param;
        $obj->value = trim(fgets(STDIN));

        if ($param !== null) {
            //scanf(STDIN, "%d\n", $number); // carrega number a partir do STDIN
            $iterator = $this->params->getIterator();
            while ($iterator->valid()) {
                if (serialize($obj) == serialize($iterator->current())) {
                    $this->params->offsetUnset($iterator->key());
                }
                $iterator->next();
            }
            $this->params->append($obj);
        }
        return $obj->value;
    }


    /**
     * Procura uma linha lida pelo console
     *
     * @access private
     * @param $param
     * @return null|string
     */
    public function findRead($param)
    {
        $iterator = $this->params->getIterator();
        while ($iterator->valid()) {
            if ($iterator->current()->param == $param) {
                return $iterator->current()->value;
            }
            $iterator->next();
        }
        return null;
    }


    /**
     * @return int
     */
    public function getNumArgs()
    {
        return $this->argc;
    }

    /**
     * @param int $key
     * @return null|string
     */
    public function getArg($key = null)
    {
        if (is_null($key)) {
            return null;
        }

        if (array_key_exists($key, $this->argv)) {
            return $this->argv[$key];
        }

        return null;
    }


    /**
     *
     */
    public function __destruct()
    {
        fclose($this->stdout);
    }

    /**
     *
     */
    public function clear()
    {
        $this->command("clear");
    }


    /**
     * @param string $command
     */
    public function command($command)
    {
        @system($command);
    }

}