<?php

namespace Pasoka\Component\Console;


/**
 * Class ServerDeploy
 *
 * TODO: Classe temporaria para facilitar o deploy
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Console
 */
final class ServerDeploy
{

    const FILE = "servers.json";

    /**
     * @var \stdClass[]
     */
    private $servers;

    /**
     * @var Console
     */
    private $console;

    /**
     * @var string
     */
    private $path;

    /**
     * @var \stdClass
     */
    private $config;


    /**
     * @param string  $path
     * @param string  $serverName
     * @param Console $console
     * @throws \Exception
     */
    public function __construct($path, $serverName = null, Console $console)
    {
        $this->console = $console;

        if (!is_dir($path)) {
            $this->showError("Path {$path} not found");
        }

        $serversFile = $path . DIRECTORY_SEPARATOR . self::FILE;

        if (!file_exists($serversFile)) {
            $this->showError("File " . self::FILE . " not found");
        }

        $serversFile = @file_get_contents($serversFile);
        $this->servers = @json_decode($serversFile);

        if ($this->servers === false || !is_array($this->servers)) {
            $this->showError("Invalid json file");
        }

        $this->config = $this->findConfig($serverName);

        if (is_null($this->config)) {
            $this->showError("Servidor '{$serverName}' não encontrado");
        }
    }


    /**
     * @param string $message
     */
    private function showError($message)
    {
        $this->console->write("\n Error: {$message}\n\n", Color::LIGHT_RED);
        exit;
    }


    /**
     * @param string $name
     * @return null|\stdClass
     */
    private function findConfig($name = null)
    {

        if (is_null($name)) {
            $this->showError("Propriedade name não foi atruibuido");
        }

        if (!is_string($name)) {
            $this->showError("Propriedade name inválido no arquivo de configuração");
        }

        foreach ($this->servers as $server) {

            if (!$server instanceof \stdClass) {
                continue;
            }

            if (!isset($server->name)) {
                continue;
            }

            if (strtolower($server->name) == strtolower($name)) {
                return $server;
            }

        }
        return null;
    }


    /**
     * @param array $commands
     */
    private function execute(array $commands)
    {
        sleep(1);
        $user = null;
        while ($user === null || empty($user)) {
            $this->console->write(" -> [ {$this->config->name} ] Digite o usuario: ", Color::LIGHT_GREEN);
            $user = $this->console->readLine();
        }

        $this->console->writeln(
            " -> Conectando... \n\n",
            Color::LIGHT_GREEN
        );

        $commands = implode(";", $commands);

        $this->console->command(
            "ssh -tt {$user}@{$this->config->server->ip} -p {$this->config->server->port} \"cd {$this->config->server->path};{$commands};\";"
        );
    }


    /**
     *
     */
    public function deploy()
    {
        $this->console->writeln(
            "\n Iniciando deploy no servidor {$this->config->name}\n",
            Color::LIGHT_GREEN
        );
        $commands = [];

        $composer = null;
        while ($composer !== "s" && $composer !== "n") {
            $this->console->write(" -> [ {$this->config->name} ] Atualizar framework (s/n) : ", Color::ORANGE);
            $composer = strtolower($this->console->readLine());
        }
        if ($composer === "s") {
            $commands[] = "composer update";
        }
        $commands[] = "echo '" . Color::LIGHT_BLUE . "---------------------------------------------------'";
        $commands[] = "git checkout -f";
        $commands[] = "git pull origin {$this->config->branch}";


        $clean = null;
        while ($clean !== "s" && $clean !== "n") {
            $this->console->write(" -> [ {$this->config->name} ] Limpar cache (s/n) : ", Color::ORANGE);
            $clean = strtolower($this->console->readLine());
        }
        if ($clean === "s") {
            $commands[] = "sudo chmod +x clean.sh";
            $commands[] = "sudo ./clean.sh";
        }


        if (isset($this->config->run) && $this->config->run instanceof \stdClass) {
            if (isset($this->config->run->deploy) && is_array($this->config->run->deploy)) {
                foreach ($this->config->run->deploy as $command) {
                    $commands[] = $command;
                }
            }
        }

        $commands[] = "echo '" . Color::ORANGE . "---------------------------------------------------'";
        $commands[] = "git log -1";
        $commands[] = "echo '" . Color::REMOVE . "---------------------------------'";
        $this->execute($commands);
    }


    /**
     *
     */
    public function rollBack()
    {
        $this->console->writeln(
            "\n Iniciando rollback no servidor {$this->config->name}\n",
            Color::LIGHT_GREEN
        );
        $commands = [];

        $rollback = null;
        while ($rollback !== "s" && $rollback !== "n") {
            $this->console->write(" -> [ {$this->config->name} ] Deseja realmente fazer rollback? (s/n) :", Color::ORANGE);
            $rollback = strtolower($this->console->readLine());
        }
        if ($rollback === "s") {

            $commands[] = "echo '" . Color::LIGHT_BLUE . "---------------------------------------------------'";
            $commands[] = "git checkout -f";
            $commands[] = "git reset --hard HEAD~1";

            if (isset($this->config->run) && $this->config->run instanceof \stdClass) {
                if (isset($this->config->run->rollback) && is_array($this->config->run->rollback)) {
                    foreach ($this->config->run->rollback as $command) {
                        $commands[] = $command;
                    }
                }
            }

            $commands[] = "echo '" . Color::ORANGE . "---------------------------------------------------'";
            $commands[] = "git log -1";
            $commands[] = "echo '" . Color::REMOVE . "---------------------------------'";
            $this->execute($commands);
        } else {
            $this->console->write(" -> wtf?!\n\n", Color::LIGHT_RED);
        }
    }


    /**
     *
     */
    public function clean()
    {
        $this->console->writeln(
            "\n Iniciando limpeza no servidor {$this->config->name}\n",
            Color::LIGHT_GREEN
        );
        $commands = [];

        $composer = null;
        while ($composer !== "s" && $composer !== "n") {
            $this->console->write(" -> [ {$this->config->name} ] Atualizar framework (s/n) : ", Color::ORANGE);
            $composer = strtolower($this->console->readLine());
        }
        if ($composer === "s") {
            $commands[] = "composer update";
        }

        $clean = null;
        while ($clean !== "s" && $clean !== "n") {
            $this->console->write(" -> [ {$this->config->name} ] Limpar cache (s/n) : ", Color::ORANGE);
            $clean = strtolower($this->console->readLine());
        }
        if ($clean === "s") {
            $commands[] = "sudo chmod +x clean-all.sh";
            $commands[] = "sudo ./clean-all.sh";
        }


        if (isset($this->config->clean) && $this->config->clean instanceof \stdClass) {
            if (isset($this->config->run->clean) && is_array($this->config->run->clean)) {
                foreach ($this->config->run->clean as $command) {
                    $commands[] = $command;
                }
            }
        }

        $commands[] = "echo '" . Color::ORANGE . "---------------------------------------------------'";
        $commands[] = "git log -1";
        $commands[] = "echo '" . Color::REMOVE . "---------------------------------'";
        $this->execute($commands);
    }


}