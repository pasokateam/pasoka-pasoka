<?php

namespace Pasoka\Component\Console;

use Pasoka\Core\Console\Traits\ConsoleTrait;
use Pasoka\Core\Dependence\PasokaLoader;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Class Power
 *
 * TODO: Classe temporaria para facilitar o deploy
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Console
 */
final class Power
{

    use ConsoleTrait;

    const VERSION = "3.1";

    /**
     * Helper arguments
     *
     * @var string[]
     */
    private $arguments = array(
        '--help',
        '-help',
        '-h',
        '-?',
        'deploy',
        'rollback',
        'clean'
    );

    /**
     * @var BootstrapInterface
     */
    private $bootstrap;

    /**
     * @var PasokaLoader
     */
    private $loader;

    /**
     * Constructor
     *
     * @param Console                  $console
     * @param BootstrapInterface|mixed $bootstrap
     * @param PasokaLoader             $loader
     */
    public function __construct(Console $console, PasokaLoader $loader, $bootstrap)
    {
        $this->params = new \ArrayObject();
        $this->showMessages = true;
        $this->console = $console;
        $this->loader = $loader;
        $this->bootstrap = $bootstrap;
    }

    /**
     * Begin helper
     */
    public function run()
    {
        $numArgs = $this->console->getNumArgs();

        if ($numArgs > 1) {

            $firstArg = $this->console->getArg(1);
            $secondArg = $this->console->getArg(2);

            $command = explode(":", $firstArg);
            $param = null;

            if ($numArgs != 2 || in_array($command[0], $this->arguments)) {

                if (count($command) > 1) {
                    $command = trim($command[1]);
                    $command = strtolower($command);
                }

                if (isset($secondArg)) {
                    $param = $secondArg;
                }

                // ARGUMENT MODULE
                if (strpos($firstArg, 'deploy') !== false) {
                    $this->argDeploy($command);
                }

                // ARGUMENT MODULE
                if (strpos($firstArg, 'rollback') !== false) {
                    $this->argRollback($command);
                }

                // ARGUMENT MODULE
                if (strpos($firstArg, 'clean') !== false) {
                    $this->argClean($command);
                }

                // ARGUMENT HELP
                if (strpos($firstArg, '--help') !== false ||
                    strpos($firstArg, '-h') !== false ||
                    strpos($firstArg, '-help') !== false ||
                    strpos($firstArg, '?') !== false
                ) {
                    $this->argWelcome();
                }
            } else {
                $this->console->writeln(
                    " - Error: Command '{$firstArg}' not found, use -help for more information\n\n",
                    Color::LIGHT_RED
                );
            }
            return;
        } else {
            $this->argWelcome();
        }
    }


    /**
     *
     */
    private function showHeader()
    {

        //     ____                          __
        //    / __ \____ _      _____  _____/ /
        //   / /_/ / __ \ | /| / / _ \/ ___/ /
        //  / ____/ /_/ / |/ |/ /  __/ /  /_/
        // /_/    \____/|__/|__/\___/_/  (_)
        $this->console->writeln("---------------------------------------------------------", Color::ORANGE);
        $this->console->writeln("-      ____                          __                      ", Color::ORANGE);
        $this->console->writeln("-     / __ \\____ _      _____  _____/ / ", Color::ORANGE);
        $this->console->write(self::VERSION . "               ", Color::LIGHT_RED);
        $this->console->writeln("-    / /_/ / __ \\ | /| / / _ \\/ ___/ /                       ", Color::ORANGE);
        $this->console->writeln("-   / ____/ /_/ / |/ |/ /  __/ /  /_/                       ", Color::ORANGE);
        $this->console->writeln("-  /_/    \\____/|__/|__/\\___/_/  (_)                       ", Color::ORANGE);
        $this->console->writeln("-  Grandes poderes vem com grandes responsabilidades     ", Color::ORANGE);
        $this->console->writeln("---------------------------------------------------------", Color::ORANGE);
    }


    /**
     * @param string $name
     * @return ServerDeploy
     */
    private function getServer($name = null)
    {
        return new ServerDeploy(
            $this->bootstrap->getPathApp(),
            $name,
            $this->console
        );
    }


    /**
     * @param string $serverName
     */
    private function argDeploy($serverName)
    {
        $this->console->clear();
        $this->showHeader();
        $this->getServer($serverName)->deploy();
    }

    /**
     * @param string $serverName
     */
    private function argRollback($serverName)
    {
        $this->console->clear();
        $this->showHeader();
        $this->getServer($serverName)->rollBack();
    }

    /**
     * @param string $serverName
     */
    private function argClean($serverName)
    {
        $this->console->clear();
        $this->showHeader();
        $this->getServer($serverName)->clean();
    }

    /**
     *
     */
    private function argWelcome()
    {
        $this->console->clear();
        $this->showHeader();
        $this->console->writeln(
            "  Lista de comandos:",
            Color::LIGHT_GREEN
        );
        $this->console->writeln(
            " - power deploy:<server>",
            Color::YELLOW
        );
        $this->console->write(
            "\t Atualiza repositorio do servidor",
            Color::LIGHT_BLUE
        );
        $this->console->writeln(
            " - power rollback:<server>",
            Color::YELLOW
        );
        $this->console->write(
            "\t Volta ultimo commit no servidor",
            Color::LIGHT_BLUE
        );
        $this->console->writeln(
            " - power clean:<server>",
            Color::YELLOW
        );
        $this->console->write(
            "\t\t Limpa cache do servidor\n\n",
            Color::LIGHT_BLUE
        );
    }


}