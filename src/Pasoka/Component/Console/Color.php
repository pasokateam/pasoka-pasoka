<?php

namespace Pasoka\Component\Console;

/**
 * Cores para o console
 *
 * \e[CODEm
 *
 * Black        0;30
 * Dark Gray    1;30
 * Blue         0;34
 * Light Blue   1;34
 * Green        0;32
 * Light Green  1;32
 * Cyan         0;36
 * Light Cyan   1;36
 * Red          0;31
 * Light Red    1;31
 * Purple       0;35
 * Light Purple 1;35
 * Brown/Orange 0;33
 * Yellow       1;33
 * Light Gray   0;37
 * White        1;37
 * SEM COR:     0
 *
 * 'bold' => "\033[1m",
 * 'end'  => "\033[0m"
 *
 */
final class Color
{

    private function __construct()
    {
    }

    /**
     * @var string[]
     */
    public static $name = [
        "white"      => self::WHITE,
        "red"        => self::RED,
        "lightRed"   => self::LIGHT_RED,
        "yellow"     => self::YELLOW,
        "blue"       => self::BLUE,
        "lightBLue"  => self::LIGHT_BLUE,
        "magenta"    => self::MAGENTA,
        "gray"       => self::GRAY,
        "remove"     => self::REMOVE,
        "bold"       => self::BOLD,
        "green"      => self::GREEN,
        "lightGreen" => self::LIGHT_GREEN,
        "orange"     => self::ORANGE
    ];

    const WHITE = "\e[1;37m";
    const RED = "\e[0;31m";
    const LIGHT_RED = "\e[1;31m";
    const YELLOW = "\e[1;33m";
    const BLUE = "\e[0;34m";
    const LIGHT_BLUE = "\e[1;34m";
    const MAGENTA = "\e[33;35m";
    const GRAY = "\e[33;30m";
    const REMOVE = "\033[0m";
    const BOLD = "\033[1m";
    const GREEN = "\e[0;32m";
    const LIGHT_GREEN = "\e[1;32m";
    const ORANGE = "\e[0;33m";


} 