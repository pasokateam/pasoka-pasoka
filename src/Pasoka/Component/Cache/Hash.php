<?php

namespace Pasoka\Component\Cache;

use Pasoka\Component\Cache\Interfaces\HashInterface;


/**
 * Class Hash
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Cache
 */
final class Hash implements HashInterface
{

    /**
     * @var null|string
     */
    private $id;

    /**
     * @param string $id
     */
    public function __construct($id = null)
    {
        if (!is_null($id)) {
            $this->id = $this->convert($id);
        }
    }


    /**
     * @param mixed $hash
     * @return int
     */
    public static function validate($hash = null)
    {
        if (is_null($hash)) {
            return false;
        }

        return strlen($hash) == 32 && ctype_xdigit($hash);
    }


    /**
     * @param string $string
     * @return string
     */
    public static function convert($string)
    {

        if (empty($string)) {
            throw new \InvalidArgumentException("Invalid string");
        }

        return md5($string);
    }


    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }
} 