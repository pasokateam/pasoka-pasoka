<?php

namespace Pasoka\Component\Cache;

/**
 * Class FileRow
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Cache
 */
final class FileRow
{

    /**
     * @var Hash
     */
    private $hash;

    /**
     * @var string
     */
    private $path;


    /**
     * @param Hash   $hash
     * @param string $path
     */
    public function __construct(Hash $hash = null, $path = null)
    {
        $this->hash = $hash;
        $this->path = $path;
    }


    /**
     * @return Hash
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param Hash $hash
     * @return $this
     */
    public function setHash(Hash $hash)
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return $this
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }


}