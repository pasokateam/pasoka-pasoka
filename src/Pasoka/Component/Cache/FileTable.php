<?php

namespace Pasoka\Component\Cache;

/**
 * Class FileTable
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Cache
 */
final class FileTable implements \Countable
{

    /**
     * @var array
     */
    private $rows;

    /**
     */
    public function __construct()
    {
        $this->rows = [];
    }

    /**
     * @param FileRow $fileRow
     * @throws \Exception
     * @return $this
     */
    public function add(FileRow $fileRow)
    {
        $hash = $fileRow->getHash();
        if (is_null($hash)) {
            throw new \Exception("Hash invalid");
        }

        $rows[(string)$hash] = $fileRow;
        return $this;
    }

    /**
     * @param Hash $hash
     * @return null|
     */
    public function get(Hash $hash)
    {
        return $this->exists($hash) ?
            $this->rows[(string)$hash] :
            null;
    }


    /**
     * @param Hash $hash
     * @return bool
     */
    public function exists(Hash $hash)
    {
        return array_key_exists((string)$hash, $this->rows);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->rows);
    }


    /**
     * @return $this
     */
    public function clear()
    {
        $this->rows = [];
        return $this;
    }

}