<?php

namespace Pasoka\Component\Cache\Interfaces;

/**
 * Interface CacheInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Cache\Interfaces
 */
interface CacheInterface
{

    /**
     * @param string $path
     * @param int    $time
     */
    public function __construct($path, $time = 3600);

    /**
     * @param mixed $id
     * @param array $data
     * @return mixed
     */
    public function add($id, $data);

    /**
     * @param mixed $id
     * @return \stdClass[]
     */
    public function fetch($id);

    /**
     * @param mixed $id
     * @return mixed
     */
    public function exists($id);

    /**
     * @return bool
     */
    public function isExpired($id);

    /**
     * @param mixed $id
     * @return bool
     */
    public function clear($id);

    /**
     * @return bool
     */
    public function clearAll();


} 