<?php
namespace Pasoka\Component\Cache\Interfaces;

/**
 * Interface HashInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Cache\Interfaces
 */
interface HashInterface
{

    /**
     * @param string $id
     */
    public function __construct($id = null);

    /**
     * @param string $hash
     * @return bool
     */
    public static function validate($hash);

    /**
     * @return string
     */
    public function __toString();

} 