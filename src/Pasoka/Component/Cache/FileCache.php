<?php

namespace Pasoka\Component\Cache;

use Pasoka\Component\Cache\Interfaces\CacheInterface;
use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\PasokaObject;

/**
 * Class FileCache
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @author  Adilson Ferreira - adilson@climatempo.com.br
 * @version 1.1.0
 * @package Pasoka\Component\Cache
 */
final class FileCache implements CacheInterface
{

    /**
     * NAO MUDAR!!!
     *
     * @var string
     */
    const SUFFIX = "-cache.json";

    /**
     * @var string
     */
    private $dir;

    /**
     * @var int
     */
    private $time;

    /**
     * @param string $dir
     * @param int    $time
     * @throws \Exception
     */
    public function __construct($dir, $time = 3600)
    {
        if (!is_dir($dir)) {
            throw new \Exception("Cache path not found");
        }

        $this->dir = $dir;
        $this->time = $time;
        return $this;
    }


    /**
     * @param \Pasoka\Component\Cache\Hash $hash
     * @throws \Exception
     * @return string
     */
    private function getPathFile(Hash $hash)
    {
        return $this->dir . DIRECTORY_SEPARATOR . ((string)$hash) . self::SUFFIX;
    }


    /**
     * @param Hash   $hash
     * @param string $data
     * @throws \Exception
     */
    private function save(Hash $hash, $data = null)
    {
        $path = $this->getPathFile($hash);
        $file = @fopen($path, "w+");

        if ($file !== false) {
            if (is_string($data)) {
                @fwrite($file, $data);
            }
            @fclose($file);
        }
        unset($file);

        FileWait::load($path)->remove();
    }


    /**
     * @param string                          $id
     * @param array|\stdClass|ObjectInterface $data
     * @return $this
     */
    public function add($id, $data)
    {
        $hash = new Hash($id);

        if ($data instanceof \stdClass) {
            $this->save($hash, @json_encode($data));
            return $this;
        }

        if ($data instanceof ObjectInterface) {
            $this->save($hash, @json_encode(
                $data->valueOf()
            ));
            return $this;
        }

        $this->save($hash, @json_encode(PasokaObject::valuesOf($data)));
        return $this;
    }

    /**
     * @param mixed $id
     * @return \stdClass[]|string[]|int[]|float[]|array
     */
    public function fetch($id)
    {
        $path = $this->getPathFile(new Hash($id));

        if (!file_exists($path)) {
            return [];
        }

        if (($data = @file_get_contents($path)) === false) {
            return [];
        }

        if (($data = @json_decode($data)) === false || is_null($data)) {
            return [];
        }

        return $data;
    }

    /**
     * Verifica se o cache foi expirado
     *
     * Se a propriedade time estiver com o valor -1 o cache nunca
     * vai ser atualizado
     *
     *
     * @param mixed $id
     * @throws \Exception
     * @return bool
     */
    public function isExpired($id)
    {
        $file = $this->getPathFile(new Hash($id));

        if (file_exists($file) && $this->time == -1) {
            return false;
        }

        if (FileWait::load($file)->waiting()) {
            return false;
        }

        if (!file_exists($file) || (time() - @filemtime($file)) > $this->time) {
            FileWait::load($file)->generate();
            return true;
        }

        return false;
    }

    /**
     * @param mixed $id
     * @return bool
     */
    public function clear($id)
    {
        $file = $this->getPathFile(new Hash($id));
        if (file_exists($file)) {
            @unlink($file);
            clearstatcache();
            if (!file_exists($file)) {
                return true;
            }
        }

        return false;
    }

    /**
     * TODO: Implements
     */
    public function clearAll()
    {
        exec("rm -Rf {$this->dir}/*" . self::SUFFIX);
    }

    /**
     * @param mixed $id
     * @return mixed
     */
    public function exists($id)
    {
        return @file_exists($this->getPathFile(new Hash($id)));
    }
}