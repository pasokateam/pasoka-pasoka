<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\View\Interfaces;

/**
 * Interface TemplateInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\View\Interfaces
 */
interface TemplateInterface
{

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return boolean
     */
    public function isCached($templateFile, $templateId = null);

    /**
     * @param bool $cache
     * @param int  $time
     * @return mixed
     */
    public function setCache($cache = true, $time = 3600);

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return boolean
     */
    public function clearCache($templateFile, $templateId = null);

    /**
     * @param mixed $key
     * @param mixed $value
     * @return mixed
     */
    public function setVar($key, $value);

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return mixed
     */
    public function show($templateFile, $templateId = null);

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return mixed
     */
    public function fetch($templateFile, $templateId = null);

    /**
     * @return mixed
     */
    public function getEngine();
}