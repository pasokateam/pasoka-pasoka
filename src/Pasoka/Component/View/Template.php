<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\View;

use Pasoka\Component\View\Interfaces\TemplateInterface;

/**
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Component\View
 * @namespace  Pasoka\Component\View
 * @version    1.0.0
 */
class Template implements TemplateInterface
{

    /**
     * @var string
     */
    const FILTER_TYPE_OUTPUT = "output";

    /**
     * @var string
     */
    const FILTER_TYPE_INPUT = "input";

    /**
     * @var Template
     */
    private static $instance;

    /**
     * @var \Smarty
     */
    private $smarty;

    /**
     * @var string
     */
    private $pathCache;

    /**
     * @var string
     */
    private $pathCompile;

    /**
     * @var string
     */
    private $pathTemplate;

    /**
     * @param string $pathTemplate
     * @param string $pathCompile
     * @param string $pathCache
     * @param bool   $caching
     * @param bool   $compile
     */
    private function __construct(
        $pathTemplate,
        $pathCompile,
        $pathCache,
        $caching = false,
        $compile = false
    )
    {
        $this->smarty = new \Smarty();
        $this->smarty->setTemplateDir($pathTemplate);
        $this->smarty->setCompileDir($pathCompile);
        $this->smarty->setCacheDir($pathCache);
        $this->smarty->caching = $caching;
        $this->smarty->force_compile = $compile;
        $this->smarty->left_delimiter = '{|';
        $this->smarty->right_delimiter = '|}';

        $this->pathTemplate = $pathTemplate;
        $this->pathCompile = $pathCompile;
        $this->pathCache = $pathCache;
    }

    /**
     * Singleton
     *
     * @param string $pathTemplate
     * @param string $pathCompile
     * @param string $pathCache
     * @param bool   $caching
     * @param bool   $compile
     * @return Template
     */
    public static function getInstance(
        $pathTemplate,
        $pathCompile,
        $pathCache,
        $caching = false,
        $compile = false
    )
    {
        if (self::$instance == null) {
            self::$instance = new self(
                $pathTemplate,
                $pathCompile,
                $pathCache,
                $caching,
                $compile
            );
        }
        return self::$instance;
    }

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return boolean
     */
    public function isCached($templateFile, $templateId = null)
    {
        return $this->smarty->isCached($templateFile, $templateId);
    }

    /**
     * @param boolean $cache
     * @param int     $time
     * @return $this
     */
    public function setCache($cache = true, $time = 3600)
    {
        $this->smarty->caching = $cache;
        $this->smarty->cache_lifetime = $time;
        return $this;
    }

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return boolean
     */
    public function clearCache($templateFile, $templateId = null)
    {
        return $this->smarty->clearCache($templateFile, $templateId);
    }

    /**
     * @param string $key
     * @param mixed  $value
     * @return $this
     */
    public function setVar($key, $value)
    {
        $this->smarty->assign($key, $value);
        return $this;
    }

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return $this
     */
    public function show($templateFile, $templateId = null)
    {
        $this->smarty->display($templateFile, $templateId);
        return $this;
    }

    /**
     * @param string $templateFile
     * @param mixed  $templateId
     * @return string
     */
    public function fetch($templateFile, $templateId = null)
    {
        return $this->smarty->fetch($templateFile, $templateId);
    }

    /**
     * @return \Smarty
     */
    public function getEngine()
    {
        return $this->smarty;
    }


    /**
     * load a filter of specified type and name
     *
     * @param  string $type filter type
     * @param  string $name filter name
     * @return $this
     */
    public function loadFilter($type, $name)
    {
        $this->smarty->loadFilter($type, $name);
        return $this;
    }


    /**
     * Minify output smarty
     *
     * @return $this
     */
    public function minify()
    {
        $this->smarty->loadFilter(self::FILTER_TYPE_OUTPUT, "trimwhitespace");
        return $this;
    }

    /**
     * @return string
     */
    public function getPathCache()
    {
        return $this->pathCache;
    }

    /**
     * @return string
     */
    public function getPathCompile()
    {
        return $this->pathCompile;
    }

    /**
     * @return string
     */
    public function getPathTemplate()
    {
        return $this->pathTemplate;
    }


}