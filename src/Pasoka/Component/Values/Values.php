<?php

namespace Pasoka\Component\Values;

use Pasoka\Component\Values\Interfaces\ValuesInterface;

/**
 * Class Values
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Component\Values
 */
final class Values implements ValuesInterface
{

    /**
     * @var Values
     */
    private static $instance;

    /**
     * @var array
     */
    private $files = [];

    /**
     * @var string
     */
    private $path = "";

    /**
     * @var array
     */
    private $values = [];

    /**
     * Construct
     *
     * @param string $path
     */
    private function __construct($path)
    {
        $this->path = $path;
        $this->load($path);
    }

    /**
     * Singleton pattern
     *
     * @param string $path
     * @return \Pasoka\Component\Values\Values
     */
    public static function getInstance($path)
    {
        if (self::$instance == null) {
            self::$instance = new self($path);
        }
        return self::$instance;
    }

    /**
     * Validate content groups
     *
     * @param array $groups
     * @return string
     */
    private function validateGroups($groups)
    {

        if (!is_array($groups)) {
            return "Invalid root";
        }

        foreach ($groups as $group) {

            if (!$group instanceof \stdClass) {
                return "Invalid group";
            }

            $reflection = new \ReflectionObject($group);

            if (!$reflection->hasProperty("group")) {
                return "Group - does not have property 'group'";
            }
            if (!$reflection->hasProperty("values")) {
                return "Group - does not have property 'values'";
            }

            if (!$group->values instanceof \stdClass) {
                return "Group - values is not a object";
            }
        }

        return "";
    }


    /**
     * Load value files
     *
     * @param string $path
     * @throws \Exception
     */
    private function load($path)
    {

        if (!is_dir($path)) {
            throw new \Exception("Path {$path} not found");
        }

        $dir = new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS);
        $it = new \RecursiveIteratorIterator($dir, \RecursiveIteratorIterator::SELF_FIRST);
        $it->setMaxDepth(0);

        // loop values files
        foreach ($it as $fileInfo) {


            // validate type file
            if (!$fileInfo instanceof \SplFileInfo || !$fileInfo->isFile()) {
                continue;
            }

            // validate extension file
            if (strpos($fileInfo->getFilename(), ".json") === false) {
                continue;
            }

            // validate json file
            if (($groups = json_decode(@file_get_contents($fileInfo->getRealPath()))) == false) {
                continue;
            }

            // validate content file
            if (strlen($this->validateGroups($groups)) > 0) {
                continue;
            }

            array_push($this->files, $fileInfo->getFilename());
            array_push($this->values, $groups);
        }
    }

    /**
     * @param string        $variable
     * @param string|number $value
     * @return $this
     */
    public function setVar($variable, $value)
    {
        $json = json_encode($this->values);
        $json = str_replace("{\${$variable}}", $value, $json);
        $json = json_decode($json);
        if ($json !== false) {
            $this->values = $json;
        }
        return $this;
    }

    /**
     * Get value by group name and property value
     *
     * @param string $groupName
     * @param string $property
     * @return mixed
     */
    public function getValue($groupName, $property)
    {
        foreach ($this->values as $groups) {
            foreach ($groups as $group) {
                if ($group->group != $groupName) {
                    continue;
                }

                if (!isset($group->values->$property)) {
                    continue;
                }

                return $group->values->$property;
            }
        }
        return null;
    }

    /**
     * TODO future implements
     *
     * Define new value in the group
     *
     * @param string $group
     * @param string $property
     * @param mixed  $value
     */
    public function setValue($group, $property, $value)
    {

    }

    /**
     * TODO  future implements
     *
     * Save groups files
     *
     * @return bool
     */
    public function save()
    {

    }

    /**
     * Return all groups
     *
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }


    /**
     * @param string $groupName
     * @return bool
     */
    public function hasGroup($groupName)
    {
        foreach ($this->values as $groups) {
            foreach ($groups as $group) {
                if ($group->group == $groupName) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return one group by name
     *
     * @param string $groupName
     * @return null|\stdClass
     */
    public function getGroup($groupName)
    {
        foreach ($this->values as $groups) {
            foreach ($groups as $group) {
                if ($group->group == $groupName) {
                    return $group->values;
                }
            }
        }
        return null;
    }


    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

} 