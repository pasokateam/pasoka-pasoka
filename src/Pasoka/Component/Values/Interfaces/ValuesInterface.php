<?php

namespace Pasoka\Component\Values\Interfaces;

/**
 * Interface ValuesInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Values\Interfaces
 */
interface ValuesInterface
{
    /**
     * @param string $path
     * @return self
     */
    public static function getInstance($path);

    /**
     * @return mixed
     */
    public function getValues();

    /**
     * @param string $groupName
     * @param string $property
     * @return mixed
     */
    public function getValue($groupName, $property);

    /**
     * @param string $group
     * @param string $property
     * @param mixed  $value
     */
    public function setValue($group, $property, $value);

    /**
     * @param string $groupName
     * @return mixed
     */
    public function getGroup($groupName);

    /**
     * @return bool
     */
    public function save();
} 