<?php
namespace Pasoka\Component\Videos\Youtube;

final class Youtube
{
    /**
     * Chave da API do Google
     * @var String
     */
    public $keyApi;

    public function __construct($keyApi)
    {
        $this->keyApi = $keyApi;
    }

    /**
     * Retorna a instancia da classe playlist
     *
     * @param $playlistID Id da playlist do youtube
     * @param $pathCache Diretorio onde sera armazenado o cache
     * @param $cacheTime Tempo da expiração do cache
     * @return Playlist
     */
    public function playlist($playlistID, $pathCache, $cacheTime = 1800)
    {
        return new Playlist($this->keyApi, $playlistID, $pathCache, $cacheTime);
    }

    /**
     * Retorna a instancia da classe Live
     *
     * @param $channelId Id do canal do youtube
     * @return Live
     */
    public function live($channelId, $pathCache, $cacheTime = 1800)
    {
        return new Live($this->keyApi, $channelId, $pathCache, $cacheTime);
    }

    public function channel($channelId, $order = 'date', $maxResult = 10, $pathCache, $cacheTime = 1800)
    {
        return new Channel($this->keyApi, $channelId, $order, $maxResult, $pathCache, $cacheTime);
    }

    public function video($idVideo, $pathCache, $cacheTime = 1800)
    {
        return new Video($this->keyApi, $idVideo, $pathCache, $cacheTime);
    }

    public function search($channelId, $key, $pathCache, $cacheTime = 1800)
    {
        return new Search($channelId, $key, $pathCache, $cacheTime);
    }
}