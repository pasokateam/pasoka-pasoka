<?php
namespace Pasoka\Component\Videos\Youtube;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Youtube\Interfaces\Youtube as InterfaceYoutube;
use Pasoka\Component\Videos\Youtube\VO\Video as VideoVO;

class Search extends AbstractYoutube implements InterfaceYoutube
{
    const URL_API_YOUTUBE = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=__CHANNELID__&key=__KEY__&type=video&maxResults=__MAX__&order=date&q=__TERM__';

    public function __construct($channelId, $key, $pathCache, $cacheTime = 1800)
    {
        $this->urlApiYouTube = str_replace('__CHANNELID__', $channelId, self::URL_API_YOUTUBE);
        $this->urlApiYouTube = str_replace('__KEY__', $key, $this->urlApiYouTube);
        $this->channelId = $channelId;
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
    }

    public function get($term, $limit = 25)
    {
        $this->urlApiYouTube = str_replace('__MAX__',  $limit, $this->urlApiYouTube);
        $this->urlApiYouTube = str_replace('__TERM__', $term,  $this->urlApiYouTube);

        $cacheName = 'SEARCH_YOUTUBE_' . $limit . $term . $channelId;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);

        if ($fileCache->isExpired($cacheName)) {
            $json = file_get_contents($this->urlApiYouTube);

            /**
             * Valida se o retorno da API do youtube foi OK
             * Caso contrario tenta pegar os videos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Unable to return youtube data');
                }

                return $this->factoryVideo($data);
            }

            $fileCache->add($cacheName, json_decode($json));
            return $this->factoryVideo(json_decode($json));
        } else {
            $data = $fileCache->fetch($cacheName);
            return $this->factoryVideo($data);
        }
    }

    /**
    * Metodo responsavel por popular a classe VO Video e retornar
    * um array de Video
    *
    * @param $data
    * @return array
    */
    private function factoryVideo($data)
    {
        $videos = [];
        foreach ($data->items as $value) {
            $videoVO = new VideoVO();
            $videoVO->setTotalResults($data->pageInfo->totalResults);
            $videoVO->setIdVideo($value->id->videoId);
            $videoVO->setPublicationDate($value->snippet->publishedAt);
            $videoVO->setChannelId($value->snippet->channelId);
            $videoVO->setTitle($value->snippet->title);
            $videoVO->setDescription($value->snippet->description);
            $videoVO->setThumbnails($value->snippet->thumbnails);
            $videoVO->setChannelTitle($value->snippet->channelTitle);

            $videos[] = $videoVO;
        }

        return $videos;
    }
}