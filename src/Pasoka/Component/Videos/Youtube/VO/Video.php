<?php
namespace Pasoka\Component\Videos\Youtube\VO;

class Video implements \JsonSerializable
{
    private $message;
    private $totalResults;
    private $idPlaylist;
    private $idVideo;
    private $publicationDate;
    private $channelId;
    private $title;
    private $description;
    private $thumbnails;
    private $channelTitle;
    private $position;
    private $resourceId;
    private $timeVideo;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param mixed $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResourceId()
    {
        return $this->resourceId;
    }

    /**
     * @param mixed $resourceId
     */
    public function setResourceId(\stdClass $resourceId)
    {
        $this->resourceId = $resourceId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelTitle()
    {
        return $this->channelTitle;
    }

    /**
     * @param mixed $channelTitle
     */
    public function setChannelTitle($channelTitle)
    {
        $this->channelTitle = $channelTitle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getChannelId()
    {
        return $this->channelId;
    }

    /**
     * @param mixed $channelId
     */
    public function setChannelId($channelId)
    {
        $this->channelId = $channelId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getThumbnails()
    {
        return $this->thumbnails;
    }

    /**
     * @param mixed $thumbnails
     */
    public function setThumbnails($thumbnails)
    {
        $this->thumbnails = $thumbnails;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * @param mixed $publicationDate
     */
    public function setPublicationDate($publicationDate)
    {
        $this->publicationDate = $publicationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdVideo()
    {
        return $this->idVideo;
    }

    /**
     * @param mixed $idVideo
     */
    public function setIdVideo($idVideo)
    {
        $this->idVideo = $idVideo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdPlaylist()
    {
        return $this->idPlaylist;
    }

    /**
     * @param mixed $idPlaylist
     */
    public function setIdPlaylist($idPlaylist)
    {
        $this->idPlaylist = $idPlaylist;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTimeVideo()
    {
        return $this->timeVideo;
    }

    /**
     * @param mixed $timeVideo
     */
    public function setTimeVideo($timeVideo)
    {
        $this->timeVideo = $timeVideo;
    }

    /**
     * Esse metodo vem da interface JsonSerializable do PHP o mesmo e
     * chamado quando usamos o objeto na funcao json_encode
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'message'         => $this->message,
            'totalResults'    => $this->totalResults,
            'idPlaylist'      => $this->idPlaylist,
            'idVideo'         => $this->idVideo,
            'publicationDate' => $this->publicationDate,
            'channelId'       => $this->channelId,
            'title'           => $this->title,
            'description'     => $this->description,
            'thumbnails'      => $this->thumbnails,
            'channelTitle'    => $this->channelTitle,
            'position'        => $this->position,
            'resourceId'      => $this->resourceId
        ];
    }
}