<?php
namespace Pasoka\Component\Videos\Youtube;

abstract class AbstractYoutube
{
    /**
     * @var string Url da API onde sera feita a requisição
     */
    protected $urlApiYouTube;

    /**
     * @var Diretorio onde sera armazenado o cache
     */
    protected $pathCache;

    /**
     * @var Tempo de expiração do cache
     */
    protected $cacheTime;
}