<?php
namespace Pasoka\Component\Videos\Youtube;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Youtube\Interfaces\Youtube as InterfaceYoutube;
use Pasoka\Component\Videos\Youtube\VO\Video as VideoVO;

/**
 * Class Channel
 *
 * @package Pasoka\Component\Videos\Youtube
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.1
 */
class Channel extends AbstractYoutube implements InterfaceYoutube
{
    const URL_API_YOUTUBE = 'https://www.googleapis.com/youtube/v3/search?part=snippet,id&channelId=__CHANNEL__&order=__ORDER__&key=__KEY__&maxResults=__MAXRESULT__';

    public function __construct(
        $key,
        $channelId,
        $order = 'date',
        $maxResult = 10,
        $pathCache,
        $cacheTime = 1800
    ) {
        $this->urlApiYouTube = str_replace('__CHANNEL__',   $channelId, self::URL_API_YOUTUBE);
        $this->urlApiYouTube = str_replace('__ORDER__',     $order,     $this->urlApiYouTube);
        $this->urlApiYouTube = str_replace('__KEY__',       $key,       $this->urlApiYouTube);
        $this->urlApiYouTube = str_replace('__MAXRESULT__', $maxResult, $this->urlApiYouTube);
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
        $this->channelId = $channelId;
    }

    public function get()
    {
        $cacheName = 'CHANNEL_YOUTUBE_' . $this->maxResult . $this->channelId;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);
        
        if ($fileCache->isExpired($cacheName)) {
            
            $json = file_get_contents($this->urlApiYouTube);
            
            /**
             * Valida se o retorno da API do youtube foi OK
             * Caso contrario tenta pegar os videos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Unable to return youtube data');
                }

                return $this->factoryVideo($data);
            }

            $fileCache->add($cacheName, json_decode($json));
            return $this->factoryVideo(json_decode($json));
        } else {
            $data = $fileCache->fetch($cacheName);
            return $this->factoryVideo($data);
        }
    }

    /**
     * Metodo responsavel por popular a classe VO Video e retornar
     * um array de Video
     *
     * @param $data
     * @return array
     */
    private function factoryVideo($data)
    {
        $videos = [];
        foreach ($data->items as $itens) {

            $video = new VideoVO();
            $video->setIdVideo($itens->id->videoId);
            $video->setPublicationDate($itens->snippet->publishedAt);
            $video->setChannelId($itens->snippet->channelId);
            $video->setTitle($itens->snippet->title);
            $video->setDescription($itens->snippet->description);
            $video->setThumbnails($itens->snippet->thumbnails);
            $video->setChannelTitle($itens->snippet->channelTitle);

            $videos[] = $video;
        }

        return $videos;
    }
} 