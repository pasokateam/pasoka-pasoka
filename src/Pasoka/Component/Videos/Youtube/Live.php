<?php
namespace Pasoka\Component\Videos\Youtube;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Youtube\Interfaces\Youtube as InterfaceYoutube;
use Pasoka\Component\Videos\Youtube\VO\Video as VideoVO;

/**
 * Class Live
 *
 * @package Pasoka\Component\Videos\Youtube
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.1
 */
class Live extends AbstractYoutube implements InterfaceYoutube
{
    const URL_API_YOUTUBE = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=__CHANNEL__&eventType=live&type=video&key=__KEY__';

    /**
     * Construct
     *
     * @param string $keyApi
     * @param string $channelId
     */
    public function __construct($keyApi, $channelId, $pathCache, $cacheTime = 1800)
    {
        $this->urlApiYouTube = str_replace('__CHANNEL__', $channelId, self::URL_API_YOUTUBE);
        $this->urlApiYouTube = str_replace('__KEY__', $keyApi, $this->urlApiYouTube);
        $this->channelId = $channelId;
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
    }

    /**
     * @return bool||stdClass|array
     */
    public function get()
    {
        $cacheName = 'VIDEO_YOUTUBE_LIVE_' . $this->channelId;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);
        
        if ($fileCache->isExpired($cacheName)) {
            $json = @file_get_contents($this->urlApiYouTube);

            /**
             * Valida se o retorno da API do youtube foi OK
             * Caso contrario tenta pegar os videos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Unable to return youtube data');
                }

                return $this->factoryVideo($data);
            }

            $fileCache->add($cacheName, json_decode($json));
            return $this->factoryVideo(json_decode($json));
        } else {
            $data = $fileCache->fetch($cacheName);
            return $this->factoryVideo($data);
        }

    }

    /**
    * Metodo responsavel por popular a classe VO Video e retornar
    * um array de Video
    *
    * @param $data
    * @return array
    */
    private function factoryVideo($data)
    {
        $video = new VideoVO();
        if (isset($data->items[0])) {
            $video->setIdVideo($data->items[0]->id->videoId);
            $video->setPublicationDate($data->items[0]->snippet->publishedAt);
            $video->setChannelId($data->items[0]->snippet->channelId);
            $video->setTitle($data->items[0]->snippet->title);
            $video->setDescription($data->items[0]->snippet->description);
            $video->setThumbnails($data->items[0]->snippet->thumbnails);
            $video->setChannelTitle($data->items[0]->snippet->channelTitle);

            return $video;
        } else {
            return $video;
        }
    }
}