<?php
namespace Pasoka\Component\Videos\Youtube;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Youtube\Interfaces\Youtube as InterfaceYoutube;
use Pasoka\Component\Videos\Youtube\VO\Video as VideoVO;

/**
 * Class Playlist
 *
 * @package Pasoka\Component\Videos\Youtube
 * @author Danilo Luz <danilo.luz@climatempo.com.br>
 * @version 1.0.1
 */
class Playlist extends AbstractYoutube implements InterfaceYoutube
{
    /**
     * @var String
     */
    private $playlistId;

    const URL_API_YOUTUBE = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,status&maxResults=__MAX__&playlistId=__PLAYLISTID__&key=__KEY__';

    /**
     *
     * @param string $keyApi
     * @param string $playlistID
     * @param string $pathCache
     * @param int    $cacheTime
     */
    public function __construct($keyApi, $playlistID, $pathCache, $cacheTime = 1800)
    {
        $this->urlApiYouTube = str_replace('__PLAYLISTID__', $playlistID, self::URL_API_YOUTUBE);
        $this->urlApiYouTube = str_replace('__KEY__', $keyApi, $this->urlApiYouTube);
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
        $this->playlistId = $playlistID;
    }

    /**
     * Retorna os videos de uma determinada playlist
     *
     * @param int $maxResults Resultado máximo retornado
     * @return array de Video
     */
    public function getVideos($maxResults = 10)
    {
        if ($maxResults < 1) {
            throw new \InvalidArgumentException('Maximum result cannot be less than zero');
        }

        if (!is_int($maxResults)) {
            throw new \InvalidArgumentException('Type of the parameter is not permitted');
        }

        $this->urlApiYouTube = str_replace('__MAX__', $maxResults, $this->urlApiYouTube);

        $cacheName = 'PLAYLIST_YOUTUBE_' . $maxResults . $this->playlistId;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);

        if ($fileCache->isExpired($cacheName)) {
            $json = file_get_contents($this->urlApiYouTube);

            /**
             * Valida se o retorno da API do youtube foi OK
             * Caso contrario tenta pegar os videos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Unable to return youtube data');
                }

                return $this->factoryVideo($data);
            }

            $fileCache->add($cacheName, json_decode($json));
            return $this->factoryVideo(json_decode($json));
        } else {
            $data = $fileCache->fetch($cacheName);
            return $this->factoryVideo($data);
        }
    }

    /**
     * Metodo responsavel por popular a classe VO Video e retornar
     * um array de Video
     *
     * @param $data
     * @return array
     */
    private function factoryVideo($data)
    {
        $videos = array();
        foreach ($data->items as $value) {
            $video = new VideoVO();
            $video->setPublicationDate($value->snippet->publishedAt)
                ->setChannelId($value->snippet->channelId)
                ->setTitle($value->snippet->title)
                ->setDescription($value->snippet->description)
                ->setThumbnails($value->snippet->thumbnails)
                ->setChannelTitle($value->snippet->channelTitle)
                ->setIdPlaylist($value->snippet->playlistId)
                ->setPosition($value->snippet->position)
                ->setResourceId($value->snippet->resourceId)
                ->setIdVideo($value->snippet->resourceId->videoId);
            $videos[] = $video;
        }

        return $videos;
    }
}