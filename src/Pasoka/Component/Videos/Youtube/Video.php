<?php
namespace Pasoka\Component\Videos\Youtube;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Youtube\Interfaces\Youtube as InterfaceYoutube;
use Pasoka\Component\Videos\Youtube\VO\Video as VideoVO;

class Video extends AbstractYoutube implements InterfaceYoutube
{
    const URL_API_YOUTUBE = 'https://www.googleapis.com/youtube/v3/videos?id=__IDVIDEO__&key=__KEY__&part=snippet';

    public function __construct($key, $idVideo, $pathCache, $cacheTime = 1800)
    {
        $this->urlApiYouTube = str_replace('__IDVIDEO__', $idVideo, self::URL_API_YOUTUBE);
        $this->urlApiYouTube = str_replace('__KEY__',     $key,     $this->urlApiYouTube);
        $this->idVideo = $idVideo;
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
    }

    public function get()
    {

        $cacheName = 'VIDEO_YOUTUBE_' . $this->idVideo;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);

        if ($fileCache->isExpired($cacheName)) {
            $json = file_get_contents($this->urlApiYouTube);
            
            /**
             * Valida se o retorno da API do youtube foi OK
             * Caso contrario tenta pegar os videos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Unable to return youtube data');
                }

                return $this->factoryVideo($data);
            }

            $fileCache->add($cacheName, json_decode($json));
            return $this->factoryVideo(json_decode($json));
        } else {
            $data = $fileCache->fetch($cacheName);
            return $this->factoryVideo($data);
        }        
    }

    /**
    * Metodo responsavel por popular a classe VO Video e retornar
    * um array de Video
    *
    * @param $data
    * @return array
    */
    private function factoryVideo($data)
    {
        if (isset($data->items[0])) {

            $video = new VideoVO();
            $video->setIdVideo($data->items[0]->id);
            $video->setPublicationDate($data->items[0]->snippet->publishedAt);
            $video->setChannelId($data->items[0]->snippet->channelId);
            $video->setTitle($data->items[0]->snippet->title);
            $video->setDescription($data->items[0]->snippet->description);
            $video->setThumbnails($data->items[0]->snippet->thumbnails);
            $video->setChannelTitle($data->items[0]->snippet->channelTitle);

            return $video;
        } else {
            throw new \RuntimeException('Video not found');
        }
    }
}