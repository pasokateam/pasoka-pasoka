<?php
namespace Pasoka\Component\Videos;

use Pasoka\Component\Videos\Dailymotion\Dailymotion;
use Pasoka\Component\Videos\Youtube\Youtube;
use Psr\Log\InvalidArgumentException;

/**
 * Class VideosManager
 *
 * @package Pasoka\Component\Videos
 * @author Michel Araujo
 * @version 1.1.0
 */
final class VideosManager
{
    const YOUTUBE = 'youtube';
    const DAILYMOTION = 'dailymotion';

    /**
     * Retorna a instancia da plataforma escolhida
     *
     * @param $apiType
     * @author Michel Araujo
     * @return Dailymotion|Youtube
     */
    public static function getInstance($apiType)
    {
        $arguments = func_get_args();

        switch ($apiType) {
            case self::YOUTUBE:
                if (array_key_exists(1, $arguments)) {

                    if (!is_string($arguments[1])) {
                        throw new InvalidArgumentException('Type of key pass as parameter is invalid');
                    }

                    return new Youtube($arguments[1]);
                } else {
                    throw new \RuntimeException(
                        'you need to pass as parameter the youtube authentication
                        key as the second parameter of getInstance'
                    );
                }
            case self::DAILYMOTION:

                return new Dailymotion();

            default:
                throw new InvalidArgumentException('Video API not found');
        }
    }
}