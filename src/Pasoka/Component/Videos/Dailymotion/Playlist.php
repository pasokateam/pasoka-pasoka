<?php

namespace Pasoka\Component\Videos\Dailymotion;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Dailymotion\Interfaces\Dailymotion as InterfaceDailymotion;

/**
 * Class relacionada a playlists.
 */
final class Playlist extends AbstractDailymotion implements InterfaceDailymotion
{

    /**
     * @var string Código identificador da playlist.
     */
    private $playlistId;

    /**
     * URL base para recuperar dados relacionados à playlist.
     */
    const URL_API_DAILYMOTION = 'https://api.dailymotion.com/playlist/__PLAYLISTID__';

    /**
     * @param string  $playlistId  Código identificador da playlist.
     * @param string  $pathCache   Caminho do diretório de cache.
     * @param int     $cacheTime   Tempo de cache, em segundos. Por padrão é 3 minutos.
     * @param int     $cacheTime
     * @author Michael Felix Dias <michael@climatempo.com.br>
     */
    public function __construct($playlistId, $pathCache, $cacheTime = 180)
    {
        $this->urlApiDailymotion = str_replace('__PLAYLISTID__', $playlistId, self::URL_API_DAILYMOTION);
        $this->pathCache = $pathCache;
        $this->cacheTime = $cacheTime;
        $this->playlistId = $playlistId;
    }

    /**
     * Busca e retorna a lista de vídeos que pertencem a playlist.
     *
     * @param int  $limit  Limite máximo de vídeos retornados.
     * @param int  $page   Número da página criada pela páginação, que foi definida pelo limite de vídeos retornados.
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @return array
     */
    public function getVideos($limit = 10, $page = 1)
    {
        if ($limit < 1) {
            throw new \InvalidArgumentException('Param $limit não pode ser menor que zero.');
        }

        if (! is_int($limit)) {
            throw new \InvalidArgumentException('$limit: Tipo de param não permitido.');
        }

        if ($page < 1) {
            throw new \InvalidArgumentException('Param $page não pode ser menor que zero.');
        }

        if (! is_int($page)) {
            throw new \InvalidArgumentException('$page: Tipo de param não permitido.');
        }

        $this->urlApiDailymotion .= '/videos?' . http_build_query(['limit' => $limit, 'page' => $page]);

        $cacheName = 'PLAYLIST_DAILYMOTION_' . $limit . '_' . $page . '_' . $this->playlistId;
        $fileCache = new FileCache($this->pathCache, $this->cacheTime);

        if ($fileCache->isExpired($cacheName)) {
            $json = file_get_contents($this->urlApiDailymotion);

            /**
             * Valida se o retorno da Api do Dailymotion foi Ok
             * Caso contrario tenta pegar os vídeos do cache
             */
            if (is_null($json) || empty($json) || $json == false) {
                $data = $fileCache->fetch($cacheName);

                if (is_null($data) || $data == false) {
                    throw new \RuntimeException('Impossível retornar dados de Playlist do Dailymotion.');
                }

                return $data;
            }

            $data = json_decode($json);
            $fileCache->add($cacheName, $data);
            return $data;
        } else {
            return $fileCache->fetch($cacheName);
        }
    }
}