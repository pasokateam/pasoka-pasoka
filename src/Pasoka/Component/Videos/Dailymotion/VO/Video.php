<?php
namespace Pasoka\Component\Videos\Dailymotion\VO;

/**
 * Class Video
 *
 * @package Pasoka\Component\Videos\Dailymotion\VO
 * @author Michel Araujo
 * @version 1.0.0
 */
final class Video implements \JsonSerializable
{
    /**
     * @var String
     */
    private $message;

    /**
     * @var Int
     */
    private $totalResults;

    /**
     * @var String
     */
    private $idVideo;

    /**
     * @var String
     */
    private $title;

    /**
     * @var String
     */
    private $channel;

    /**
     * @var String
     */
    private $owner;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getTotalResults()
    {
        return $this->totalResults;
    }

    /**
     * @param mixed $totalResults
     */
    public function setTotalResults($totalResults)
    {
        $this->totalResults = $totalResults;
    }

    /**
     * @return mixed
     */
    public function getIdVideo()
    {
        return $this->idVideo;
    }

    /**
     * @param mixed $idVideo
     */
    public function setIdVideo($idVideo)
    {
        $this->idVideo = $idVideo;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param mixed $channel
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * Esse metodo vem da interface JsonSerializable do PHP o mesmo e
     * chamado quando usamos o objeto na funcao json_encode
     *
     * @author Michel Araujo
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'message'         => $this->message,
            'totalResults'    => $this->totalResults,
            'idVideo'         => $this->idVideo,
            'publicationDate' => $this->owner,
            'title'           => $this->title,
            'description'     => $this->channel
        ];
    }
}