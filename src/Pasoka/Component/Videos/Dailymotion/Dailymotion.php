<?php
namespace Pasoka\Component\Videos\Dailymotion;

use Pasoka\Component\Cache\FileCache;
use Pasoka\Component\Videos\Dailymotion\VO\Video;

/**
 * Class Dailymotion
 *
 * @package Pasoka\Component\Videos\Dailymotion
 * @author Michel Araujo
 * @version 1.0.0
 */
final class Dailymotion
{
    /**
     * URL da api do dailymotion
     */
    const URL_API_DAILYMOTION = 'https://api.dailymotion.com/user/__CHANNEL__/__TYPE__';

    /**
     * Retorna os videos que estão aovivo
     *
     * @param String $channelId Nome do canal (climatempo)
     * @param String $pathCache Caminho do cache
     * @param int $timeCache Tempo do cache por padrão 3 minutos
     * @author Michel Araujo
     * @return array|bool
     */
    public function live($channelId, $pathCache, $timeCache = 180)
    {
        $cacheName = 'DAILYMOTION_LIVE';
        $fileCache = new FileCache($pathCache, $timeCache);

        if ($fileCache->isExpired($cacheName)) {
            $url = str_replace('__CHANNEL__', $channelId, self::URL_API_DAILYMOTION);
            $url = str_replace('__TYPE__', 'videos', $url);
            $url = $url.'?flags=live';

            $json = @file_get_contents($url);
            if ($json === false) {
                return false;
            }

            $data = @json_decode($json);
            if ($data === false) {
                return false;
            }

            $fileCache->add($cacheName, $data);
            return $fileCache->fetch($cacheName);
        } else {
            return $fileCache->fetch($cacheName);
        }
    }

    /**
     * Retorna uma instância da classe Playlist.
     *
     * @param string  $playlistId  Código identificador da playlist.
     * @param string  $pathCache   Caminho do diretório de cache.
     * @param int     $cacheTime   Tempo de cache, em segundos. Por padrão é 3 minutos.
     * @author Michael Felix Dias <michael@climatempo.com.br>
     * @return Playlist
     */
    public function playlist($playlistId, $pathCache, $cacheTime = 180)
    {
        return new Playlist($playlistId, $pathCache, $cacheTime);
    }
}