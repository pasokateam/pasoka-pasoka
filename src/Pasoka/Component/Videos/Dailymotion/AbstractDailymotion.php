<?php

namespace Pasoka\Component\Videos\Dailymotion;

abstract class AbstractDailymotion
{

    /**
     * @var string URL da API onde sera feita a requisição.
     */
    protected $urlApiDailymotion;

    /**
     * @var string Diretório onde será armazenado o cache.
     */
    protected $pathCache;

    /**
     * @var string Tempo de expiração do cache.
     */
    protected $cacheTime;
}