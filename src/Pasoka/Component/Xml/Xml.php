<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Xml;

use Pasoka\Component\Http\Response\Header;
use Pasoka\Component\Http\Response\Interfaces\ResponseInterface;


/**
 * Class Xml
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @version 1.0.0
 * @package Pasoka\Component\Xml
 */
class Xml implements ResponseInterface
{
    /**
     * @var \SimpleXMLElement
     */
    private $xml;

    /**
     * @var \stdClass|array
     */
    private $data;

    /**
     * Construtor
     *
     * @param string $root
     * @param null   $data
     */
    public function __construct($root, $data = null)
    {
        if (!is_string($root)) {
            $root = "feed";
        }
        if (is_object($data) || is_array($data)) {
            $this->data = $data;
        }
        $this->xml = new \SimpleXMLElement("<$root></$root>");
    }

    /**
     * Converte dados array ou object para elementos de
     * uma instancia da classe \SimplesXMLElement
     *
     * @param \stdClass|array   $data
     * @param \SimpleXMLElement $xml
     */
    private function toXML($data = null, \SimpleXMLElement &$xml = null)
    {
        if (is_null($xml)) {
            $xml =& $this->xml;
        }
        if (is_null($data)) {
            $data = $this->data;
        }
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $key = "item";
            }
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    self::toXML($value, $subnode);
                } else {
                    $subnode = $xml->addChild("item$key");
                    self::toXML($value, $subnode);
                }
            } else {
                if (is_object($value) || is_array($value)) {

                    $child = $xml->addChild("$key");
                    self::toXML($value, $child);
                } else {
                    $xml->addAttribute("$key", htmlspecialchars("$value"));
                }
            }
        }
    }

    /**
     * Exibe retorno do head em xml
     */
    public function display()
    {
        $this->toXML();
        (new Header())
            ->add('Content-type: text/xml')
            ->run();
        die($this->xml->asXML());
    }

    /**
     * @return string
     */
    public function encode()
    {
        // TODO: Implement encode() method.
    }

    /**
     * @return \stdClass|array
     */
    public function decode()
    {
        // TODO: Implement decode() method.
    }

    /**
     * @param string $file
     * @return bool
     */
    public function save($file)
    {
        // TODO: Implement save() method.
    }
}