<?php

namespace Pasoka\Component\Cron\Interfaces;

/**
 * Interface RunnableInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Cron\Interfaces
 */
interface RunnableInterface
{

    /**
     * Metodo que sera executado pela cron
     *
     * @return mixed
     */
    public function main();
} 