<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Log;

use Monolog\Formatter\JsonFormatter;
use Pasoka\Component\Log\Interfaces\LoggerManagerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

/**
 * Responsável por gerenciar os recursos de log
 *
 * @author     Michel Araujo - araujo_michel@yahoo.com.br
 * @package    Pasoka
 * @subpackage Pasoka\Component\Log
 * @namespace  Pasoka\Component\Log
 * @version    1.0
 */
abstract class LoggerManager implements LoggerManagerInterface
{
    protected $logger;
    protected $name;
    protected $file;

    /**
     * Construtor responsável por configura a instancia do log
     *
     * @param string $name
     * @param string $file
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($name, $file)
    {
        $this->name = $name;
        $this->file = $file;

        $handler = new StreamHandler($file);
        $handler->setFormatter(new JsonFormatter());

        $this->logger = new Logger($name);
        $this->logger->pushHandler($handler);
    }

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function emergency($message, array $context = array())
    {
        $this->logger->addEmergency($message);
        return $this;
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function alert($message, array $context = array())
    {
        $this->logger->addAlert($message);
        return $this;
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function critical($message, array $context = array())
    {
        $this->logger->addCritical($message);
        return $this;
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function error($message, array $context = array())
    {
        $this->logger->addError($message);
        return $this;
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function warning($message, array $context = array())
    {
        $this->logger->addWarning($message);
        return $this;
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function notice($message, array $context = array())
    {
        $this->logger->addNotice($message);
        return $this;
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function info($message, array $context = array())
    {
        $this->logger->addInfo($message);
        return $this;
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function debug($message, array $context = array())
    {
        $this->logger->addDebug($message);
        return $this;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed  $level
     * @param string $message
     * @param array  $context
     *
     * @return null
     */
    public function log($level, $message, array $context = array())
    {
        $this->logger->log($level, $message);
        return $this;
    }

    /**
     * Verifica o tipo do erro e grava no arquivo
     *
     * @param string $message
     * @param int    $type
     *
     * @throws \Exception
     * @return null|\Pasoka\Component\Log\LoggerManager
     */
    public function save($message, $type)
    {
        switch ($type) {
            case LogLevel::EMERGENCY:
                return $this->emergency($message);
                break;
            case LogLevel::ALERT:
                return $this->alert($message);
                break;
            case LogLevel::CRITICAL:
                return $this->critical($message);
                break;
            case LogLevel::DEBUG:
                return $this->debug($message);
                break;
            case LogLevel::ERROR:
                return $this->error($message);
                break;
            case LogLevel::INFO:
                return $this->info($message);
                break;
            case LogLevel::NOTICE:
                return $this->notice($message);
                break;
            case LogLevel::WARNING:
                return $this->warning($message);
                break;
            default:
                throw new \Exception('Type error not definite');
        }
    }
}