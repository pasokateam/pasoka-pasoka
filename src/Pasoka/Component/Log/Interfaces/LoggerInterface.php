<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Log\Interfaces;

/**
 * @author michel
 */
interface LoggerInterface
{
    /**
     * @param $name
     * @param $file
     */
    public function __construct($name, $file);

    /**
     * @param $message
     * @param $type
     * @return mixed
     */
    public function writer($message, $type);
}