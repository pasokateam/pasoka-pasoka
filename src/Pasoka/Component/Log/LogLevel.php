<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Log;

/**
 * Defini constantes com os níveis de log suportados
 *
 * @author     Michel Araujo - araujo_michel@yahoo.com.br
 * @package    Pasoka
 * @subpackage Pasoka\Component\Log
 * @namespace  Pasoka\Component\Log
 * @version    1.0
 */
final class LogLevel
{
    const EMERGENCY = 'emergency';
    const ALERT = 'alert';
    const CRITICAL = 'critical';
    const ERROR = 'error';
    const WARNING = 'warning';
    const NOTICE = 'notice';
    const INFO = 'info';
    const DEBUG = 'debug';
}