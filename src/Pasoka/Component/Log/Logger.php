<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Log;

use \InvalidArgumentException;
use Pasoka\Component\Log\Interfaces\LoggerInterface;

/**
 * Responsável por seta as configurações de log e
 * chamar o responsável para gravar o log no arquivo
 *
 * @author     Michel Araujo - araujo_michel@yahoo.com.br
 * @package    Pasoka
 * @subpackage Pasoka\Component\Log
 * @namespace  Pasoka\Component\Log
 * @version    1.0
 */
class Logger extends LoggerManager implements LoggerInterface
{
    /**
     * Construtor responsável por configura a instancia do log
     *
     * @param string $name
     * @param string $file
     * @throws InvalidArgumentException
     */
    public function __construct($name, $file)
    {
        if (is_null($file) || !is_file($file)) {
            throw new \InvalidArgumentException('File not found');
        }

        parent::__construct($name, $file);
    }

    /**
     * Chama o metodo save responsável por gravar o log no arquivo
     *
     * @see \Pasoka\Component\Log\Interfaces\LoggerInterface::writer()
     */
    public function writer($message, $type)
    {
        return $this->save($message, $type);
    }
}