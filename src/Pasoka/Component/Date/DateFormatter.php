<?php

namespace Pasoka\Component\Date;

/**
 * Class DateFormatter
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @version 1.0.0
 * @package Pasoka\Component\Date
 */
class DateFormatter
{

    /**
     * @var string
     */
    const FORMAT_DEFAULT = "Y-m-d H:i:s";

    /**
     * @param null|\DateTime $dateTime
     * @param string         $format
     *
     * @return null|string
     */
    public static function getString($dateTime = null, $format = self::FORMAT_DEFAULT)
    {
        if ($dateTime instanceof \DateTime) {

            if (is_null($format)) {
                $format = self::FORMAT_DEFAULT;
            }

            return $dateTime->format($format);
        }

        return null;
    }

} 