<?php
namespace Pasoka\Component\Date;

class TranslationWeek
{
    private $week = [
        'PT' => [
            'Domingo',
            'Segunda-feira',
            'Terça-feira',
            'Quarta-feira',
            'Quinta-feira',
            'Sexta-feira',
            'Sábado'
        ]
    ];

    public function getWeekDay($language, $day)
    {
        if (!is_int($day)) {
            throw new \InvalidArgumentException('Type of parameter invalid');
        }

        return $this->week[$language][$day];
    }
}