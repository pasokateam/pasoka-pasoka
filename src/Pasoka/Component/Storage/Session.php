<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Storage;

/**
 * Classe que manipula dados nas sessões.
 *
 * @author     Michael Dias <michael@climatempo.com.br>
 * @package    Pasoka
 * @subpackage Component\Storage
 * @version    1.0.0
 */
class Session extends AbstractStorage
{

    /**
     *
     * Singleton Pattern.
     * Esta propriedade representa a única instância desta classe.
     */
    private static $instance;

    /**
     *
     * Singleton Pattern.
     * Este método construtor é executado uma única vez.
     * Inicia o mecanismo de sessão do PHP.
     *
     * @param string $name
     */
    private function __construct($name)
    {
        session_name($name);
        session_start();
    }

    /**
     *
     * Singleton Pattern.
     * Não é possível clonar a única instância desta classe.
     */
    private function __clone() {}

    /**
     *
     * Obtém a única instância possível desta classe.
     *
     * @see    self::$instance
     * @param  string $name
     * @return self
     */
    public static function getInstance($name = 'PHPSESSID')
    {
        if (! self::$instance instanceof self) {
            self::$instance = new self($name);
        }

        return self::$instance;
    }

    /**
     *
     * Destrói todos os dados na sessão.
     *
     * @return bool
     */
    public function destroy()
    {
        return session_destroy();
    }

    /**
     *
     * Obtém todos os dados na sessão.
     *
     * @return array
     */
    public function getAll()
    {
        return $_SESSION;
    }

    /**
     *
     * Registra ou atualiza um dado na sessão.
     *
     * @param string $offset
     * @param mixed  $value
     */
    public function offsetSet($offset, $value)
    {
        $_SESSION[$offset] = $value;
    }

    /**
     *
     * Obtém um dado na sessão.
     *
     * @param  string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $_SESSION[$offset] : null;
    }

    /**
     *
     * Verifica a existência de um dado na sessão.
     *
     * @param  string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $_SESSION)) {
            return true;
        }

        return false;
    }

    /**
     *
     * Remove um dado na sessão.
     *
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        unset($_SESSION[$offset]);
    }

    /**
     *
     * Obtém a quantidade de dados armazenados.
     *
     * @return int
     */
    public function count()
    {
        return count($_SESSION);
    }
}