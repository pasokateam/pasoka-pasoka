<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Storage;

/**
 * Classe abstrata que disponibiliza uma interface para os mecanismos
 * de armazenamentos (storage).
 *
 * @author     Michael Dias <michael@climatempo.com.br>
 * @package    Pasoka
 * @subpackage Component\Storage
 * @version    1.0.0
 */
abstract class AbstractStorage implements \ArrayAccess, \Countable
{

    /**
     * Destrói todos os dados armazenados.
     *
     * @return bool
     */
    abstract public function destroy();

    /**
     * Obtém todos os dados armazenados.
     *
     * @return array
     */
    abstract public function getAll();
}