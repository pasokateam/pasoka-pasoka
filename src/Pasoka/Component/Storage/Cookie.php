<?php
/**
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Storage;

/**
 * Classe que manipula dados de cookies.
 *
 * @author     Michael Dias <michael@climatempo.com.br>
 * @package    Pasoka
 * @subpackage Component\Storage
 * @version    1.0.0
 */
class Cookie extends AbstractStorage
{

    /**
     *
     * Singleton Pattern.
     * Esta propriedade representa a única instância desta classe.
     */
    private static $instance;

    /**
     *
     * Singleton Pattern.
     * Este método construtor é executado uma única vez.
     */
    private function __construct()
    {
    }

    /**
     *
     * Singleton Pattern.
     * Não é possível clonar a única instância desta classe.
     */
    private function __clone()
    {
    }

    /**
     *
     * Obtém a única instância possível desta classe.
     *
     * @see    self::$instance
     * @return self
     */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     *
     * Destrói todos os dados de cookies.
     *
     * @return bool
     */
    public function destroy()
    {
        foreach ($this->getAll() as $name => $value) {
            $this->offsetUnset($name);
        }

        return $this->count() == 0 ? true : false;
    }

    /**
     *
     * Obtém todos os dados de cookies.
     *
     * @return array
     */
    public function getAll()
    {
        return $_COOKIE;
    }

    /**
     *
     * Registra ou atualiza um dado de cookie.
     *
     * @param string $offset
     * @param mixed  $value
     * @param int    $expire
     * @param string $path
     * @param string $domain
     * @param bool   $secure
     * @param bool   $httpOnly
     */
    public function offsetSet(
        $offset,
        $value,
        $expire = null,
        $path = null,
        $domain = null,
        $secure = null,
        $httpOnly = null)
    {
        setcookie($offset, $value, $expire, $path, $domain, $secure, $httpOnly);
    }

    /**
     *
     * Obtém um dado de cookie.
     *
     * @param  string $offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $_COOKIE[$offset] : null;
    }

    /**
     *
     * Verifica a existência de um dado de cookie.
     *
     * @param  string $offset
     * @return bool
     */
    public function offsetExists($offset)
    {
        if (array_key_exists($offset, $_COOKIE)) {
            return true;
        }

        return false;
    }

    /**
     *
     * Remove um dado de cookie.
     *
     * @param string $offset
     */
    public function offsetUnset($offset)
    {
        if ($this->offsetExists($offset)) {
            setcookie($offset, null);
        }
    }

    /**
     *
     * Obtém a quantidade de dados armazenados.
     *
     * @return int
     */
    public function count()
    {
        return count($_COOKIE);
    }
}