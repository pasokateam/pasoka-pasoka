<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Http\Request;

use Pasoka\Component\Http\Request\Interfaces\RequestFilterInterface;

/**
 * Classe para obter dados externos via resquisicao GET
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @author     Michael Dias - michael@climatempo.com.br
 * @package    Pasoka
 * @subpackage Component\Request
 * @namespace  Pasoka\Component\Request
 * @version    1.0.0
 */
class Get extends AbstractFilter implements RequestFilterInterface
{
    /**
     * Retorna um dado externo como string.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return string null
     */
    public static function getString($variableName)
    {
        return parent::filterString($_GET, $variableName);
    }


    /**
     * Retorna um dado externo como inteiro.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return int null
     */
    public static function getInt($variableName)
    {
        return parent::filterInt($_GET, $variableName);
    }

    /**
     * Retorna um dado externo como float.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return float null
     */
    public static function getFloat($variableName)
    {
        return parent::filterFloat($_GET, $variableName);
    }

    /**
     * Retorna um dados externo como e-mail.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return string|null
     */
    public static function getEmail($variableName)
    {
        return parent::filterEmail($_GET, $variableName);
    }

    /**
     * Retorna um dados externo como url.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return string|null
     */
    public static function getUrl($variableName)
    {
        return parent::filterUrl($_GET, $variableName);
    }

    /**
     * Retorna um dado externo como array de string.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return string[]
     */
    public static function getArrayString($variableName)
    {
        return parent::filterArrayString($_GET, $variableName);
    }

    /**
     * Retorna um dado externo como array de inteiro.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return int[]
     */
    public static function getArrayInt($variableName)
    {
        return parent::filterArrayInt($_GET, $variableName);
    }

    /**
     * Retorna um dado externo como array de float.
     *
     * @access public
     *
     * @param string $variableName
     *
     * @return float[]
     */
    public static function getArrayFloat($variableName)
    {
        return parent::filterArrayFloat($_GET, $variableName);
    }

    /**
     * @param string $variableName
     * @param string $format
     *
     * @return \DateTime
     */
    public static function getDateTime($variableName, $format = "Y-m-d H:i:s")
    {
        return parent::filterDateTime($_GET, $variableName, $format);
    }
}