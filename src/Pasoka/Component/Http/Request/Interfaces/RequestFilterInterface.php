<?php

namespace Pasoka\Component\Http\Request\Interfaces;

/**
 * Interface RequestFilterInterface
 *
 * @author  michel
 * @version 1.0.0
 * @package Pasoka\Component\Http\Request\Interfaces
 */
interface RequestFilterInterface
{
    /**
     * @param string $variableName
     *
     * @return string|null
     */
    public static function getString($variableName);

    /**
     * @param string $variableName
     *
     * @return int|null
     */
    public static function getInt($variableName);

    /**
     * @param string $variableName
     *
     * @return float|null
     */
    public static function getFloat($variableName);

    /**
     * @param string $variableName
     *
     * @return string|null
     */
    public static function getEmail($variableName);

    /**
     * @param string $variableName
     *
     * @return string|null
     */
    public static function getUrl($variableName);

    /**
     * @param string $variableName
     *
     * @return string[]
     */
    public static function getArrayString($variableName);

    /**
     * @param string $variableName
     *
     * @return int[]
     */
    public static function getArrayInt($variableName);

    /**
     * @param string $variableName
     *
     * @return float[]
     */
    public static function getArrayFloat($variableName);


    /**
     * @param string $variableName
     * @param string $format
     *
     * @return \DateTime
     */
    public static function getDateTime($variableName, $format = "Y-m-d H:i:s");
}