<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Http\Request;

use Pasoka\Component\Http\Request\Interfaces\RequestFilterInterface;

/**
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @author     Michael Dias - michael@climatempo.com.br
 * @package    Pasoka\Component\Http\Request
 */
final class Request extends AbstractFilter implements RequestFilterInterface
{
    /**
     * @param string $variableName
     * @return string|null
     */
    public static function getString($variableName)
    {
        return parent::filterString($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return int|null
     */
    public static function getInt($variableName)
    {
        return parent::filterInt($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return float|null
     */
    public static function getFloat($variableName)
    {
        return parent::filterFloat($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return string|null
     */
    public static function getEmail($variableName)
    {
        return parent::filterEmail($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return string|null
     */
    public static function getUrl($variableName)
    {
        return parent::filterUrl($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return string[]
     */
    public static function getArrayString($variableName)
    {
        return parent::filterArrayString($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return int[]
     */
    public static function getArrayInt($variableName)
    {
        return parent::filterArrayInt($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @return float[]
     */
    public static function getArrayFloat($variableName)
    {
        return parent::filterArrayFloat($_REQUEST, $variableName);
    }

    /**
     * @param string $variableName
     * @param string $format
     * @return \DateTime
     */
    public static function getDateTime($variableName, $format = "Y-m-d H:i:s")
    {
        return parent::filterDateTime($_REQUEST, $variableName, $format);
    }
}