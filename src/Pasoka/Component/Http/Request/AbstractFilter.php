<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Http\Request;

/**
 * Super Classe de input para POST e GET.
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @author     Michael Dias - michael@climatempo.com.br
 * @package    Pasoka
 * @subpackage Component\Request
 * @namespace  Pasoka\Component\Request
 * @version    1.0.0
 */
abstract class AbstractFilter
{
    /**
     * Dependency Injection do método nativo filter_var.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     * @param int    $filter
     * @param int    $options
     *
     * @return boolean|mixed
     */
    protected static function filter(
        $superGlobal,
        $variableName,
        $filter = null,
        $options = null
    )
    {
        if (!is_array($superGlobal)
            || !array_key_exists($variableName, $superGlobal)
        ) {
            return false;
        }

        return filter_var($superGlobal[$variableName], $filter, $options);
    }

    /**
     * Retorna um dado externo como string.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string
     */
    protected static function filterString($superGlobal, $variableName)
    {
        $string = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_STRING
        );

        $string = strip_tags($string);
        if ($string !== false) {
            return (string)$string;
        }

        return null;
    }

    /**
     * Retorna um dado externo como inteiro.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string
     */
    protected static function filterInt($superGlobal, $variableName)
    {
        $int = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_NUMBER_INT
        );

        if ($int !== false) {
            return (int)$int;
        }

        return null;
    }

    /**
     * Retorna um dado externo como float.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string
     */
    protected static function filterFloat($superGlobal, $variableName)
    {
        $float = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_NUMBER_FLOAT
        );

        if ($float !== false) {
            return (float)$float;
        }

        return null;
    }

    /**
     * Retorna um dado externo
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string
     */
    protected static function filterEmail($superGlobal, $variableName)
    {
        $email = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_EMAIL
        );

        if ($email !== false && !empty($email)) {
            return $email;
        }

        return null;
    }

    /**
     * Retorna um dado externo url
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string
     */
    protected static function filterUrl($superGlobal, $variableName)
    {
        $url = self::filter($superGlobal, $variableName, FILTER_SANITIZE_URL);
        if ($url !== false) {
            return $url;
        }

        return null;
    }

    /**
     * Retorna um dado externo como array de string.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return string[]
     */
    protected static function filterArrayString($superGlobal, $variableName)
    {
        $array = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_STRING,
            FILTER_REQUIRE_ARRAY
        );

        if ($array !== false) {
            return $array;
        }

        return [];
    }

    /**
     * Retorna um dado externo como array de int.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return int[]
     */
    protected static function filterArrayInt($superGlobal, $variableName)
    {
        $array = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_NUMBER_INT,
            FILTER_REQUIRE_ARRAY
        );

        if ($array !== false) {
            for ($i = 0, $m = count($array); $i < $m; $i++) {
                $array[$i] = (int)$array[$i];
            }

            return $array;
        }

        return [];
    }

    /**
     * Retorna um dado externo como array de float.
     *
     * @access protected
     *
     * @param array  $superGlobal
     * @param string $variableName
     *
     * @return float[]
     */
    protected static function filterArrayFloat($superGlobal, $variableName)
    {
        $array = self::filter(
            $superGlobal,
            $variableName,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_REQUIRE_ARRAY
        );

        if ($array !== false) {
            for ($i = 0, $m = count($array); $i < $m; $i++) {
                $array[$i] = (float)$array[$i];
            }

            return $array;
        }

        return [];
    }


    /**
     * Retorna um dado externo como \DateTime.
     *
     * @access public
     *
     * @param array  $superGlobal ($_GET|$_POST)
     * @param string $variableName
     * @param string $format
     *
     * @return \DateTime|null
     */
    public static function filterDateTime($superGlobal,
                                          $variableName,
                                          $format = "Y-m-d H:i:s")
    {
        $getDate = self::filterString($superGlobal, $variableName);
        $datetime = \DateTime::createFromFormat($format, $getDate);
        if ($datetime && $datetime->format($format) == $getDate) {
            return $datetime;
        }

        return null;
    }
}