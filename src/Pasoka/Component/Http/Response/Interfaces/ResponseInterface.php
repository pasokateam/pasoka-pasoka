<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Http\Response\Interfaces;

/**
 * Interface ResponseInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @version 1.0.0
 * @package Pasoka\Interfaces
 */
interface ResponseInterface
{

    /**
     * @return string
     */
    public function encode();

    /**
     * @return \stdClass|array
     */
    public function decode();

    /**
     * Envia response
     */
    public function display();

    /**
     * @param string $file
     *
     * @return bool
     */
    public function save($file);
}