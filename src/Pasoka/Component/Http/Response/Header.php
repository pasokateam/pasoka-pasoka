<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Http\Response;

/**
 * Classe para alterar o header.
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Component\Response
 * @namespace  Pasoka\Component\Response
 * @version    1.0.0
 */
class Header
{
    /**
     * @access private
     * @var string[]
     */
    private $params = [];

    /**
     * @access public
     *
     * @param string $param
     *
     * @return $this
     */
    public function add($param)
    {
        $this->params[] = $param;

        return $this;
    }

    /**
     * Executa a função header do PHP
     *
     * @access public
     * @return void
     */
    public function run()
    {
        for ($i = 0, $m = count($this->params); $i < $m; $i++) {
            @header($this->params[$i]);
        }
    }

    /**
     * Define um headerCallback
     *
     * @param $callback
     * @return boolean
     */
    public function callback($callback)
    {
        if (is_callable($callback)) {
            return header_register_callback($callback);
        } else {
            return false;
        }
    }
}