<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Annotation\Interfaces;

/**
 * Interface AnnotationInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Component\Annotation\Interfaces
 */
interface AnnotationInterface
{


    /**
     * @param string      $comment
     * @param string|null $from
     * @return bool
     */
    public static function hide($comment, $from = null);

} 