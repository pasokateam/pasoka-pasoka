<?php
/*!
 * This file is part the Pasoka package.
 *
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Component\Annotation;

use Pasoka\Component\Annotation\Interfaces\AnnotationInterface;

/**
 * Class Annotation
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @version 1.0.0
 * @package Pasoka\Component\Object
 */
final class Annotation implements AnnotationInterface
{

    /**
     * Metodo a ser desenvolvido para remover responsabilidade
     * de obter annotations de outros metodos desta classe.
     *
     * @param string $comment
     * @param string $type
     *
     * @return mixed|null
     */
//    private static function getAnnotation($comment, $type)
//    {
//        if (strpos($comment, "@{$type}") !== false) {
//
//            $comment = str_replace(["/", "*", " ", "\n"], "", $comment);
//            $comment = str_replace('\\', "\\\\", $comment);
//            $comment = explode("@", $comment);
//
//            foreach ($comment as $annotation) {
//
//                if (strpos(strtolower($annotation), $type) !== false) {
//
//                    if (preg_match('/\((.*?)\)/', $annotation, $matches)) {
//
//                        $config = json_decode($matches[1]);
//
//                        if (!is_null($config)) {
//                            return $config;
//                        }
//                    }
//                } else {
//                    return [];
//                }
//            }
//
//        }
//
//        return null;
//    }

//    /**
//     * @param string $comment
//     */
//    public static function date($comment)
//    {
//    }

    /**
     * @hide(_CONFIG_)
     * Nao deixa uma propriedade ser impressa.
     *
     * - _CONFIG_: Array de classes de origem que nao devem imprimir o valor
     *
     * @param string $comment
     * @param string $from
     *
     * @return bool
     */
    public static function hide($comment, $from = null)
    {

        if (strpos($comment, "@hide") !== false) {

            $comment = str_replace(["/", "*", " ", "\n"], "", $comment);
            $comment = str_replace('\\', "\\\\", $comment);
            $comment = explode("@", $comment);

            foreach ($comment as $annotation) {

                if (strpos(strtolower($annotation), "hide") !== false) {

                    if (preg_match('/\((.*?)\)/', $annotation, $matches)) {

                        $config = json_decode($matches[1]);

                        if (is_null($config) ||
                            (is_array($config) && array_search($from, $config) !== false)
                        ) {
                            return true;
                        }

                    } else {
                        return true;
                    }
                }
            }
        }

        return false;
    }
} 