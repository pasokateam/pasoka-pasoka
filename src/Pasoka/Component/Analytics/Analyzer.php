<?php

namespace Pasoka\Component\Analytics;

use Pasoka\Component\Analytics\VO\Application;
use Pasoka\Component\Analytics\VO\Log;
use Pasoka\Component\Analytics\VO\LogDateTime;
use Pasoka\Component\Analytics\VO\LogLevel;
use Pasoka\Component\Analytics\VO\Server;
use Pasoka\Component\Http\Request\Post;
use Pasoka\Component\Json\Json;
use Pasoka\Component\Log\Logger;

/**
 * Class Analyzer
 *
 * @package Pasoka\Component\Analytics
 */
final class Analyzer
{

    /**
     * @var Patient
     */
    private $patient;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * Constructor
     *
     * @param Patient $patient
     * @param Logger  $logger
     *
     * @throws \Exception
     */
    public function __construct(Patient $patient, Logger $logger)
    {
        $this->patient = $patient;
        $this->logger = $logger;
        $token = Post::getString("token");

        // validate access
        if (!$this->hasIpAccess($patient->getServerList(), $this->getClientIp())) {
            throw new \Exception("Unauthorized server ip address");
        }

        if (is_null($token) || ($token === (string)$patient->getToken()) === false) {
            throw new \Exception("Invalid token");
        }
    }

    /**
     * Get Server Info
     *
     * @return Server
     */
    private function getServerInfo()
    {
        return (new AnalyzerServer(
            new Server()
        ))->getServer();
    }

    /**
     * Get client request ip address
     *
     * @return mixed|string
     */
    private function getClientIp()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

    /**
     * Enable PHP error handler
     */
    public function errorHandler()
    {
        $instance = $this;
        @error_reporting(0);
        @ini_set("display_errors", "off");
        @set_error_handler(function ($errorNo, $errorMessage, $errorFile, $errorLine) use ($instance) {
            $message = "{$errorMessage} (line {$errorLine} in {$errorFile})";
            switch ($errorNo) {
                case E_ERROR:
                case E_PARSE:
                    $instance->logger->error($message);
                    break;
                case E_WARNING:
                    $instance->logger->warning($message);
                    break;
                case E_NOTICE:
                    $instance->logger->notice($message);
                    break;
                default:
                    $instance->logger->info($message);
                    break;
            }
            return true;
        }, E_ALL);
    }


    /**
     * Convert stdClass to Log
     *
     * @param \stdClass  $obj
     * @param     string $logFile
     *
     * @return Log
     */
    private function stdClassToLog($obj = null, $logFile)
    {

        if (!$obj instanceof \stdClass) {
            return null;
        }

        $ref = new \ReflectionObject($obj);
        $log = new Log();
        $level = new LogLevel();

        $log->setFile($logFile);

        if ($ref->hasProperty("message")) {
            $log->setMessage($obj->message);
        }
        if ($ref->hasProperty("channel")) {
            $log->setChannel($obj->channel);
        }

        if ($ref->hasProperty("datetime") && $obj->datetime instanceof \stdClass) {
            $refDate = new \ReflectionObject($obj->datetime);
            $date = new LogDateTime();
            if ($refDate->hasProperty("date")) {
                $date->setDate($obj->datetime->date);
            }
            if ($refDate->hasProperty("timezone")) {
                $date->setTimezone($obj->datetime->timezone);
            }
            $log->setDateTime($date);
        }

        if ($ref->hasProperty("level")) {
            $level->setLevel($obj->level);
        }
        if ($ref->hasProperty("level_name")) {
            $level->setName($obj->level_name);
        }

        $log->setLevel($level);

        return $log;
    }

    /**
     * Get application Info
     *
     * @return Application
     */
    private function getApplicationInfo()
    {
        $application = new Application();
        $application->setServer($this->getServerInfo());
        $application->setName($this->patient->getName());

        // log directories
        foreach ($this->patient->getLogDirectories() as $directory) {

            if (!is_dir($directory)) {
                continue;
            }

            // log files
            foreach ($this->getLogFiles($directory) as $logFile) {

                if (!file_exists($logFile) ||
                    ($logs = @file_get_contents($logFile)) === false
                ) {
                    continue;
                }

                $rows = explode("\n", $logs);

                if (count($rows) > 0) {

                    foreach ($rows as $row) {

                        if (($obj = @json_decode($row)) === false || is_null($obj)) {
                            continue;
                        }

                        $log = $this->stdClassToLog($obj, $logFile);

                        if (!is_null($log)) {
                            $application->addLog($log);
                        }

                    }
                }
            }
        }

        $application->orderLogsByDate();
        $application->limitLogs($this->patient->getLogLimit());
        return $application;
    }

    /**
     * Get log files
     *
     * @param string $directory
     *
     * @return \string[]
     */
    private function getLogFiles($directory)
    {
        $files = [];
        $dir = new \RecursiveDirectoryIterator(
            $directory,
            \FilesystemIterator::SKIP_DOTS
        );
        $it = new \RecursiveIteratorIterator(
            $dir,
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $it->setMaxDepth(0);

        foreach ($it as $fileInfo) {
            if ($fileInfo instanceof \SplFileInfo && $fileInfo->isFile()) {
                if (strpos($fileInfo->getRealPath(), ".log") !== false) {
                    $files[] = $fileInfo->getRealPath();
                }
            }
        }

        return $files;
    }

    /**
     * Get logger component instance
     *
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }


    /**
     * Validate client access
     *
     * @param string[] $listIp
     * @param string   $clientIp
     *
     * @return bool
     */
    private function hasIpAccess($listIp = [], $clientIp = null)
    {
        if (is_null($clientIp)) {
            return true;
        }

        if (count($listIp) === 0) {
            return true;
        }

        if (array_search($clientIp, $listIp) !== false) {
            return true;
        }

        return false;

    }

    /**
     * Display JSON analyzer
     */
    public function display()
    {
        (new Json(
            $this
                ->getApplicationInfo()
                ->valueOf()
        ))->display();
    }
}