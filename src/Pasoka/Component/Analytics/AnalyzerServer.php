<?php

namespace Pasoka\Component\Analytics;

use Pasoka\Component\Analytics\VO\Cpu;
use Pasoka\Component\Analytics\VO\Disk;
use Pasoka\Component\Analytics\VO\Memory;
use Pasoka\Component\Analytics\VO\Server;

/**
 * Class AnalyzerServer
 *
 * @package Pasoka\Component\Analytics
 */
final class AnalyzerServer
{

    /**
     * @var Server
     */
    private $server;

    /**
     * Constructor
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Execute terminal command
     *
     * @param $command
     *
     * @return string
     */
    private function terminal($command)
    {
        return @shell_exec("{$command}");
    }

    /**
     * Fetch CPU info
     */
    private function fetchCpuInfo()
    {
        $command = $this->terminal("cat /proc/cpuinfo");
        $cpuList = array();
        $cpuIndex = 0;

        if ($command) {

            $rows = explode("\n", $command);

            foreach ($rows as $row) {

                $column = explode(":", $row);

                if (count($column) === 2) {

                    $columnName = trim($column[0]);
                    $columnName = strtolower($columnName);

                    if ($columnName === "model name") {

                        if (!isset($cpuList[$cpuIndex])) {
                            $cpuList[$cpuIndex] = new Cpu(
                                trim($column[1])
                            );
                        }
                    }
                } else {
                    $cpuIndex++;
                }
            }

            $this->server->setCpu($cpuList);
        }
        return $this;
    }

    /**
     * Fetch server info
     */
    private function fetchServerInfo()
    {
        $command = $this->terminal("uptime");

        if ($command) {

            $columns = explode(", ", $command);
            $totalColumns = count($columns);

            if ($totalColumns > 4) {

                // UP TIME
                $upTime = explode("up", $columns[0]);

                if (count($upTime) === 2) {
                    $this->server->setUpTime(trim($upTime[1]));
                }

                // LOAD LIST
                $loadList = array();
                for ($loadIndex = 2; $loadIndex < $totalColumns; $loadIndex += 1) {
                    $loadValue = $columns[$loadIndex];
                    $loadValue = str_replace("load average:", "", $loadValue);
                    $loadValue = trim($loadValue);

                    if (is_numeric($loadValue)) {
                        $loadList[] = (float)$loadValue;
                    }
                }
                $this->server->setLoad($loadList);

                // DATE
                $this->server->setDate(new \DateTime("NOW"));

                // IP
                if (isset($_SERVER["SERVER_ADDR"])) {
                    $this->server->setIp($_SERVER["SERVER_ADDR"]);
                }

                // NAME
                if (isset($_SERVER["SERVER_NAME"])) {
                    $this->server->setName($_SERVER["SERVER_NAME"]);
                }
            }
        }
        return $this;
    }

    /**
     * Fetch memory info
     */
    private function fetchMemory()
    {
        $command = $this->terminal("free -m");

        if ($command) {

            $rows = explode("\n", $command);

            if (count($rows) > 0) {
                $columns = explode(" ", $rows[1]);
                $columns = array_filter($columns, function ($val) {
                    return strlen(trim($val)) > 0;
                });
                $columns = array_values($columns);

                if (count($columns) > 4) {
                    $memory = new Memory();
                    $memory->setTotal((float)$columns[1]);
                    $memory->setUsed((float)$columns[2]);
                    $memory->setFree((float)$columns[3]);
                    $this->server->setMemory($memory);
                }
            }
        }
        return $this;
    }


    /**
     * Fetch disk info
     */
    private function fetchDisk()
    {
        $command = $this->terminal("df -h");

        if ($command) {

            $rows = explode("\n", $command);

            if (count($rows) > 0) {
                $columns = explode(" ", $rows[1]);
                $columns = array_filter($columns, function ($val) {
                    return strlen(trim($val)) > 0;
                });
                $columns = array_values($columns);

                if (count($columns) > 4) {
                    $disk = new Disk();
                    $disk->setSize((float)$columns[1]);
                    $disk->setUsed((float)$columns[2]);
                    $disk->setAvail((float)$columns[3]);
                    $disk->setUse($columns[4]);
                    $this->server->setDisk($disk);
                }
            }
        }
        return $this;
    }


    /**
     * Get server info
     *
     * @return Server
     */
    public function getServer()
    {
        return $this
            ->fetchCpuInfo()
            ->fetchServerInfo()
            ->fetchMemory()
            ->fetchDisk()
            ->server;
    }

}