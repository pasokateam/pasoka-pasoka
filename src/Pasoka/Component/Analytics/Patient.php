<?php

namespace Pasoka\Component\Analytics;

use Pasoka\Component\Cache\Interfaces\HashInterface;
use Pasoka\Component\Manifest\Config\ModuleConfig;

/**
 * Class Patient
 *
 * @package Pasoka\Component\Analytics
 */
final class Patient
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var HashInterface
     */
    private $token;

    /**
     * @var string[]
     */
    private $serverList;

    /**
     * @var boolean
     */
    private $framework;

    /**
     * @var ModuleConfig
     */
    private $moduleConfig;

    /**
     * @var string[]
     */
    private $logDirectories;

    /**
     * @var number
     */
    private $logLimit;

    /**
     * @return number
     */
    public function getLogLimit()
    {
        return $this->logLimit;
    }

    /**
     * @param number $logLimit
     *
     * @return $this
     */
    public function setLogLimit($logLimit)
    {
        $this->logLimit = $logLimit;
        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return HashInterface
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param HashInterface $token
     *
     * @return $this
     */
    public function setToken(HashInterface $token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getServerList()
    {
        return $this->serverList;
    }

    /**
     * @param string[] $serverList
     *
     * @return $this
     */
    public function setServerList(array $serverList)
    {
        $this->serverList = $serverList;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isFramework()
    {
        return $this->framework;
    }

    /**
     * @param boolean $framework
     *
     * @return $this
     */
    public function setFramework($framework)
    {
        $this->framework = $framework;
        return $this;
    }

    /**
     * @return ModuleConfig
     */
    public function getModuleConfig()
    {
        return $this->moduleConfig;
    }

    /**
     * @param ModuleConfig $moduleConfig
     *
     * @return $this
     */
    public function setModuleConfig($moduleConfig)
    {
        $this->moduleConfig = $moduleConfig;
        return $this;
    }

    /**
     * @return \string[]
     */
    public function getLogDirectories()
    {
        return $this->logDirectories;
    }

    /**
     * @param \string[] $logDirectories
     *
     * @return $this
     */
    public function setLogDirectories($logDirectories)
    {
        $this->logDirectories = $logDirectories;
        return $this;
    }


    /**
     * @param string $directory
     *
     * @return $this
     */
    public function addLogDirectory($directory)
    {
        if (is_dir($directory)) {
            $this->logDirectories[] = $directory;
        }

        return $this;
    }


    /**
     * @param string $ip
     *
     * @return $this
     */
    public function addServerIp($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
            $this->serverList[] = $ip;
        }

        return $this;
    }
}