<?php


namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Application
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Application implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Server
     */
    protected $server;

    /**
     * @var Log[]
     */
    protected $logs;

    /**
     * @param string $name
     * @param Server $server
     * @param array  $logs
     */
    public function __construct($name = null, Server $server = null, array $logs = [])
    {
        $this->name = $name;
        $this->server = $server;
        $this->logs = $logs;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * @param Server $server
     *
     * @return $this
     */
    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }

    /**
     * @return Log[]
     */
    public function getLogs()
    {
        return $this->logs;
    }

    /**
     * @param Log[] $logs
     *
     * @return $this
     */
    public function setLogs($logs)
    {
        $this->logs = $logs;
        return $this;
    }


    /**
     * @param Log $log
     *
     * @return $this
     */
    public function addLog(Log $log)
    {
        $this->logs[] = $log;
        return $this;
    }


    /**
     * @param bool $reverse
     *
     * @return $this
     */
    public function orderLogsByDate($reverse = false)
    {
        usort($this->logs, function (Log $a1, Log $a2) {
            return strtotime($a2->getDateTime()) - strtotime($a1->getDateTime());
        });

        if ($reverse) {
            $this->logs = array_reverse($this->logs);
        }
        return $this;
    }

    /**
     * @param int $length
     *
     * @return $this
     */
    public function limitLogs($length = null)
    {
        if (!is_numeric($length) || is_null($length)) {
            $length = 100;
        }
        $this->logs = array_splice($this->logs, 0, $length);
        return $this;
    }
}