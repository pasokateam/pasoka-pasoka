<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Log
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Log implements ObjectInterface
{
    use ObjectTrait;

    /**
     * @var LogDateTime
     */
    protected $dateTime;

    /**
     * @var string
     */
    protected $channel;

    /**
     * @var LogLevel
     */
    protected $level;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var string
     */
    protected $file;

    /**
     * @param LogDateTime $dateTime
     * @param string      $channel
     * @param LogLevel    $level
     * @param string      $message
     * @param string      $file
     */
    public function __construct(
        LogDateTime $dateTime = null,
        $channel = null,
        LogLevel $level = null,
        $message = null,
        $file = null)
    {
        $this->dateTime = $dateTime;
        $this->channel = $channel;
        $this->level = $level;
        $this->message = $message;
        $this->file = $file;
    }

    /**
     * @return LogDateTime
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }

    /**
     * @param LogDateTime $dateTime
     *
     * @return $this
     */
    public function setDateTime(LogDateTime $dateTime)
    {
        $this->dateTime = $dateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @param string $channel
     *
     * @return $this
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;
        return $this;
    }

    /**
     * @return LogLevel
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param LogLevel $level
     *
     * @return $this
     */
    public function setLevel(LogLevel $level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     *
     * @return $this
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }
}