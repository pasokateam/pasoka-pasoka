<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Cpu
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Cpu implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @var string
     */
    protected $modelName;

    /**
     * @param string $modelName
     */
    public function __construct($modelName = null)
    {
        $this->modelName = $modelName;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @param string $modelName
     *
     * @return $this
     */
    public function setModelName($modelName)
    {
        $this->modelName = $modelName;
        return $this;
    }
}