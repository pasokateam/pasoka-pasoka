<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class LogDateTime
 *
 * @package Pasoka\Component\Analytics\VO
 */
class LogDateTime implements ObjectInterface
{
    use ObjectTrait;

    /**
     * @var string
     */
    protected $date;

    /**
     * @var string
     */
    protected $timezone;

    /**
     * @param string $date
     * @param string $timezone
     */
    function __construct($date = null, $timezone = null)
    {
        $this->date = $date;
        $this->timezone = $timezone;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     *
     * @return $this
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString()
    {
        return (string)$this->date;
    }

    /**
     * @return \DateTime
     */
    public function toDateTime()
    {
        return new \DateTime($this->date);
    }
}