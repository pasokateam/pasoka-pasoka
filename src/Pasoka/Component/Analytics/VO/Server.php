<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Server
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Server implements ObjectInterface
{
    use ObjectTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $ip;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var float[]
     */
    protected $load;

    /**
     * @var string
     */
    protected $upTime;

    /**
     * @var Cpu[]
     */
    protected $cpu;

    /**
     * @var Memory
     */
    protected $memory;

    /**
     * @var Disk
     */
    protected $disk;

    /**
     * Constructor
     *
     * @param string    $name
     * @param string    $ip
     * @param float[]   $load
     * @param \DateTime $date
     * @param string    $upTime
     * @param Cpu[]     $cpu
     * @param Memory    $memory
     * @param Disk      $disk
     */
    public function __construct(
        $name = null,
        $ip = null,
        array $load = array(),
        \DateTime $date = null,
        $upTime = null,
        array $cpu = array(),
        Memory $memory = null,
        Disk $disk = null)
    {
        $this->date = $date;
        $this->upTime = $upTime;
        $this->cpu = $cpu;
        $this->memory = $memory;
        $this->disk = $disk;
        $this->ip = $ip;
        $this->load = $load;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return \float[]
     */
    public function getLoad()
    {
        return $this->load;
    }

    /**
     * @param \float[] $load
     *
     * @return $this
     */
    public function setLoad(array $load)
    {
        $this->load = $load;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpTime()
    {
        return $this->upTime;
    }

    /**
     * @param string $upTime
     *
     * @return $this
     */
    public function setUpTime($upTime)
    {
        $this->upTime = $upTime;
        return $this;
    }

    /**
     * @return Cpu[]
     */
    public function getCpu()
    {
        return $this->cpu;
    }

    /**
     * @param Cpu[] $cpu
     *
     * @return $this
     */
    public function setCpu(array $cpu)
    {
        $this->cpu = $cpu;
        return $this;
    }

    /**
     * @return Memory
     */
    public function getMemory()
    {
        return $this->memory;
    }

    /**
     * @param Memory $memory
     *
     * @return $this
     */
    public function setMemory(Memory $memory)
    {
        $this->memory = $memory;
        return $this;
    }

    /**
     * @return Disk
     */
    public function getDisk()
    {
        return $this->disk;
    }

    /**
     * @param Disk $disk
     *
     * @return $this
     */
    public function setDisk(Disk $disk)
    {
        $this->disk = $disk;
        return $this;
    }
}