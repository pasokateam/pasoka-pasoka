<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Disk
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Disk implements ObjectInterface
{

    use ObjectTrait;

    const UNIT_MB = "MB";
    const UNIT_GB = "GB";

    /**
     * @var float
     */
    protected $size;

    /**
     * @var float
     */
    protected $used;

    /**
     * @var float
     */
    protected $avail;

    /**
     * @var float
     */
    protected $use;

    /**
     * @var string
     */
    protected $unit;

    /**
     * Construct
     *
     * @param float  $size
     * @param float  $used
     * @param float  $avail
     * @param float  $use
     * @param string $unit
     */
    public function __construct($size = null, $used = null, $avail = null, $use = null, $unit = self::UNIT_GB)
    {
        $this->size = $size;
        $this->used = $used;
        $this->avail = $avail;
        $this->use = $use;
        $this->unit = $unit;
    }

    /**
     * @return float
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param float $size
     *
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return float
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @param float $used
     *
     * @return $this
     */
    public function setUsed($used)
    {
        $this->used = $used;
        return $this;
    }

    /**
     * @return float
     */
    public function getAvail()
    {
        return $this->avail;
    }

    /**
     * @param float $avail
     *
     * @return $this
     */
    public function setAvail($avail)
    {
        $this->avail = $avail;
        return $this;
    }

    /**
     * @return float
     */
    public function getUse()
    {
        return $this->use;
    }

    /**
     * @param float $use
     *
     * @return $this
     */
    public function setUse($use)
    {
        $this->use = $use;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

}