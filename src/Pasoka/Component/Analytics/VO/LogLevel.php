<?php

namespace Pasoka\Component\Analytics\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class LogLevel
 *
 * @package Pasoka\Component\Analytics\VO
 */
class LogLevel implements ObjectInterface
{
    use ObjectTrait;

    /**
     * @var number
     */
    protected $level;

    /**
     * @var string
     */
    protected $name;

    /**
     * @param number $level
     * @param string $name
     */
    public function __construct($level = null, $name = null)
    {
        $this->level = $level;
        $this->name = $name;
    }

    /**
     * @return number
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param number $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

}