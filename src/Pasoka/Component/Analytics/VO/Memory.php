<?php


namespace Pasoka\Component\Analytics\VO;


use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Class Memory
 *
 * free -m
 *
 * @package Pasoka\Component\Analytics\VO
 */
class Memory implements ObjectInterface
{

    use ObjectTrait;

    const UNIT_MB = "MB";
    const UNIT_GB = "GB";

    /**
     * (megabyte)
     *
     * @var float
     */
    protected $total;

    /**
     * (megabyte)
     *
     * @var float
     */
    protected $used;

    /**
     * (megabyte)
     *
     * @var float
     */
    protected $free;

    /**
     * @var string
     */
    protected $unit;

    /**
     * Construct
     *
     * @param float  $total
     * @param float  $used
     * @param float  $free
     * @param string $unit
     */
    public function __construct($total = null, $used = null, $free = null, $unit = self::UNIT_MB)
    {
        $this->total = $total;
        $this->used = $used;
        $this->free = $free;
        $this->unit = $unit;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->total = $total;
        return $this;
    }

    /**
     * @return float
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @param float $used
     *
     * @return $this
     */
    public function setUsed($used)
    {
        $this->used = $used;
        return $this;
    }

    /**
     * @return float
     */
    public function getFree()
    {
        return $this->free;
    }

    /**
     * @param float $free
     *
     * @return $this
     */
    public function setFree($free)
    {
        $this->free = $free;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     *
     * @return $this
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }


}