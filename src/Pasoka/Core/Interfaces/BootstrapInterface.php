<?php

namespace Pasoka\Core\Interfaces;

/**
 * Interface BootstrapInterface
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Core\Interfaces
 */
interface BootstrapInterface
{

    /**
     * @return string
     */
    public static function getPathRoot();

    /**
     * @return string
     */
    public static function getPathProject();

    /**
     * @return string
     */
    public static function getPathPublic();

    /**
     * @return string
     */
    public static function getPathSrc();

    /**
     * @return string
     */
    public static function getPathApp();

    /**
     * @return string
     */
    public static function getPathVendor();

    /**
     * @return string
     */
    public static function getUrlProject();
} 