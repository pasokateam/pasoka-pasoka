<?php

namespace Pasoka\Core\Console\Traits;

use Pasoka\Component\Console\Console;

/**
 * Class ConsoleTrait
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Console\Traits
 */
trait ConsoleTrait
{

    /**
     * @var bool
     */
    private $showMessages;

    /**
     * @var Console
     */
    private $console;


    /**
     * @param string $message
     * @param string $color
     */
    public function consoleWriteln($message = "", $color = "")
    {
        if ($this->showMessages) {
            $this->console->writeln(
                $message,
                $color
            );
        }
    }

    /**
     * @param string $message
     * @param string $color
     */
    public function consoleWrite($message = "", $color = "")
    {
        if ($this->showMessages) {
            $this->console->write(
                $message,
                $color
            );
        }
    }
} 