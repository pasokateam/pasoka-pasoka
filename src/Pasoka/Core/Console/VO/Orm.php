<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */

namespace Pasoka\Core\Console\VO;

use Pasoka\Component\Object\Interfaces\ObjectInterface;
use Pasoka\Component\Object\Traits\ObjectTrait;

/**
 * Classe ValueObject do ORM
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Console\VO
 * @namespace  Pasoka\Console\VO
 * @version    1.0.0
 */
class Orm implements ObjectInterface
{

    use ObjectTrait;

    /**
     * @access private
     * @var string
     */
    private $driver;

    /**
     * @access private
     * @var string
     */
    private $host;

    /**
     * @access private
     * @var int
     */
    private $port;

    /**
     * @access private
     * @var string
     */
    private $dbname;

    /**
     * @access private
     * @var string
     */
    private $user;

    /**
     * @access private
     * @var string
     */
    private $password;

    /**
     * @access private
     * @var string
     */
    private $pathEntities;

    /**
     * @param string $driver
     * @param string $host
     * @param int    $port
     * @param string $dbname
     * @param string $user
     * @param string $password
     * @param string $pathEntities
     */
    public function __construct($driver = null, $host = null, $port = 3306, $dbname = null, $user = null, $password = null,
                                $pathEntities = null)
    {
        $this->dbname = $dbname;
        $this->driver = $driver;
        $this->host = $host;
        $this->password = $password;
        $this->port = $port;
        $this->user = $user;
        $this->pathEntities = $pathEntities;
    }

    /**
     * @param string $pathEntities
     */
    public function setPathEntities($pathEntities)
    {
        $this->pathEntities = $pathEntities;
    }

    /**
     * @return string
     */
    public function getPathEntities()
    {
        return $this->pathEntities;
    }

    /**
     * @param string $dbname
     */
    public function setDbname($dbname)
    {
        $this->dbname = $dbname;
    }

    /**
     * @return string
     */
    public function getDbname()
    {
        return $this->dbname;
    }

    /**
     * @param string $driver
     */
    public function setDriver($driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return string
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

}