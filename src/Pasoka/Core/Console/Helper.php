<?php
/**
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Core\Console;


use Pasoka\Component\Console\Color;
use Pasoka\Component\Console\Console;
use Pasoka\Component\Manifest\Config\DatabaseConfig;
use Pasoka\Core\Console\Command\Cache;
use Pasoka\Core\Console\Command\Module;
use Pasoka\Core\Console\Command\Route;
use Pasoka\Core\Console\Traits\ConsoleTrait;
use Pasoka\Core\Dependence\PasokaLoader;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Helper class for development with framework through terminal.
 *
 * @author     Guilherme Santos - guilhermedossantos91@gmail.com
 * @package    Pasoka
 * @subpackage Console
 * @namespace  Pasoka\Console
 * @version    1.0.0
 */
final class Helper
{

    use ConsoleTrait;

    /**
     * Helper arguments
     *
     * @var string[]
     */
    private $arguments = array(
        '--help',
        '-help',
        '-h',
        '-?',
        'module',
        'route',
        'cache'
    );

    /**
     * @var BootstrapInterface
     */
    private $bootstrap;

    /**
     * @var PasokaLoader
     */
    private $loader;

    /**
     * Constructor
     *
     * @param Console                  $console
     * @param BootstrapInterface|mixed $bootstrap
     * @param PasokaLoader             $loader
     */
    public function __construct(Console $console, PasokaLoader $loader, $bootstrap)
    {
        $this->params = new \ArrayObject();
        $this->showMessages = true;
        $this->console = $console;
        $this->loader = $loader;
        $this->bootstrap = $bootstrap;
    }

    /**
     * Begin helper
     */
    public function run()
    {
        $numArgs = $this->console->getNumArgs();

        if ($numArgs > 1) {

            $firstArg = $this->console->getArg(1);
            $secondArg = $this->console->getArg(2);

            $command = explode(":", $firstArg);
            $param = null;

            if ($numArgs != 2 || in_array($command[0], $this->arguments)) {

                if (count($command) > 1) {
                    $command = trim($command[1]);
                    $command = strtolower($command);
                }

                if (isset($secondArg)) {
                    $param = $secondArg;
                }

                // ARGUMENT MODULE
                if (strpos($firstArg, 'module') !== false) {
                    $this->argModule($command);
                }

                // ARGUMENT CACHE
                if (strpos($firstArg, 'cache') !== false) {
                    $this->argCache($command);
                }

                // ARGUMENT ROUTE
                if (strpos($firstArg, 'route') !== false) {
                    $this->argRoute($command);
                }

                // ARGUMENT CONFIG
                if (strpos($firstArg, 'config') !== false) {
                    $this->argModule($command);
                }

                // ARGUMENT HELP
                if (strpos($firstArg, '--help') !== false ||
                    strpos($firstArg, '-h') !== false ||
                    strpos($firstArg, '-help') !== false ||
                    strpos($firstArg, '?') !== false
                ) {
                    $this->argWelcome();
                }
            } else {
                $this->console->writeln(
                    " - Error: Command '{$firstArg}' not found, use -help for more information\n\n",
                    Color::LIGHT_RED
                );
            }
            return;
        } else {
            $this->argWelcome();
        }
    }

    /**
     * Message invalid arguments
     */
    private function invalidArg()
    {
        $this->console->writeln(" - Error: Invalid parameters, use -help for more information\n\n", Color::LIGHT_RED);
    }


    /**
     * Actions of the route argument
     *
     * @param string $command
     */
    private function argRoute($command)
    {

        switch ($command) {

            /*
             * Command for generate routes
             */
            case 'generate':
                $this->console->clear();
                (new Route(
                    $this->console,
                    $this->bootstrap,
                    $this->loader
                ))->generate();
                break;
            default:
                $this->invalidArg();
        }
    }


    /**
     * Actions of the cache argument
     *
     * @param string $command
     */
    private function argCache($command)
    {

        $cache = new Cache($this->bootstrap, $this->loader);

        switch ($command) {

            /*
             * Command for cache clear
             */
            case 'clean':
                $cache->clean($this);
                break;
            default:
                $this->invalidArg();
        }
    }


    /**
     * Acoes do argumento module
     *
     * @param string $command
     */
    private function argModule($command)
    {

        $module = new Module(
            $this->bootstrap,
            $this->console,
            $this->loader
        );

        switch ($command) {

            /*
             * Command for module create
             */
            case 'create':

                $module->prepare();
                break;

            /*
             * Command for module remove
             */
            case 'remove':
                $module->remove();
                break;
            default:
                $this->invalidArg();
                break;
        }
    }


    /**
     * Welcome message
     */
    private function argWelcome()
    {
        $this->console->clear();
        $welcome = $this->loader
            ->getTemplate()
            ->setVar('color', Color::$name)
            ->fetch('helper/welcome.tpl');
        $this->console->display("\n{$welcome}");
    }


}