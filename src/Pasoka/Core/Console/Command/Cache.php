<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Core\Console\Command;

use Pasoka\Component\Console\Color;
use Pasoka\Component\Manifest\Manifest;
use Pasoka\Core\Console\Helper;
use Pasoka\Core\Dependence\PasokaLoader;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Class Cache
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Core\Console\Command
 */
final class Cache
{

    /**
     * @access private
     * @var string[]
     */
    private $paths;

    /**
     * @var BootstrapInterface|mixed
     */
    private $bootstrap;

    /**
     * @param BootstrapInterface|mixed $bootstrap
     * @param PasokaLoader             $loader
     */
    public function __construct($bootstrap, PasokaLoader $loader)
    {

        $manifest = $loader->getModule()->getView();
        $this->bootstrap = $bootstrap;
        $dir = new \RecursiveDirectoryIterator(
            $this->bootstrap->getPathSrc(),
            \FilesystemIterator::SKIP_DOTS
        );
        $it = new \RecursiveIteratorIterator(
            $dir,
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $it->setMaxDepth(0);

        $this->paths = [];
        $this->paths[] = $this->bootstrap->getPathRoot($manifest->getPathCache());
        $this->paths[] = $this->bootstrap->getPathRoot($manifest->getPathCompile());

        foreach ($it as $fileInfo) {

            if ($fileInfo instanceof \SplFileInfo && $fileInfo->isDir()) {

                $module = $this->bootstrap->getPathSrc($fileInfo->getFilename());
                $moduleManifest = new Manifest($module);
                $view = $moduleManifest->getModule()->getView();

                $cache = $module . DIRECTORY_SEPARATOR . $view->getPathCache();
                $compile = $module . DIRECTORY_SEPARATOR . $view->getPathCompile();;

                if (is_dir($cache)) {
                    $this->paths[] = $cache;
                }
                if (is_dir($compile)) {
                    $this->paths[] = $compile;
                }
            }
        }

    }


    /**
     * @param Helper $helper
     */
    public function clean(Helper $helper)
    {
        @system("clear");

        $helper->consoleWriteln("[cache]", Color::YELLOW);
        $helper->consoleWrite(" Clear cache of:\n", Color::GREEN);
        foreach ($this->paths as $path) {
            if (is_dir($path) && (
                    strpos($path, 'cache') !== false ||
                    strpos($path, 'compile') !== false)
            ) {
                if (@system("rm -Rf {$path}/*") !== false) {
                    $helper->consoleWriteln("    - clean: ", Color::YELLOW);
                    $helper->consoleWrite("$path", Color::GREEN);
                }
            }
        }

        $helper->consoleWriteln("\n Finish\n", Color::LIGHT_GREEN);
    }

} 