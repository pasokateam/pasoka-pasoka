<?php

namespace Pasoka\Console\Command;

use Pasoka\Component\Log\LogLevel;
use Pasoka\Component\Route\Interfaces\ControllerInterface;
use Pasoka\Core\Console\Helper;

/**
 * Class Cron
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Console\Command
 */
final class Cron
{

    private $helper;

    /**
     * @var \Pasoka\Component\Log\Logger
     */
    private $log;

    /**
     * Endereco do arquivo de cron do usuario
     *
     * @var string
     */
    private $cronFile;

    /**
     * @var \ArrayIterator
     */
    private $configFiles;

    /**
     * @var \ArrayIterator
     */
    private $errors;

    /**
     * @var \ArrayIterator
     */
    private $runnableClasses;

    /**
     * @var string
     */
    const INTERFACE_NAME = 'Pasoka\Component\Cron\Interfaces\RunnableInterface';

    /**
     * Construct
     *
     * @param Helper $helper
     * @param string $user
     */
    public function __construct(Helper $helper, $user)
    {
        $this->helper = $helper;
        $this->cronFile = "/var/spool/cron/{$user}";
        $this->runnableClasses = new \ArrayIterator();
        $this->configFiles = new \ArrayIterator();
        $this->errors = new \ArrayIterator();
        $this->log = PasokaLogger::load();

        $it = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(
                PATH_SRC,
                \FilesystemIterator::SKIP_DOTS
            ),
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $it->setMaxDepth(0);

        foreach ($it as $fileInfo) {
            if ($fileInfo->isDir()) {

                $jsonFile = PATH_SRC . "/{$fileInfo->getFilename()}/cron.json";

                foreach ($this->getCronConfig($jsonFile) as $runnable) {
                    $this->add($runnable);
                }
            }
        }
    }


    /**
     * @param \stdClass[] $configs
     * @return \stdClass[]
     */
    private function filterConfig(array $configs = [])
    {
        $valid = [];
        foreach ($configs as $runnable) {
            if ($runnable instanceof \stdClass) {

                $reflection = new \ReflectionObject($runnable);

                if ($reflection->hasProperty("class") && $reflection->hasProperty("config")) {

                    if ($runnable->config instanceof \stdClass) {

                        $reflection = new \ReflectionObject($runnable->config);

                        if ($reflection->hasProperty('mm') &&
                            $reflection->hasProperty('hh') &&
                            $reflection->hasProperty('dd') &&
                            $reflection->hasProperty('MM') &&
                            $reflection->hasProperty('ss')
                        ) {
                            array_push($valid, $runnable);
                        } else {
                            $message = "[Cron] Invalid params: {$runnable->class}";
                            $this->errors->append($message);
                            $this->log->writer(
                                $message,
                                LogLevel::WARNING
                            );
                        }

                    }
                }
            }
        }

        return $valid;
    }


    /**
     * @param string $jsonFile
     * @return \stdClass[]
     */
    private function getCronConfig($jsonFile)
    {
        if (file_exists($jsonFile)) {

            $cron = json_decode(file_get_contents($jsonFile));

            if ($cron) {

                $this->configFiles->append($jsonFile);
                return $this->filterConfig($cron);
            } else {
                $message = "Invalid JSON: {$jsonFile}";
                $this->errors->append($message);
                $this->log->writer(
                    $message,
                    LogLevel::ERROR
                );
            }
        }
        return [];
    }


    /**
     * Verifica se a classe é valida
     *
     * @param string $class
     * @return ControllerInterface|null
     */
    public function isRunnable($class)
    {
        if (class_exists($class)) {
            $reflection = new \ReflectionClass($class);
            if ($reflection->implementsInterface(self::INTERFACE_NAME)) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param \stdClass $runnable
     * @return $this
     */
    public function add(\stdClass $runnable)
    {
        $runnable->class = str_replace(".", "\\", $runnable->class);
        if ($this->isRunnable($runnable->class)) {
            $this->runnableClasses->append($runnable);
        }
        return $this;
    }


    /**
     *
     */
    public function generate()
    {

        if (!file_exists($this->cronFile)) {
            $this->helper->warningMessage("\n\n[W] File not found, creating a new file.\n");
            $this->log->writer(
                "[Cron] Cron file not found: {$this->cronFile}",
                LogLevel::WARNING
            );

        }
        if (!is_writable($this->cronFile)) {
            $this->log->writer(
                "[Cron] Permission denied in: {$this->cronFile}",
                LogLevel::ERROR
            );

            $this->helper->errorMessage("\n\n[w] Arquivo não encontrado, criando arquiv");
        }


        while ($this->runnableClasses->valid()) {


            $this->runnableClasses->next();
        }

    }
} 