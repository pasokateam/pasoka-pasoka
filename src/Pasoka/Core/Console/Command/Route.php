<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Core\Console\Command;

use Pasoka\Component\Console\Color;
use Pasoka\Component\Console\Console;
use Pasoka\Component\Route\Router;
use Pasoka\Core\Console\Traits\ConsoleTrait;
use Pasoka\Core\Dependence\PasokaLoader;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Class Route
 *
 * @package Pasoka\Core\Console\Command
 */
final class Route
{

    use ConsoleTrait;

    /**
     * Rotas
     *
     * @var \ArrayObject
     */
    private $routes;

    /**
     * Logger
     *
     * @var \Pasoka\Component\Log\Logger
     */
    private $log;

    /**
     * @var BootstrapInterface|mixed
     */
    private $bootstrap;

    /**
     * @var PasokaLoader
     */
    private $loader;

    /**
     * @var string
     */
    private $fileHTACCESS;

    /**
     * @var string[]
     */
    private $listRoutes = [];

    /**
     * @var \stdClass
     */
    private $values;

    /**
     *
     * @param Console                  $console
     * @param BootstrapInterface|mixed $bootstrap
     * @param PasokaLoader             $loader
     * @param bool                     $showMessages
     */
    public function __construct(Console $console, $bootstrap, PasokaLoader $loader, $showMessages = true)
    {

        $this->showMessages = $showMessages;
        $this->console = $console;
        $this->bootstrap = $bootstrap;
        $this->fileHTACCESS = $this->bootstrap->getPathPublic('.htaccess');
        $this->routes = new \ArrayObject();
        $this->loader = $loader;
        $this->log = $this->loader->getLogger();
        $this->values = $this->loader->getValues()->getGroup("route-en");

        $this->consoleWriteln(
            $this->values->groupName,
            Color::YELLOW
        );
        $this->consoleWrite(
            $this->values->creating,
            Color::GREEN
        );

        $this->listRoutes = $this->getJsonFiles();

        foreach ($this->listRoutes as $jsonFile) {

            $routes = $this->getJsonObjects($jsonFile);

            foreach ($routes as $route) {

                if ($route instanceof \stdClass) {

                    if (!isset($route->params)) {
                        $route->params = [];
                    }

                    while (strpos($route->alias, '/') === 0 || strpos($route->alias, '\\') === 0) {
                        $route->alias = substr($route->alias, 1, strlen($route->alias));
                        $route->alias = trim($route->alias);
                    }
                    $this->add($route->alias, $route->class, $route->method, $route->params);
                }
            }
        }


        if (!is_dir($this->bootstrap->getPathPublic())) {
            $this->log->error($this->values->errorPublicPath);
            $this->consoleWriteln(
                $this->values->errorPublicPath,
                Color::RED
            );
        }
        if (!file_exists($this->fileHTACCESS)) {
            $this->log->error($this->values->errorHtAccess);
            $this->consoleWriteln(
                $this->values->errorHtAccess,
                Color::RED
            );
        }
        if (!is_writable($this->fileHTACCESS)) {
            $this->log->error($this->values->errorHtAccessPermission);
            $this->consoleWriteln(
                $this->values->errorHtAccessPermission,
                Color::RED
            );
        }

    }


    /**
     * @param string $jsonFile
     * @return \stdClass|null
     */
    private function getJsonObjects($jsonFile)
    {
        if (file_exists($jsonFile)) {

            $json = json_decode(file_get_contents($jsonFile));

            if ($json) {
                return $json;
            } else {
                $message = $this->values->errorInvalidJson . $jsonFile;
                $this->log->error($message);
                $this->consoleWriteln(
                    $message,
                    Color::LIGHT_RED
                );
            }
        } else {
            $message = $this->values->errorFileNotFound . $jsonFile;
            $this->consoleWriteln(
                $message,
                Color::LIGHT_RED
            );
            $this->log->error($message);

        }

        return null;
    }

    /**
     * @return string[]
     */
    private function getJsonFiles()
    {

        $files = [];
        $dir = new \RecursiveDirectoryIterator(
            $this->bootstrap->getPathSrc(),
            \FilesystemIterator::SKIP_DOTS
        );
        $it = new \RecursiveIteratorIterator(
            $dir,
            \RecursiveIteratorIterator::SELF_FIRST
        );
        $it->setMaxDepth(0);

        foreach ($it as $fileInfo) {
            if ($fileInfo instanceof \SplFileInfo && $fileInfo->isDir()) {

                $jsonFile = $this->bootstrap->getPathSrc($fileInfo->getFilename(), "routes.json");
                $json = $this->getJsonObjects($jsonFile);

                if (!is_null($json)) {
                    $pathRoutes = str_replace($this->bootstrap->getPathSrc(), "", $jsonFile);
                    $this->consoleWriteln(" - Load: {$pathRoutes} \n", Color::GREEN);
                    array_push($files, $jsonFile);
                }
            }
        }

        return $files;
    }


    /**
     * Obtem lista de arquivos de rotas
     *
     * @return \string[]
     */
    public function getRoutes()
    {
        return $this->listRoutes;
    }


    /**
     * Metodo para adicionar novos caminhos/url de controller.
     *
     * @param string   $alias
     * @param string   $class
     * @param string   $method
     * @param string[] $params
     * @return $this
     */
    private function add($alias, $class, $method, array $params = [])
    {
        $class = str_replace(".", "\\", $class);

        if (!Router::isRoutable($class, $method)) {
            $message = "    - Error: invalid route, {$alias} - {$class}::{$method}";
            $this->consoleWriteln($message, Color::LIGHT_RED);
            $this->log->error($message);
            return $this;
        }

        if ($this->routes->count() > 0) {
            for ($i = 0; $i < $this->routes->count(); $i++) {

                $obj = $this->routes->offsetGet($i);

                if (!(
                    $obj->alias == $alias &&
                    $obj->class == $class &&
                    $obj->method == $method &&
                    $obj->params == $params
                )
                ) {
                    $this->append($alias, $class, $method, $params);
                    break;
                } else {
                    $message = "    - Warning: duplicate route, {$alias} - {$class}::{$method}";
                    $this->consoleWriteln($message, Color::YELLOW);
                }
            }
        } else {
            $this->append($alias, $class, $method, $params);
        }

        return $this;
    }

    /**
     * Metodo privado para adicionar rota na lista da instancia.
     *
     * @param string   $alias
     * @param string   $class
     * @param string   $method
     * @param string[] $params
     */
    private function append($alias, $class, $method, array $params = [])
    {
        $route = new \stdClass();
        $route->alias = $alias;
        $route->class = $class;
        $route->method = $method;
        $route->params = $params;
        $this->routes->append($route);
        $this->consoleWriteln(
            "    - Creating: {$alias}",
            Color::GREEN
        );
        $this->consoleWriteln(
            "      {$class}::{$method}",
            Color::YELLOW
        );


    }

    /**
     * Escreve arquivo de rotas.
     *
     * @param string $htAccess
     */
    private function write($htAccess = "")
    {
        $ht = new \SplFileObject($this->fileHTACCESS, "w+");
        $ht->fwrite($htAccess);
        $ht = null;

        if (file_exists($this->fileHTACCESS)) {
            $this->consoleWriteln(
                "\n Generated successfully\n\n\n",
                Color::LIGHT_GREEN
            );
        }
    }

    /**
     * Retorna ArrayObject com estrutura default do htaccess.
     *
     * @return \ArrayObject
     */
    private function getHtAccess()
    {
        $htAccess = new \ArrayObject();
        //$htAccess->append("<IfModule mod_rewrite.c>");
        $htAccess->append("RewriteEngine On");
        $htAccess->append("Options +FollowSymLinks");
        //$htAccess->append("RewriteCond %{SCRIPT_FILENAME} !-f");
        //$htAccess->append("RewriteCond %{SCRIPT_FILENAME} !-d");
        $htAccess->append("RewriteCond %{REQUEST_FILENAME} !-f");
        //$htAccess->append("RewriteCond %{REQUEST_FILENAME} !-d");
        //$htAccess->append("RewriteCond %{REQUEST_URI}::$1 ^(/.+)/(.*)::\2$");
        //$htAccess->append("RewriteRule ^(.*) - [E=BASE:%1]");
        //$htAccess->append("RewriteCond %{ENV:REDIRECT_STATUS} ^$");
        $htAccess->append("ErrorDocument 404 /index.php");
        //$htAccess->append("RedirectMatch 302 ^/$ /index.php/");
        $htAccess->append("RewriteCond $1 !\.(js|ico|gif|jpg|png|css|html|swf|mp3|wav|txt)$");
        return $htAccess;
    }

    /**
     * Generate routes
     */
    public function generate()
    {
        $htAccess = $this->getHtAccess();
        $total = $this->routes->count();

        for ($i = 0; $i < $total; $i++) {
            $route = $this->routes->offsetGet($i);
            $route->class = str_replace("\\", ".", $route->class);
            $paramsUrl = "";

            //%{ENV:BASE}
            $params = $route->params;
            if (count($params)) {
                for ($c = 0, $m = count($params); $c < $m; $c++) {
                    $var = $c + 1;
                    $param = $params[$c];
                    $paramsUrl .= "&{$param}=\${$var}";
                }
            }

            $rewrite = "RewriteRule ^{$route->alias}$ index.php?class={$route->class}&method={$route->method}{$paramsUrl} [QSA,L]";
            $htAccess->append($rewrite);
        }

        //$htAccess->append("</IfModule>");
        //$htAccess->append("RewriteRule ^(.+)$ index.php");
        $this->write(implode("\n", $htAccess->getArrayCopy()));
    }

} 