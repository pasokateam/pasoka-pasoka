<?php
/*!
 * For the full copyright and license information, please view the
 * license file that was distributed with this source code.
 */
namespace Pasoka\Core\Console\Command;

use Pasoka\Component\Console\Color;
use Pasoka\Component\Console\Console;
use Pasoka\Component\Json\Json;
use Pasoka\Component\Manifest\Config\DatabaseConfig;
use Pasoka\Component\Manifest\Config\LogConfig;
use Pasoka\Component\Manifest\Config\ModuleConfig;
use Pasoka\Component\Manifest\Config\ValuesConfig;
use Pasoka\Component\Manifest\Config\ViewConfig;
use Pasoka\Component\Manifest\Manifest;
use Pasoka\Component\View\Template;
use Pasoka\Core\Dependence\PasokaLoader;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Class Module
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Core\Console\Command
 */
final class Module
{
    /**
     * @var null|string
     */
    private $name;

    /**
     * @var DatabaseConfig
     */
    private $database;

    /**
     * @var mixed|BootstrapInterface
     */
    private $bootstrap;

    /**
     * @var Console
     */
    private $console;
    /**
     * @var \Pasoka\Core\Dependence\PasokaLoader
     */
    private $loader;

    /**
     * @param BootstrapInterface|mixed             $bootstrap
     * @param \Pasoka\Component\Console\Console    $console
     * @param \Pasoka\Core\Dependence\PasokaLoader $loader
     */
    public function __construct($bootstrap, Console $console, PasokaLoader $loader)
    {
        $this->console = $console;
        $this->bootstrap = $bootstrap;
        $this->loader = $loader;
    }

    /**
     * @param string $name
     * @return mixed|string
     */
    public function formatName($name)
    {
        $name = preg_replace("/[^a-zA-Z\s]/", "",
            strtr($name, "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ ",
                "aaaaeeiooouucAAAAEEIOOOUUC_"));
        $name = ucwords(strtolower($name));
        $name = str_replace(" ", "", $name);

        return $name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $this->formatName($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @param DatabaseConfig $database
     */
    public function setDatabase(DatabaseConfig $database)
    {
        $this->database = $database;
    }


    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * Gera fix.sh
     *
     * @param Template $tpl
     */
    public function makeFix(Template $tpl)
    {
        $tpl->setVar('date', (new \DateTime('NOW'))
            ->format("Y-m-d H:i:s"));

        $dir = new \RecursiveDirectoryIterator(
            $this->bootstrap->getPathSrc(),
            \FilesystemIterator::SKIP_DOTS
        );
        $it = new \RecursiveIteratorIterator(
            $dir,
            \RecursiveIteratorIterator::SELF_FIRST
        );

        $it->setMaxDepth(0);
        $listModules = [];

        foreach ($it as $fileInto) {
            if ($fileInto->isDir()) {
                $listModules[] = (new Manifest(
                    $this->bootstrap->getPathSrc(
                        $fileInto->getFilename()
                    )
                ))->getModule();
            }
        }

        // content
        $content = $tpl->setVar('listModules', $listModules)
            ->setVar('total', count($listModules))
            ->fetch('module/shFix.tpl');

        // write
        $file = new \SplFileObject(
            $this->bootstrap->getPathRoot("fix.sh"),
            "w+"
        );
        chmod($this->bootstrap->getPathRoot("fix.sh"), 0777);
        $file->fwrite($content);
    }


    public function prepare()
    {
        $database = new DatabaseConfig();
        $name = null;

        /*
         * show message enter name
         */
        function messageName(Console $console)
        {
            $console->writeln(
                " - Enter the module name: ",
                Color::LIGHT_BLUE
            );
        }

        /*
         * Show module name
         */
        function messageModuleName(Console $console, $moduleName)
        {
            $console->writeln(
                "[{$moduleName}]",
                Color::YELLOW
            );
        }

        $this->console->clear();
        $this->console->writeln(
            "[module:create]",
            Color::YELLOW
        );
        $this->console->write(
            " Creating new module",
            Color::GREEN
        );
        messageName($this->console);

        while ($name == null || strlen($name) == 0) {

            $name = $this->formatName($this->console->readLine());

            if ($name == null || strlen($this->formatName($name)) == 0) {

                $this->console->writeln(
                    " - Error: please enter valid name, no numbers, spaces or special characters: ",
                    Color::LIGHT_RED
                );

            } else if (is_dir($this->bootstrap->getPathSrc($name))) {
                $name = null;
                $this->console->writeln(
                    " - Error: That name already exists. Please choose another name.",
                    Color::LIGHT_RED
                );
                $this->console->writeln(
                    " - Error: please enter valid name, no numbers, spaces or special characters: ",
                    Color::LIGHT_RED
                );

            }
        }
        $this->setName($name);


        // DRIVER
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Driver connection? (pdo_mysql): ", Color::LIGHT_BLUE);
        $database->setDriver($this->console->readLine());
        if ($database->getDriver() == null || trim($database->getDriver()) == "") {
            $database->setDriver('pdo_mysql');
        }

        // HOST
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Database server address? (localhost): ", Color::LIGHT_BLUE);
        $database->setHost($this->console->readLine());
        if ($database->getHost() == null || trim($database->getHost()) == "") {
            $database->setHost('localhost');
        }

        // PORT
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Database port? (3306): ", Color::LIGHT_BLUE);
        $database->setPort($this->console->readLine());
        if ($database->getPort() == null || trim($database->getPort()) == "") {
            $database->setPort(3306);
        }

        // DBNAME
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Database name? (" . strtolower($this->getName()) . "): ", Color::LIGHT_BLUE);
        $database->setName(strtolower($this->console->readLine()));
        if ($database->getName() == null || trim($database->getName()) == "") {
            $database->setName(strtolower($this->getName()));
        }

        // USER
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Database user?  (root): ", Color::LIGHT_BLUE);
        $database->setUser($this->console->readLine());
        if ($database->getUser() == null || trim($database->getUser()) == "") {
            $database->setUser('root');
        }

        // PASSWORD
        messageModuleName($this->console, $this->getName());
        $this->console->write(" - [ORM] Database password? : ", Color::LIGHT_BLUE);
        $database->setPassword($this->console->readLine());
        if ($database->getPassword() == null) {
            $database->setPassword('');
        }

        $database->setDevMode(true);
        $database->setPathEntities("Model/Entity");
        $this->setDatabase($database);

        if ($this->create()) {
            (new Route(
                $this->console,
                $this->bootstrap,
                $this->loader,
                false
            ))->generate();
            $this->console->writeln(" Created successfully\n\n", Color::LIGHT_GREEN);
        } else {
            $this->console->writeln(" - Error; undefined", Color::LIGHT_RED);
        }
    }

    /**
     * Module create
     */
    public function create()
    {
        // caminho dos diretorios principais
        $dirModule = $this->bootstrap->getPathSrc($this->name);

        // caminho da classe loader
        $dirDep = "{$dirModule}/Dependence";

        // caminho das controllers
        $dirController = "{$dirModule}/Controller";

        // caminho da camada model
        $dirModel = "{$dirModule}/Model";

        // caminho das entidades do ORM
        $dirOrmEntity = "{$dirModel}/Entity";
        $dirOrmRepository = "{$dirModel}/Repository";

        $dirResources = "{$dirModule}/resources";

        // caminho da view
        $dirView = "{$dirResources}/view";

        // caminho da values
        $dirValues = "{$dirResources}/values";

        // caminho da values
        $dirLogs = "{$dirResources}/logs";

        // caminho dos caches da view
        $dirViewCache = "{$dirView}/cache";

        // caminho dos arquivos compilados da view
        $dirViewCompile = "{$dirView}/compile";

        // caminho dos templates da view
        $dirViewTemplates = "{$dirView}/templates";

        //
        $file = null;
        $date = (new \DateTime('NOW'))->format("Y-m-d H:i:s");
        $loader = new PasokaLoader($this->bootstrap);
        $tpl = $loader
            ->getTemplate()
            ->setCache(false, -1)
            ->setVar('date', $date)
            ->setVar('module', $this->name);

        if (!is_dir($dirController)) {
            $mask = umask(0);

            if (mkdir($dirModule, 0775)) {

                mkdir($dirDep, 0775);
                mkdir($dirController, 0775);
                mkdir($dirModel, 0775);
                mkdir($dirOrmEntity, 0775);
                mkdir($dirOrmRepository, 0775);

                // gera classe controller test
                $content = $tpl->fetch('module/classController.tpl');
                $file = new \SplFileObject("{$dirController}/IndexController.php", "w+");
                $file->fwrite($content);

                // gera classe loader
                $content = $tpl->fetch('module/classLoader.tpl');
                $file = new \SplFileObject("{$dirDep}/{$this->name}Loader.php", "w+");
                $file->fwrite($content);

                // gera rotas do modulo
                $routeTest = new \stdClass();
                $routeTest->alias = strtolower($this->name);
                $routeTest->class = "{$this->name}.Controller.IndexController";
                $routeTest->method = 'recipe';
                $routeTest->params = [];

                $routeTestLang = new \stdClass();
                $routeTestLang->alias = strtolower($this->name) . "/([a-z]*)/?";
                $routeTestLang->class = "{$this->name}.Controller.IndexController";
                $routeTestLang->method = 'recipe';
                $routeTestLang->params = ["lang"];

                $json = new Json(Json_encode([$routeTest, $routeTestLang], JSON_PRETTY_PRINT));
                $json->save($dirModule . "/routes.json");

                /**
                 * MANIFEST
                 */

                // view
                $view = new ViewConfig();
                $view->setCache(false);
                $view->setCompile(true);
                $view->setPathCache("resources/view/cache");
                $view->setPathCompile("resources/view/compile");
                $view->setPathTemplate("resources/view/templates");

                // log
                $log = new LogConfig();
                $log->setName($this->name);
                $log->setPath("resources/logs");

                // values
                $values = new ValuesConfig();
                $values->setPath("resources/values");

                // module config
                $moduleConfig = new ModuleConfig();
                $moduleConfig->setName($this->name);
                $moduleConfig->setView($view);
                $moduleConfig->setLog($log);
                $moduleConfig->setValues($values);
                $moduleConfig->setDatabase($this->database);
                $moduleConfig->setMetadata(new \stdClass());

                $json = new Json(json_encode($moduleConfig->valueOf(), JSON_PRETTY_PRINT));
                $json->save($dirModule . "/manifest.json");


                if (mkdir($dirResources, 0775)) {

                    if (mkdir($dirValues, 0775)) {

                        $content = $tpl->fetch('module/values/en.json');
                        $file = new \SplFileObject("{$dirValues}/en.json", "w+");
                        $file->fwrite($content);

                        $content = $tpl->fetch('module/values/pt.json');
                        $file = new \SplFileObject("{$dirValues}/pt.json", "w+");
                        $file->fwrite($content);
                    }
                    mkdir($dirLogs);

                    if (mkdir($dirView, 0775)) {

                        mkdir($dirViewCompile, 0775);
                        mkdir($dirViewCache, 0775);
                        mkdir($dirViewTemplates, 0775);

                        // gera layout teste
                        $content = $tpl->fetch('module/htmlLayout.tpl');
                        $content = str_replace("[[", "|", $content);
                        $content = str_replace("]]", "|", $content);
                        $file = new \SplFileObject("{$dirViewTemplates}/layout.tpl", "w+");
                        $file->fwrite($content);

                        // gera index teste
                        $content = $tpl->fetch('module/htmlIndex.tpl');
                        $content = str_replace("[[", "|", $content);
                        $content = str_replace("]]", "|", $content);
                        $file = new \SplFileObject("{$dirViewTemplates}/index.tpl", "w+");
                        $file->fwrite($content);
                    }
                }


                //gera fix
                $this->makeFix($tpl);
            }
            umask($mask);
            $file = null;
            return true;
        }
        return false;
    }

    /**
     *
     */
    public function remove()
    {
        $name = null;
        $remove = null;

        $this->console->clear();
        $this->console->writeln("[module:remove] Remove modules", Color::YELLOW);
        $this->console->writeln(" - Enter module name: ", Color::LIGHT_BLUE);

        while ($name == null || strlen($name) == 0) {
            $name = $this->formatName($this->console->readLine());

            if ($name == null || strlen($this->formatName($name)) == 0) {

                $this->console->writeln(" - Error: module {$name} not found", Color::LIGHT_RED);
                $this->console->writeln(" - Enter module name: ", Color::BLUE);

            } else if (is_dir($this->bootstrap->getPathSrc($name))) {

                while ($remove == null || ($remove != 'Y' && $remove != 'N')) {

                    $this->console->writeln(" - Delete module {$name}? (Y/N) ", Color::LIGHT_RED);
                    $remove = strtoupper($this->console->readLine());

                    if ($remove == 'Y') {

                        if ($this->console->command("rm -Rf " . $this->bootstrap->getPathSrc($name)) !== false) {
                            (new Route(
                                $this->console,
                                $this->bootstrap,
                                $this->loader,
                                false
                            ))->generate();

                            //
                            $this->makeFix($this->loader->getTemplate());
                            $this->console->writeln(" Deleted successfully\n", Color::LIGHT_GREEN);
                        }
                    }
                }
            } else {
                $this->console->writeln(" - Error: module {$name} not found", Color::LIGHT_RED);
                $this->console->writeln(" - Enter module name: ", Color::LIGHT_BLUE);

                $name = null;
            }
        }
    }
}