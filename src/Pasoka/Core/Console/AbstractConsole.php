<?php

namespace Pasoka\Core\Console;

/**
 * Class AbstractConsole
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Pasoka\Console
 */
abstract class AbstractConsole
{

    /**
     * Cores para o console
     *
     * \e[CODEm
     *
     * Black        0;30
     * Dark Gray    1;30
     * Blue         0;34
     * Light Blue   1;34
     * Green        0;32
     * Light Green  1;32
     * Cyan         0;36
     * Light Cyan   1;36
     * Red          0;31
     * Light Red    1;31
     * Purple       0;35
     * Light Purple 1;35
     * Brown/Orange 0;33
     * Yellow       1;33
     * Light Gray   0;37
     * White        1;37
     * SEM COR:     0
     *
     * @access private
     * @var string[]
     */
    public $color = [
        'white'     => "\e[1;37m",
        'red'       => "\e[0;31m",
        'green'     => "\e[0;32m",
        'yellow'    => "\e[1;33m",
        'blue'      => "\e[0;34m",
        'lightblue' => "\e[1;34m",
        'magenta'   => "\e[33;35m",
        'gray'      => "\e[33;30m",
        'cyan'      => "\e[0;36m"
    ];

    /**
     * @access private
     * @var string[]
     */
    public $fmt = [
        'bold' => "\033[1m",
        'end'  => "\033[0m"
    ];


    /**
     * Total de argumentos
     *
     * @access private
     * @var int
     */
    public $argc;

    /**
     * Lista de argumentos lidos
     *
     * @access private
     * @var string[]
     */
    public $argv;

    /**
     * @access private
     */
    public $stdout;

    /**
     * Lista de linhas lidas pelo console
     *
     * @access private
     * @var \ArrayObject
     */
    protected $params;

    /**
     * Construtor
     *
     * @access public
     * @param $argc
     * @param $argv
     */
    public function __construct($argc, $argv)
    {
        $this->argc = $argc;
        $this->argv = $argv;
        $this->stdout = fopen("php://stdout", "w");
    }


    /**
     * Exibe uma mensagem com cor e negrito no console
     *
     * @access private
     * @param string $message
     * @param string $color
     */
    public function message($message, $color = "")
    {
        $this->write("{$color}");
        $this->write("{$this->fmt['bold']}");
        $this->write("{$message}");
        $this->write("{$this->fmt['end']}");
    }

    /**
     * Escreve mensagem no console
     *
     * @access private
     * @param string $inf
     */
    public function write($inf)
    {
        fwrite($this->stdout, $inf);
    }

    /**
     * Exibe mensagem de informacao no console
     *
     * @access private
     * @param string $message
     */
    public function infoMessage($message)
    {
        $this->message($message, $this->color['blue']);
    }

    /**
     * Le uma linha do console
     *
     * {|literal|}
     * if { }
     * {|/literal|}
     *
     * @access private
     * @param null|string $param
     * @return string
     */
    public function readLine($param = null)
    {
        $obj = new \stdClass();
        $obj->param = $param;
        $obj->value = trim(fgets(STDIN));

        if ($param !== null) {
            //scanf(STDIN, "%d\n", $number); // carrega number a partir do STDIN
            $iterator = $this->params->getIterator();
            while ($iterator->valid()) {
                if (serialize($obj) == serialize($iterator->current())) {
                    $this->params->offsetUnset($iterator->key());
                }
                $iterator->next();
            }
            $this->params->append($obj);
        }
        return $obj->value;
    }

    /**
     * Exibe mensagem de erro no console
     *
     * @access private
     * @param string $message
     */
    public function errorMessage($message)
    {
        $this->message($message, $this->color['red']);
    }

    /**
     * Exibe mensagem de aviso no console
     *
     * @access private
     * @param string $message
     */
    public function warningMessage($message)
    {
        $this->message($message, $this->color['yellow']);
    }


    /**
     * Procura uma linha lida pelo console
     *
     * @access private
     * @param $param
     * @return null|string
     */
    public function findRead($param)
    {
        $iterator = $this->params->getIterator();
        while ($iterator->valid()) {
            if ($iterator->current()->param == $param) {
                return $iterator->current()->value;
            }
            $iterator->next();
        }
        return null;
    }

    public function println($message, $color)
    {

    }


    /**
     *
     */
    public function __destruct()
    {
        fclose($this->stdout);
    }
} 