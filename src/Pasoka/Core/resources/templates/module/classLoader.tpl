<?php

namespace {|$module|}\Dependence;

use Doctrine\ORM\EntityManager;
use Pasoka\Component\Connect\Connection;
use Pasoka\Component\Log\Logger;
use Pasoka\Component\Manifest\Config\ModuleConfig;
use Pasoka\Component\Manifest\Manifest;
use Pasoka\Component\Values\Values;
use Pasoka\Component\View\Template;

final class {|$module|}Loader
{
    /**
     * @var {|$module|}Loader
     */
    private static $instance;

    /**
     * @var Manifest
     */
    private $manifest;

    /**
     * @var ModuleConfig
     */
    private $module;

    /**
     * Constructor
     */
    private function __construct()
    {
        $this->manifest = new Manifest(__DIR__ . "/../");
        $this->module = $this->manifest->getModule();
    }

    /**
    * @return {|$module|}Loader
    */
    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @return ModuleConfig
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
    * @return EntityManager
    */
    public function getEntityManager()
    {
        $database = $this->module->getDatabase();
        $dbParams = [
            'driver'   => $database->getDriver(),
            'user'     => $database->getUser(),
            'host'     => $database->getHost(),
            'port'     => $database->getPort(),
            'password' => $database->getPassword(),
            'dbname'   => $database->getName(),
            'path'     => $database->getName()
        ];

        return Connection::create([
            \Bootstrap::getPathSrc(
                $this->module->getName(),
                $database->getPathEntities()
            )
        ], $dbParams, $database->isDevMode());
    }


    /**
     * @return Template
     */
    public function getTemplate()
    {
        $view = $this->module->getView();
        return Template::getInstance(
            \Bootstrap::getPathSrc($this->module->getName(), $view->getPathTemplate()),
            \Bootstrap::getPathSrc($this->module->getName(), $view->getPathCompile()),
            \Bootstrap::getPathSrc($this->module->getName(), $view->getPathCache()),
            $view->isCache(),
            $view->isCompile()
        );
    }


    /**
     * @return Logger
     */
    public function getLogger()
    {
        $log = $this->module->getLog();
        $file = (new \DateTime("NOW"))->format("Y-m-d") . ".log";
        $file = \Bootstrap::getPathSrc($this->module->getName(), $log->getPath(), $file);
        if (!file_exists($file)) {
            $fObj = new \SplFileObject($file, "w+");
            $fObj->fwrite("");
            $fObj = null;
            chmod($file, 0777);
        }
        return new Logger(
            $log->getName(),
            $file
        );
    }


    /**
     * @return Values
     */
    public function getValues()
    {
        $values = $this->module->getValues();
        return Values::getInstance(
            \Bootstrap::getPathSrc(
                $this->module->getName(),
                $values->getPath()
            )
        );
    }
}