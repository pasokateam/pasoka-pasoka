<?php
namespace {|$module|}\View;

use {|$module|}\Config\{|$module|}ViewConfig;
use Pasoka\Component\View\Template;

/**
 * [Gerado via helper - {|$date|}]
 * Classe que carrega o Smarty, usada para exibicao e cache
 * da camada de view.
 *
 * @author Pasoka Helper
 * @package {|$module|}
 * @subpackage View
 * @namespace {|$module|}\View
 * @version 1.0.0
 */
class {|$module|}Template extends Template
{

    /**
     * Retorna instancia da classe de Template (que faz singleton do Smarty)
     *
     * @access public
     * @return \Pasoka\Component\View\Template
     */
    public static function load()
    {
        return parent::getInstance(
            PATH_ROOT . {|$module|}ViewConfig::PATH_TEMPLATE,
            PATH_ROOT . {|$module|}ViewConfig::PATH_COMPILE,
            PATH_ROOT . {|$module|}ViewConfig::PATH_CACHE,
            PATH_ROOT . {|$module|}ViewConfig::PATH_CONFIG,
            {|$module|}ViewConfig::CACHING
        );
    }
}
