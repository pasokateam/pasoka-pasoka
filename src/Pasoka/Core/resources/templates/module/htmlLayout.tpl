<!DOCTYPE html>
<html>
<head>
    <title>{[[block name=title]]}{[[/block]]}</title>
    <link rel="shortcut icon" href="/favicon.png"/>
    <style>
        * {
            font-family: "Droid Sans", "Arial", "Helvetica", sans-serif;
            color: #404040;
        }

        a {
            text-decoration: none;
            color: #0a2b1d;
            font-weight: bold;
        }

        .text-right {
            text-align: right;
        }

        body {
            background: #3998dc url("/images/pasoka/cloud.jpg") repeat-x left top;
        }

        ol li, ul li {
            margin: 10px;
        }

        .container {
            width: 900px;
            margin: 0 auto;
        }

        .recipe {
            background: #ffffff;
            padding: 1px 30px 30px;
            width: 600px;
            position: relative;
            left: 120px;
            bottom: -40px;
            z-index: 0;
            opacity: 0.9;
        }

        .peanut {
            z-index: 1;
            position: relative;
            top: -30px;
        }

        .radius {
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            border-radius: 4px;
        }

        .shadow {
            -webkit-box-shadow: 0 2px 2px #101010;
            -moz-box-shadow: 0 2px 2px #101010;
            box-shadow: 0 2px 2px #101010;
        }

        .grass {
            position: relative;
            top: -80px;
            left: 0;
            z-index: -1;
        }

        .clear {
            clear: both;
        }

    </style>
</head>
<body>
{[[block name=body]]}{[[/block]]}
</body>
</html>