<?php
namespace {|$module|}\Controller;

use {|$module|}\Dependence\{|$module|}Loader;
use Pasoka\Component\Http\Request\Request;
use Pasoka\Component\Route\Interfaces\ControllerInterface;


/**
 * [Generated by helper - {|$date|}]
 *
 * Class IndexController
 *
 * @author Pasoka Helper
 * @package {|$module|}
 * @subpackage Controller
 * @namespace {|$module|}\Controller
 * @version 1.0.0
 */
class IndexController implements ControllerInterface
{

    /**
     * @var \{|$module|}\Dependence\{|$module|}Loader
     */
    private $loader;

    /**
     * @var \Pasoka\Component\View\Template
     */
    private $tpl;

    /**
     * @var \Pasoka\Component\Values\Values
     */
    private $values;

    /**
     * @var \Pasoka\Component\Log\Logger
     */
    private $log;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->loader = {|$module|}Loader::getInstance();
        $this->tpl = $this->loader->getTemplate();
        $this->values = $this->loader->getValues();
        $this->log = $this->loader->getLogger();
    }

    /**
     * Test Method
     *
     * @param Request $request
     */
    public function recipe(Request $request)
    {

        $language = strtolower($request->getString('lang'));
        if ($language != "pt" && $language != "en") {
            $this->log->info("invalid language {$language}");
            $language = "pt";
        }

        $this->tpl
            ->setVar("name", "{|$module|}")
            ->setVar("values", $this->values->getGroup($language))
            ->setCache(true, 3600)
            ->minify()
            ->show("index.tpl", $language);
    }
}