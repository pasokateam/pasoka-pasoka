{[[extends file='layout.tpl']]}

{[[block name=title]]}{[[$name]]} - {[[$values->recipe]]}{[[/block]]}

{[[block name=body]]}
    <div class="container">
        <div class="recipe radius shadow">
            <h1>{[[$name]]}</h1>
            <section>
                <h2>{[[$values->ingredients]]}</h2>
                <ul>
                    {[[foreach from=$values->ingredientsList item="ingredient"]]}
                        <li>{[[$ingredient]]}</li>
                    {[[/foreach]]}
                </ul>
            </section>
            <section>
                <h2>{[[$values->methodPreparation]]}</h2>
                <ul>
                    {[[foreach from=$values->timeList item="time"]]}
                        <li>{[[$time]]}</li>
                    {[[/foreach]]}
                </ul>
                <ol>
                    {[[foreach from=$values->preparationList item="preparation"]]}
                        <li>{[[$preparation]]}</li>
                    {[[/foreach]]}
                </ol>
            </section>
            <div class="text-right">
                <a href="/{[[strtolower($name)]]}/pt">PT</a> |
                <a href="/{[[strtolower($name)]]}/en">EN</a>
            </div>
        </div>

        <img src="/images/pasoka/peanut.png" class="peanut">
        <img src="/images/pasoka/grass.png" class="grass">
    </div>
    <!-- Generated by helper - {|$date|} -->
{[[/block]]}

