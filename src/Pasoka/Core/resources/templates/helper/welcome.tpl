{|$color.white|}             __________________________________________
{|$color.white|}            /    \ /     //  ____/////////  /   /     /
{|$color.white|}           /     //     //      /////////      /     /
{|$color.white|}          /   __// __  //____  /////////     \/ __  /
{|$color.white|}         /__/   /_ /_ //______/////////__/\ FRAMEWORK

{|$color.yellow|}[HELPER v1.0: Auxiliar para agilizar o desenvolvimento com PASOKA framework]

{|$color.yellow|}Opcoes:

{|$color.white|}   --help, -help, -h, -?

{|$color.yellow|}Comandos:

{|$color.yellow|}  [cache]
{|$color.green|}  cache:clean                  {|$color.white|}Limpa todos os caches

{|$color.yellow|}  [route]
{|$color.green|}  route:generate               {|$color.white|}Gera/Atualiza rotas das controllers

{|$color.yellow|}  [module]
{|$color.green|}  module:create                {|$color.white|}Cria novo modulo
{|$color.green|}  module:remove                {|$color.white|}Remove modulo criado

