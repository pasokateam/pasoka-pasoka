
{|$color.white|}             __________________________________________
{|$color.white|}            /    \ /     //  ____/////////  /   /     /
{|$color.white|}           /     //     //      /////////      /     /
{|$color.white|}          /   __// __  //____  /////////     \/ __  /
{|$color.white|}         /__/   /_ /_ //______/////////__/\ FRAMEWORK

{|$color.yellow|}[HELPER v1.0: Auxiliar para agilizar o desenvolvimento com PASOKA framework]

{|$color.yellow|}Opcoes:

{|$color.white|}   --help, -help, -h, -?

{|$color.yellow|}Comandos:

{|$color.yellow|}  [cache]
{|$color.green|}   cache:clean                  {|$color.white|}Limpa todos os caches

{|$color.yellow|}  [route]
{|$color.green|}   route:generate               {|$color.white|}Gera/Atualiza rotas das controllers

{|$color.yellow|}  [module]
{|$color.green|}   module:create                {|$color.white|}Cria novo modulo
{|$color.green|}   module:remove                {|$color.white|}Remove modulo criado

{|*{|$color.yellow|}  [view]*|}
{|*{|$color.green|}   view:generate                {|$color.white|}Cria camada de template*|}
{|*{|$color.green|}   view:remove                  {|$color.white|}Remove camada de template*|}
{|*{|$color.green|}   view:clean                   {|$color.white|}Limpa cache do template*|}

{|*{|$color.yellow|}  [js]*|}
{|*{|$color.green|}   js:compress                  {|$color.white|}Minifica javascripts de um diretorio*|}

{|*{|$color.yellow|}  [orm]*|}
{|*{|$color.green|}   orm:generate                 {|$color.white|}Cria camada de template*|}
{|*{|$color.green|}   orm:remove                   {|$color.white|}Remove camada de template*|}

{|*{|$color.yellow|}  [cron]*|}
{|*{|$color.green|}   cron:generate                {|$color.white|}Gera/Atualiza executaveis na cron*|}
