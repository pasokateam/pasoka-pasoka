<?php

namespace Pasoka\Core\Dependence;

use Pasoka\Component\Log\Logger;
use Pasoka\Component\Manifest\Config\ModuleConfig;
use Pasoka\Component\Manifest\Manifest;
use Pasoka\Component\Values\Values;
use Pasoka\Component\View\Template;
use Pasoka\Core\Interfaces\BootstrapInterface;

/**
 * Class PasokaLoader
 *
 * @author  Guilherme Santos - guilhermedossantos91@gmail.com
 * @package Dependence
 */
final class PasokaLoader
{

    /**
     * @var BootstrapInterface
     */
    private $bootstrap;

    /**
     * @var Manifest
     */
    private $manifest;

    /**
     * @var ModuleConfig
     */
    private $module;

    /**
     * @param BootstrapInterface|mixed $bootstrap
     */
    public function __construct($bootstrap)
    {
        $this->bootstrap = $bootstrap;
        $this->manifest = new Manifest(__DIR__ . "/../");
        $this->module = $this->manifest->getModule();
    }

    /**
     * @return Template
     */
    public function getTemplate()
    {
        $view = $this->module->getView();
        return Template::getInstance(
            $this->bootstrap->getPathRoot($view->getPathTemplate()),
            $this->bootstrap->getPathRoot($view->getPathCompile()),
            $this->bootstrap->getPathRoot($view->getPathCache()),
            $view->isCache(),
            $view->isCompile()
        );
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        $log = $this->module->getLog();
        $dirLog = $this->bootstrap->getPathRoot($log->getPath());
        $file = (new \DateTime("NOW"))->format("Y-m-d") . ".log";
        $file = $this->bootstrap->getPathRoot($log->getPath(), $file);

        if (!file_exists($file)) {

            if (!is_dir($dirLog)) {
                mkdir($dirLog, 0775);
            }

            $fObj = new \SplFileObject($file, "w+");
            $fObj->fwrite("");
            $fObj = null;
            chmod($file, 0775);
        }

        return new Logger(
            $log->getName(),
            $file
        );
    }

    /**
     * @return Values
     */
    public function getValues()
    {
        $values = $this->module->getValues();
        return Values::getInstance(
            $this->bootstrap->getPathRoot(
                $values->getPath()
            )
        );
    }


    /**
     * @return Manifest
     */
    public function getManifest()
    {
        return $this->manifest;
    }


    /**
     * @return ModuleConfig
     */
    public function getModule()
    {
        return $this->module;
    }

}